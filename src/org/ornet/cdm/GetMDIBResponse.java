//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractGetResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MDIB" type="{http://domain-model-uri/15/04}MDIB"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mdib"
})
@XmlRootElement(name = "GetMDIBResponse", namespace = "http://message-model-uri/15/04")
public class GetMDIBResponse
    extends AbstractGetResponse
{

    @XmlElement(name = "MDIB", namespace = "http://message-model-uri/15/04", required = true)
    protected MDIB mdib;

    /**
     * Ruft den Wert der mdib-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MDIB }
     *     
     */
    public MDIB getMDIB() {
        return mdib;
    }

    /**
     * Legt den Wert der mdib-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MDIB }
     *     
     */
    public void setMDIB(MDIB value) {
        this.mdib = value;
    }

}
