//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für ContextAssociation.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="ContextAssociation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="No"/&gt;
 *     &lt;enumeration value="Pre"/&gt;
 *     &lt;enumeration value="Assoc"/&gt;
 *     &lt;enumeration value="Dis"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ContextAssociation")
@XmlEnum
public enum ContextAssociationStateValue {


    /**
     * No = No Association. There is currently no context information associated.
     * 
     */
    @XmlEnumValue("No")
    NO_ASSOCIATION("No"),

    /**
     * Pre = Pre-Association. Context information is in a pre-association state.
     * 
     */
    @XmlEnumValue("Pre")
    PRE_ASSOCIATION("Pre"),

    /**
     * Assoc = Associated. Context information is associated.
     * 
     */
    @XmlEnumValue("Assoc")
    ASSOCIATED("Assoc"),

    /**
     * Dis = Disassociated. Context information is no longer associated.
     * 
     */
    @XmlEnumValue("Dis")
    DISASSOCIATED("Dis");
    private final String value;

    ContextAssociationStateValue(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ContextAssociationStateValue fromValue(String v) {
        for (ContextAssociationStateValue c: ContextAssociationStateValue.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
