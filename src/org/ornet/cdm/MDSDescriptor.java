//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * AbstractMDSDescriptor represents a Medical Device System (MDS) that in turn represents a medical device such as an anaesthesia workstation. The MDSDescriptor element contains an abstraction of the hardware specification of a medical device.
 * 
 * <p>Java-Klasse für AbstractMDSDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractMDSDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDeviceComponent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MetaData" type="{http://domain-model-uri/15/04}SystemMetaData" minOccurs="0"/&gt;
 *         &lt;element name="Context" type="{http://domain-model-uri/15/04}SystemContext"/&gt;
 *         &lt;element name="Clock" type="{http://domain-model-uri/15/04}ClockDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMDSDescriptor", propOrder = {
    "metaData",
    "context",
    "clock"
})
@XmlSeeAlso({
    HydraMDSDescriptor.class
})
public class MDSDescriptor
    extends DeviceComponent
{

    @XmlElement(name = "MetaData")
    protected SystemMetaData metaData;
    @XmlElement(name = "Context", required = true)
    protected SystemContext context;
    @XmlElement(name = "Clock")
    protected ClockDescriptor clock;

    /**
     * Ruft den Wert der metaData-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemMetaData }
     *     
     */
    public SystemMetaData getMetaData() {
        return metaData;
    }

    /**
     * Legt den Wert der metaData-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemMetaData }
     *     
     */
    public void setMetaData(SystemMetaData value) {
        this.metaData = value;
    }

    /**
     * Ruft den Wert der context-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SystemContext }
     *     
     */
    public SystemContext getContext() {
        return context;
    }

    /**
     * Legt den Wert der context-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SystemContext }
     *     
     */
    public void setContext(SystemContext value) {
        this.context = value;
    }

    /**
     * Ruft den Wert der clock-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ClockDescriptor }
     *     
     */
    public ClockDescriptor getClock() {
        return clock;
    }

    /**
     * Legt den Wert der clock-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ClockDescriptor }
     *     
     */
    public void setClock(ClockDescriptor value) {
        this.clock = value;
    }

}
