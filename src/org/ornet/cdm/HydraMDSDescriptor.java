//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * HydraMDS is a specialization of an abstract Medical Device System (MDS) that MAY have multiple VMDs.
 * 
 * The 11073-10201 has four specilizations: Simple MDS, Hydra MDS, Composite Single Bed MDS, and Composite Multiple Bed MDS. In contrast to a Simple MDS a Hydra MDS may have more than one Virtual Medical Device (VMD). Thus, a Simple MDS can be modeled with a Hydra MDS. Both Composite MDS allow to create recursive MDS structures.
 * 
 * <p>Java-Klasse für HydraMDSDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="HydraMDSDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractMDSDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SCO" type="{http://domain-model-uri/15/04}SCODescriptor" minOccurs="0"/&gt;
 *         &lt;element name="AlertSystem" type="{http://domain-model-uri/15/04}AlertSystemDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="VMD" type="{http://domain-model-uri/15/04}VMDDescriptor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HydraMDSDescriptor", propOrder = {
    "sco",
    "alertSystem",
    "vmDs"
})
public class HydraMDSDescriptor
    extends MDSDescriptor
{

    @XmlElement(name = "SCO")
    protected SCODescriptor sco;
    @XmlElement(name = "AlertSystem")
    protected AlertSystemDescriptor alertSystem;
    @XmlElement(name = "VMD")
    protected List<VMDDescriptor> vmDs;

    /**
     * Ruft den Wert der sco-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SCODescriptor }
     *     
     */
    public SCODescriptor getSCO() {
        return sco;
    }

    /**
     * Legt den Wert der sco-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SCODescriptor }
     *     
     */
    public void setSCO(SCODescriptor value) {
        this.sco = value;
    }

    /**
     * Ruft den Wert der alertSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public AlertSystemDescriptor getAlertSystem() {
        return alertSystem;
    }

    /**
     * Legt den Wert der alertSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public void setAlertSystem(AlertSystemDescriptor value) {
        this.alertSystem = value;
    }

    /**
     * Gets the value of the vmDs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vmDs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVMDs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VMDDescriptor }
     * 
     * 
     */
    public List<VMDDescriptor> getVMDs() {
        if (vmDs == null) {
            vmDs = new ArrayList<VMDDescriptor>();
        }
        return this.vmDs;
    }

}
