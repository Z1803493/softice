//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für IntendedUse.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="IntendedUse"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Inf"/&gt;
 *     &lt;enumeration value="MedA"/&gt;
 *     &lt;enumeration value="MedB"/&gt;
 *     &lt;enumeration value="MedC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IntendedUse")
@XmlEnum
public enum IntendedUse {


    /**
     * Inf = Informational. The descriptor and the related state information shall be used for information purposes only. They are not intended to be used in medical-grade algorithms or applications.
     * 
     */
    @XmlEnumValue("Inf")
    INFORMATIONAL("Inf"),

    /**
     * MedA = Medical Class A. The descriptor and related state information are intended to be used in medical-grade algorithms or applications. Class A indicates that no injury or damage to health is possible if the descriptor and/or related state information is somehow erroneous.
     * 
     */
    @XmlEnumValue("MedA")
    MED_A("MedA"),

    /**
     * MedB = Medical Class B. The descriptor and related state information are intended to be used in medical-grade algorithms or applications. Class B indicates that non-serious injury is possible if the descriptor and/or related state information is somehow erroneous.
     * 
     */
    @XmlEnumValue("MedB")
    MED_B("MedB"),

    /**
     * MedC = Medical Class C. The descriptor and related state information are intended to be used in medical-grade algorithms or applications. Class C indicates that death or serious injury is possible if the descriptor and/or related state information is somehow erroneous.
     * 
     */
    @XmlEnumValue("MedC")
    MED_C("MedC");
    private final String value;

    IntendedUse(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IntendedUse fromValue(String v) {
        for (IntendedUse c: IntendedUse.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
