//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * Declares a sample array that represents a real-time continuous waveform. An example would be an electrocardiogram (ECG) real-time wave.
 * 
 * <p>Java-Klasse für RealTimeSampleArrayMetricDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RealTimeSampleArrayMetricDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractMetricDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SamplePeriod" type="{http://www.w3.org/2001/XMLSchema}duration"/&gt;
 *         &lt;element name="Resolution" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="TechnicalRange" type="{http://domain-model-uri/15/04}Range" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RealTimeSampleArrayMetricDescriptor", propOrder = {
    "sampleRate",
    "resolution",
    "maxRanges"
})
public class RealTimeSampleArrayMetricDescriptor
    extends MetricDescriptor
{

    @XmlElement(name = "SamplePeriod", required = true)
    protected Duration sampleRate;
    @XmlElement(name = "Resolution", required = true)
    protected BigDecimal resolution;
    @XmlElement(name = "TechnicalRange")
    protected List<Range> maxRanges;

    /**
     * Ruft den Wert der sampleRate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getSampleRate() {
        return sampleRate;
    }

    /**
     * Legt den Wert der sampleRate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setSampleRate(Duration value) {
        this.sampleRate = value;
    }

    /**
     * Ruft den Wert der resolution-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getResolution() {
        return resolution;
    }

    /**
     * Legt den Wert der resolution-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setResolution(BigDecimal value) {
        this.resolution = value;
    }

    /**
     * Gets the value of the maxRanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maxRanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaxRanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Range }
     * 
     * 
     */
    public List<Range> getMaxRanges() {
        if (maxRanges == null) {
            maxRanges = new ArrayList<Range>();
        }
        return this.maxRanges;
    }

}
