//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InvocationState.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="InvocationState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Wait"/&gt;
 *     &lt;enumeration value="Start"/&gt;
 *     &lt;enumeration value="Cnclld"/&gt;
 *     &lt;enumeration value="CnclldMan"/&gt;
 *     &lt;enumeration value="Fin"/&gt;
 *     &lt;enumeration value="Fail"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InvocationState", namespace = "http://message-model-uri/15/04")
@XmlEnum
public enum InvocationState {


    /**
     * Wait = Waiting. The operation has been queued and waits for execution.
     * 
     */
    @XmlEnumValue("Wait")
    WAITING("Wait"),

    /**
     * Start = Started. The execution of this operation has been started.
     * 
     */
    @XmlEnumValue("Start")
    STARTED("Start"),

    /**
     * Cnclld = Cancelled. The execution has been cancelled by the DEVICE.
     * 
     */
    @XmlEnumValue("Cnclld")
    CANCELLED("Cnclld"),

    /**
     * CnclldMan = Cancelled Manually. The execution has been cancelled by the operator.
     * 
     */
    @XmlEnumValue("CnclldMan")
    CANCELLED_MANUALLY("CnclldMan"),

    /**
     * Fin = Finished. The execution has been finished.
     * 
     */
    @XmlEnumValue("Fin")
    FINISHED("Fin"),

    /**
     * The execution has been been failed.
     * 
     */
    @XmlEnumValue("Fail")
    FAILED("Fail");
    private final String value;

    InvocationState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvocationState fromValue(String v) {
        for (InvocationState c: InvocationState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
