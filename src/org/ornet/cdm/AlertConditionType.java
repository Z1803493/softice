//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AlertConditionKind.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="AlertConditionKind"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Phy"/&gt;
 *     &lt;enumeration value="Tec"/&gt;
 *     &lt;enumeration value="Oth"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AlertConditionKind")
@XmlEnum
public enum AlertConditionType {


    /**
     * Phy = Physiological. The condition arises from a patient-related variable. Examples: "Blood pressure high" or "minute volume low".
     * 
     */
    @XmlEnumValue("Phy")
    PHYSIOLOGICAL("Phy"),

    /**
     * Tec = Technical. The condition arises from a monitored equipment-related or Alert System-related variable. Examples: "Battery low" or "sensor unplugged".
     * 
     */
    @XmlEnumValue("Tec")
    TECHNICAL("Tec"),

    /**
     * Oth = Other. The condition arises from another origin, e.g., equipment-
     * user advisory conditions like "room temperature high".
     * 
     */
    @XmlEnumValue("Oth")
    OTHER("Oth");
    private final String value;

    AlertConditionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlertConditionType fromValue(String v) {
        for (AlertConditionType c: AlertConditionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
