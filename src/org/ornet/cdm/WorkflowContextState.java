//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A context state that identifies a step in a clinical workflow.
 * 
 * <p>Java-Klasse für WorkflowContextState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="WorkflowContextState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractIdentifiableContextState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrderDetail" type="{http://domain-model-uri/15/04}Order" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkflowContextState", propOrder = {
    "orderDetail"
})
public class WorkflowContextState
    extends AbstractIdentifiableContextState
{

    @XmlElement(name = "OrderDetail")
    protected Order orderDetail;

    /**
     * Ruft den Wert der orderDetail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Order }
     *     
     */
    public Order getOrderDetail() {
        return orderDetail;
    }

    /**
     * Legt den Wert der orderDetail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Order }
     *     
     */
    public void setOrderDetail(Order value) {
        this.orderDetail = value;
    }

}
