//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstract description of an operation that is exposed on the service interface.
 * 
 * <p>Java-Klasse für AbstractOperationDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractOperationDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ModifiableElement" type="{http://domain-model-uri/15/04}CodedValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="OperationTarget" use="required" type="{http://domain-model-uri/15/04}HandleRef" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractOperationDescriptor", propOrder = {
    "modifiableAttribute"
})
@XmlSeeAlso({
    SetValueOperationDescriptor.class,
    SetStringOperationDescriptor.class,
    ActivateOperationDescriptor.class,
    NonGenericOperationDescriptor.class,
    SetAlertStateOperationDescriptor.class,
    SetRangeOperationDescriptor.class,
    SetContextOperationDescriptor.class
})
public class OperationDescriptor
    extends Descriptor
{

    @XmlElement(name = "ModifiableElement")
    protected List<CodedValue> modifiableAttribute;
    @XmlAttribute(name = "OperationTarget", required = true)
    protected String operationTarget;

    /**
     * Gets the value of the modifiableAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modifiableAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifiableAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodedValue }
     * 
     * 
     */
    public List<CodedValue> getModifiableAttribute() {
        if (modifiableAttribute == null) {
            modifiableAttribute = new ArrayList<CodedValue>();
        }
        return this.modifiableAttribute;
    }

    /**
     * Ruft den Wert der operationTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationTarget() {
        return operationTarget;
    }

    /**
     * Legt den Wert der operationTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationTarget(String value) {
        this.operationTarget = value;
    }

}
