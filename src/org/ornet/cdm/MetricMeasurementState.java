//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MetricMeasurementValidity.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MetricMeasurementValidity"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Vld"/&gt;
 *     &lt;enumeration value="Vldated"/&gt;
 *     &lt;enumeration value="Qst"/&gt;
 *     &lt;enumeration value="Calib"/&gt;
 *     &lt;enumeration value="Inv"/&gt;
 *     &lt;enumeration value="Oflw"/&gt;
 *     &lt;enumeration value="Uflw"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MetricMeasurementValidity")
@XmlEnum
public enum MetricMeasurementState {


    /**
     * Vld = Valid. A correct measured value that is correct from the perspective of the measuring device.
     * 
     */
    @XmlEnumValue("Vld")
    VALID("Vld"),

    /**
     * Vldated = Validated Data. A measured value where the validity has been confirmed by an external actor, e.g., an operator, other than the device.
     * 
     */
    @XmlEnumValue("Vldated")
    VALIDATED_DATA("Vldated"),

    /**
     * Qst = Questionable. A measured value where correctness can not be guaranteed.
     * 
     */
    @XmlEnumValue("Qst")
    QUESTIONABLE("Qst"),

    /**
     * Calib = Calibration Ongoing. A measured value where correctness can not be guaranteed, because a calibration is currently going on.
     * 
     */
    @XmlEnumValue("Calib")
    CALIBRATION_ONGOING("Calib"),

    /**
     * Inv = Invalid. A measured value that is incorrect from the perspective of the measuring device.
     * 
     */
    @XmlEnumValue("Inv")
    INVALID("Inv"),

    /**
     * Oflw = Overflow. TODO: add documentation
     * 
     */
    @XmlEnumValue("Oflw")
    OVERFLOW("Oflw"),

    /**
     * Uflw = Underflow. TODO: add documentation
     * 
     */
    @XmlEnumValue("Uflw")
    UNDERFLOW("Uflw");
    private final String value;

    MetricMeasurementState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MetricMeasurementState fromValue(String v) {
        for (MetricMeasurementState c: MetricMeasurementState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
