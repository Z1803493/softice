//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractGetResponse"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MDState" type="{http://domain-model-uri/15/04}MDState"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mdState"
})
@XmlRootElement(name = "GetMDStateResponse", namespace = "http://message-model-uri/15/04")
public class GetMDStateResponse
    extends AbstractGetResponse
{

    @XmlElement(name = "MDState", namespace = "http://message-model-uri/15/04", required = true)
    protected MDState mdState;

    /**
     * Ruft den Wert der mdState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MDState }
     *     
     */
    public MDState getMDState() {
        return mdState;
    }

    /**
     * Legt den Wert der mdState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MDState }
     *     
     */
    public void setMDState(MDState value) {
        this.mdState = value;
    }

}
