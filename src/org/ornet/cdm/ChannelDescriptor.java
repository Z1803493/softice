//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A Channel is a group of metrics and alerts and is used for organizational purposes only. All metrics MUST be part of a channel.
 * 
 * Example: An example would be a blood pressure VMD with one Channel to group together all metrics that deal with the blood pressure (e.g., pressure value, pressure waveform). A second Channel object could be used to group together metrics that deal with heart rate. TODO: 11073 Copyright Example
 * 
 * <p>Java-Klasse für ChannelDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ChannelDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDeviceComponent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Metric" type="{http://domain-model-uri/15/04}AbstractMetricDescriptor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AlertSystem" type="{http://domain-model-uri/15/04}AlertSystemDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChannelDescriptor", propOrder = {
    "metrics",
    "alertSystem"
})
public class ChannelDescriptor
    extends DeviceComponent
{

    @XmlElement(name = "Metric")
    protected List<MetricDescriptor> metrics;
    @XmlElement(name = "AlertSystem")
    protected AlertSystemDescriptor alertSystem;

    /**
     * Gets the value of the metrics property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metrics property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetrics().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetricDescriptor }
     * 
     * 
     */
    public List<MetricDescriptor> getMetrics() {
        if (metrics == null) {
            metrics = new ArrayList<MetricDescriptor>();
        }
        return this.metrics;
    }

    /**
     * Ruft den Wert der alertSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public AlertSystemDescriptor getAlertSystem() {
        return alertSystem;
    }

    /**
     * Legt den Wert der alertSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public void setAlertSystem(AlertSystemDescriptor value) {
        this.alertSystem = value;
    }

}
