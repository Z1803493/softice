//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ImagingProcedure provide identifiers used by the DICOM and HL7 standard
 *  to identify the requested imaging procedures resulting from an order in a the hospital. Often these identifiers
 *  are created/assigned by the main hospital information system or departmental information systems and are taken over into any medical images by DICOM equipment in the context of this procedure. The listed elements have been taken over from the IHE Radiology Technical Framework's RAD-4 transaction ("Procedure Scheduled") and re-uses the identifiers listed vor the HL7 Version 2.5.1 IPC segment group of the OBR segment. Therefore it is recommended to comply to the underlying HL7 and DICOM data types in order to have seamless integration with other clinical IT such as DICOM modalities or image archives (PACS).
 * 
 * In order to comply to the hierarchy behind the given identifiers, the following rules (taken from IHE) SHOULD apply: If a Requested Procedure is comprised of multiple Scheduled Procedure Steps and/or if a Scheduled Procedure Step is comprised of multiple Protocol Codes, each applicable Scheduled Procedure Step / Protocol Code combination shall be included as a separate ProcedureDetails structure, i.e. the complext type "ProcedureDetails" must occur the same amount of times as there are different Scheduled Procedure Step IDs plus the amount of different Scheduled Procedure Step / Protocol Code combinations.
 * 			
 * 
 * <p>Java-Klasse für ImagingProcedure complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ImagingProcedure"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element name="AccessionIdentifier" type="{http://domain-model-uri/15/04}InstanceIdentifier"/&gt;
 *         &lt;element name="RequestedProcedureID" type="{http://domain-model-uri/15/04}InstanceIdentifier"/&gt;
 *         &lt;element name="StudyInstanceUID" type="{http://domain-model-uri/15/04}InstanceIdentifier"/&gt;
 *         &lt;element name="ScheduledProcedureStepID" type="{http://domain-model-uri/15/04}InstanceIdentifier"/&gt;
 *         &lt;element name="Modality" type="{http://domain-model-uri/15/04}CodedValue" minOccurs="0"/&gt;
 *         &lt;element name="ProtocolCode" type="{http://domain-model-uri/15/04}CodedValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImagingProcedure", propOrder = {
    "extension",
    "accessionIdentifier",
    "requestedProcedureID",
    "studyInstanceUID",
    "scheduledProcedureStepID",
    "modality",
    "protocolCode"
})
public class ImagingProcedure {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "AccessionIdentifier", required = true)
    protected InstanceIdentifier accessionIdentifier;
    @XmlElement(name = "RequestedProcedureID", required = true)
    protected InstanceIdentifier requestedProcedureID;
    @XmlElement(name = "StudyInstanceUID", required = true)
    protected InstanceIdentifier studyInstanceUID;
    @XmlElement(name = "ScheduledProcedureStepID", required = true)
    protected InstanceIdentifier scheduledProcedureStepID;
    @XmlElement(name = "Modality")
    protected CodedValue modality;
    @XmlElement(name = "ProtocolCode")
    protected CodedValue protocolCode;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der accessionIdentifier-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getAccessionIdentifier() {
        return accessionIdentifier;
    }

    /**
     * Legt den Wert der accessionIdentifier-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setAccessionIdentifier(InstanceIdentifier value) {
        this.accessionIdentifier = value;
    }

    /**
     * Ruft den Wert der requestedProcedureID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getRequestedProcedureID() {
        return requestedProcedureID;
    }

    /**
     * Legt den Wert der requestedProcedureID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setRequestedProcedureID(InstanceIdentifier value) {
        this.requestedProcedureID = value;
    }

    /**
     * Ruft den Wert der studyInstanceUID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getStudyInstanceUID() {
        return studyInstanceUID;
    }

    /**
     * Legt den Wert der studyInstanceUID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setStudyInstanceUID(InstanceIdentifier value) {
        this.studyInstanceUID = value;
    }

    /**
     * Ruft den Wert der scheduledProcedureStepID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getScheduledProcedureStepID() {
        return scheduledProcedureStepID;
    }

    /**
     * Legt den Wert der scheduledProcedureStepID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setScheduledProcedureStepID(InstanceIdentifier value) {
        this.scheduledProcedureStepID = value;
    }

    /**
     * Ruft den Wert der modality-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getModality() {
        return modality;
    }

    /**
     * Legt den Wert der modality-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setModality(CodedValue value) {
        this.modality = value;
    }

    /**
     * Ruft den Wert der protocolCode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getProtocolCode() {
        return protocolCode;
    }

    /**
     * Legt den Wert der protocolCode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setProtocolCode(CodedValue value) {
        this.protocolCode = value;
    }

}
