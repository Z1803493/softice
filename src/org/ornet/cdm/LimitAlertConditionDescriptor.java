//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A specialization of an alert condition that is satisfied if at least one limit for a referenced metric has been violated.
 * 
 * <p>Java-Klasse für LimitAlertConditionDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LimitAlertConditionDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AlertConditionDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MaxLimits" type="{http://domain-model-uri/15/04}Range"/&gt;
 *         &lt;element name="AutoLimitSupported" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LimitAlertConditionDescriptor", propOrder = {
    "maxLimitRange",
    "supportsAutoLimit"
})
public class LimitAlertConditionDescriptor
    extends AlertConditionDescriptor
{

    @XmlElement(name = "MaxLimits", required = true)
    protected Range maxLimitRange;
    @XmlElement(name = "AutoLimitSupported")
    protected Boolean supportsAutoLimit;

    /**
     * Ruft den Wert der maxLimitRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Range }
     *     
     */
    public Range getMaxLimitRange() {
        return maxLimitRange;
    }

    /**
     * Legt den Wert der maxLimitRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Range }
     *     
     */
    public void setMaxLimitRange(Range value) {
        this.maxLimitRange = value;
    }

    /**
     * Ruft den Wert der supportsAutoLimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSupportsAutoLimit() {
        return supportsAutoLimit;
    }

    /**
     * Legt den Wert der supportsAutoLimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSupportsAutoLimit(Boolean value) {
        this.supportsAutoLimit = value;
    }

}
