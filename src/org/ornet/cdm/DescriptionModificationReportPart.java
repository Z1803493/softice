//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A container for a list of descriptors that belong to one parent and that have changed.
 * 
 * <p>Java-Klasse für DescriptionModificationReportPart complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DescriptionModificationReportPart"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractReportPart"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Descriptor" type="{http://domain-model-uri/15/04}AbstractDescriptor"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ParentDescriptor" use="required" type="{http://domain-model-uri/15/04}HandleRef" /&gt;
 *       &lt;attribute name="ModificationType" type="{http://message-model-uri/15/04}DescriptionModificationType" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DescriptionModificationReportPart", namespace = "http://message-model-uri/15/04", propOrder = {
    "descriptor"
})
public class DescriptionModificationReportPart
    extends ReportPart
{

    @XmlElement(name = "Descriptor", required = true)
    protected Descriptor descriptor;
    @XmlAttribute(name = "ParentDescriptor", required = true)
    protected String parentDescriptor;
    @XmlAttribute(name = "ModificationType")
    protected DescriptionModificationType modificationType;

    /**
     * Ruft den Wert der descriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Descriptor }
     *     
     */
    public Descriptor getDescriptor() {
        return descriptor;
    }

    /**
     * Legt den Wert der descriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Descriptor }
     *     
     */
    public void setDescriptor(Descriptor value) {
        this.descriptor = value;
    }

    /**
     * Ruft den Wert der parentDescriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentDescriptor() {
        return parentDescriptor;
    }

    /**
     * Legt den Wert der parentDescriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentDescriptor(String value) {
        this.parentDescriptor = value;
    }

    /**
     * Ruft den Wert der modificationType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DescriptionModificationType }
     *     
     */
    public DescriptionModificationType getModificationType() {
        return modificationType;
    }

    /**
     * Legt den Wert der modificationType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DescriptionModificationType }
     *     
     */
    public void setModificationType(DescriptionModificationType value) {
        this.modificationType = value;
    }

}
