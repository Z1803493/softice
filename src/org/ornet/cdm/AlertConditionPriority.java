//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für AlertConditionPriority.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="AlertConditionPriority"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Lo"/&gt;
 *     &lt;enumeration value="Me"/&gt;
 *     &lt;enumeration value="Hi"/&gt;
 *     &lt;enumeration value="None"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AlertConditionPriority")
@XmlEnum
public enum AlertConditionPriority {


    /**
     * Lo = Low. Awareness for the alert condition is required.
     * 
     */
    @XmlEnumValue("Lo")
    LOW("Lo"),

    /**
     * Me = Medium. Prompt response to remove the alert condition is required.
     * 
     */
    @XmlEnumValue("Me")
    MEDIUM("Me"),

    /**
     * Hi = High. Immediate response to remove the alert condition is required.
     * 
     */
    @XmlEnumValue("Hi")
    HIGH("Hi"),

    /**
     * No awareness for the alert condition is required.
     * 
     */
    @XmlEnumValue("None")
    NONE("None");
    private final String value;

    AlertConditionPriority(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlertConditionPriority fromValue(String v) {
        for (AlertConditionPriority c: AlertConditionPriority.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
