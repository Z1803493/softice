//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * The AlertSignalState type contains the dynamic/volatile information of an alert signal. See AlertSignalDescriptor for static information.
 * 
 * <p>Java-Klasse für AlertSignalState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertSignalState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractAlertState"&gt;
 *       &lt;attribute name="ActivationState" use="required" type="{http://domain-model-uri/15/04}PausableActivation" /&gt;
 *       &lt;attribute name="Presence" type="{http://domain-model-uri/15/04}SignalPresence" /&gt;
 *       &lt;attribute name="Location" type="{http://domain-model-uri/15/04}PrimaryAlertSignalLocation" /&gt;
 *       &lt;attribute name="Slot" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertSignalState")
public class CurrentAlertSignal
    extends AbstractAlertState
{

    @XmlAttribute(name = "ActivationState", required = true)
    protected PausableActivation state;
    @XmlAttribute(name = "Presence")
    protected SignalPresence presence;
    @XmlAttribute(name = "Location")
    protected AlertSignalLocation location;
    @XmlAttribute(name = "Slot")
    protected Integer slot;

    /**
     * Ruft den Wert der state-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PausableActivation }
     *     
     */
    public PausableActivation getState() {
        return state;
    }

    /**
     * Legt den Wert der state-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PausableActivation }
     *     
     */
    public void setState(PausableActivation value) {
        this.state = value;
    }

    /**
     * Ruft den Wert der presence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link SignalPresence }
     *     
     */
    public SignalPresence getPresence() {
        return presence;
    }

    /**
     * Legt den Wert der presence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link SignalPresence }
     *     
     */
    public void setPresence(SignalPresence value) {
        this.presence = value;
    }

    /**
     * Ruft den Wert der location-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertSignalLocation }
     *     
     */
    public AlertSignalLocation getLocation() {
        return location;
    }

    /**
     * Legt den Wert der location-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertSignalLocation }
     *     
     */
    public void setLocation(AlertSignalLocation value) {
        this.location = value;
    }

    /**
     * Ruft den Wert der slot-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSlot() {
        return slot;
    }

    /**
     * Legt den Wert der slot-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSlot(Integer value) {
        this.slot = value;
    }

}
