//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PausableActivation.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PausableActivation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="On"/&gt;
 *     &lt;enumeration value="Off"/&gt;
 *     &lt;enumeration value="Psd"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PausableActivation")
@XmlEnum
public enum PausableActivation {


    /**
     * The alert system element or the the alert system itself is operating.
     * 
     */
    @XmlEnumValue("On")
    ON("On"),

    /**
     * The alert system element or the the alert system itself is not operating.
     * 
     */
    @XmlEnumValue("Off")
    OFF("Off"),

    /**
     * Psd = Paused. The alert system element or the the alert system itself is temporarly not operating.
     * 
     */
    @XmlEnumValue("Psd")
    PAUSED("Psd");
    private final String value;

    PausableActivation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PausableActivation fromValue(String v) {
        for (PausableActivation c: PausableActivation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
