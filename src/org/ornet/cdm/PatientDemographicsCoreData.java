//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * The patient demographics data as they are defined in ISO/IEEE 11073-10201:2004 (6.10.1 Patient Demographics object). If the device itself has patient-related observations (e.g., weight, height, ...) as in or output, this SHOULD be modelled as metrics. The PatientDemographicsCoreData type is for information purposes, only.
 * 
 * <p>Java-Klasse für PatientDemographicsCoreData complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="PatientDemographicsCoreData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}BaseDemographics"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Sex" type="{http://domain-model-uri/15/04}Sex" minOccurs="0"/&gt;
 *         &lt;element name="PatientType" type="{http://domain-model-uri/15/04}PatientType" minOccurs="0"/&gt;
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="Height" type="{http://domain-model-uri/15/04}Measure" minOccurs="0"/&gt;
 *         &lt;element name="Weight" type="{http://domain-model-uri/15/04}Measure" minOccurs="0"/&gt;
 *         &lt;element name="Race" type="{http://domain-model-uri/15/04}CodedValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PatientDemographicsCoreData", propOrder = {
    "sex",
    "type",
    "birthday",
    "height",
    "weight",
    "race"
})
@XmlSeeAlso({
    NeonatalPatientDemographicsCoreData.class
})
public class PatientDemographicsCoreData
    extends BaseDemographics
{

    @XmlElement(name = "Sex")
    @XmlSchemaType(name = "string")
    protected Sex sex;
    @XmlElement(name = "PatientType")
    @XmlSchemaType(name = "string")
    protected PatientType type;
    @XmlElement(name = "DateOfBirth")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar birthday;
    @XmlElement(name = "Height")
    protected Measure height;
    @XmlElement(name = "Weight")
    protected Measure weight;
    @XmlElement(name = "Race")
    protected CodedValue race;

    /**
     * Ruft den Wert der sex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Sex }
     *     
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Legt den Wert der sex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Sex }
     *     
     */
    public void setSex(Sex value) {
        this.sex = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PatientType }
     *     
     */
    public PatientType getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientType }
     *     
     */
    public void setType(PatientType value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der birthday-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthday() {
        return birthday;
    }

    /**
     * Legt den Wert der birthday-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthday(XMLGregorianCalendar value) {
        this.birthday = value;
    }

    /**
     * Ruft den Wert der height-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Measure }
     *     
     */
    public Measure getHeight() {
        return height;
    }

    /**
     * Legt den Wert der height-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Measure }
     *     
     */
    public void setHeight(Measure value) {
        this.height = value;
    }

    /**
     * Ruft den Wert der weight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Measure }
     *     
     */
    public Measure getWeight() {
        return weight;
    }

    /**
     * Legt den Wert der weight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Measure }
     *     
     */
    public void setWeight(Measure value) {
        this.weight = value;
    }

    /**
     * Ruft den Wert der race-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getRace() {
        return race;
    }

    /**
     * Legt den Wert der race-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setRace(CodedValue value) {
        this.race = value;
    }

}
