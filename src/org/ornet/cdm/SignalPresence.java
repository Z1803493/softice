//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SignalPresence.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SignalPresence"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="On"/&gt;
 *     &lt;enumeration value="Off"/&gt;
 *     &lt;enumeration value="Latch"/&gt;
 *     &lt;enumeration value="Ack"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SignalPresence")
@XmlEnum
public enum SignalPresence {


    /**
     * Indicates that the alert signal element is currently generated.
     * 
     */
    @XmlEnumValue("On")
    ON("On"),

    /**
     * Indicates that the alert signal element is currently not generated.
     * 
     */
    @XmlEnumValue("Off")
    OFF("Off"),

    /**
     * Latch = Latched. It indicates that the alert signal is currently generated even if the alert condition is no longer present.
     * 
     */
    @XmlEnumValue("Latch")
    LATCHED("Latch"),

    /**
     * Ack = Acknowledged. It indicates that the alert signal is currently not generated due to an acknowledment even if the alert condition is still present. Acknowledged signals are those, where an auditory alarm signal that is related to a currently active alarm condition is inactivated until the alarm condition is no longer present.
     * 
     */
    @XmlEnumValue("Ack")
    ACKNOWLEDGED("Ack");
    private final String value;

    SignalPresence(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SignalPresence fromValue(String v) {
        for (SignalPresence c: SignalPresence.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
