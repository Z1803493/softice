//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A state of a limit alert condition.
 * 
 * <p>Java-Klasse für LimitAlertConditionState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LimitAlertConditionState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AlertConditionState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Limits" type="{http://domain-model-uri/15/04}Range" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MonitoredAlertLimits" use="required" type="{http://domain-model-uri/15/04}MonitoredAlertLimits" /&gt;
 *       &lt;attribute name="AutoLimitActivationState" type="{http://domain-model-uri/15/04}PausableActivation" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LimitAlertConditionState", propOrder = {
    "limits"
})
public class CurrentLimitAlertCondition
    extends CurrentAlertCondition
{

    @XmlElement(name = "Limits")
    protected Range limits;
    @XmlAttribute(name = "MonitoredAlertLimits", required = true)
    protected LimitAlertObservationState limitObservationState;
    @XmlAttribute(name = "AutoLimitActivationState")
    protected PausableActivation autoLimitState;

    /**
     * Ruft den Wert der limits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Range }
     *     
     */
    public Range getLimits() {
        return limits;
    }

    /**
     * Legt den Wert der limits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Range }
     *     
     */
    public void setLimits(Range value) {
        this.limits = value;
    }

    /**
     * Ruft den Wert der limitObservationState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LimitAlertObservationState }
     *     
     */
    public LimitAlertObservationState getLimitObservationState() {
        return limitObservationState;
    }

    /**
     * Legt den Wert der limitObservationState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LimitAlertObservationState }
     *     
     */
    public void setLimitObservationState(LimitAlertObservationState value) {
        this.limitObservationState = value;
    }

    /**
     * Ruft den Wert der autoLimitState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PausableActivation }
     *     
     */
    public PausableActivation getAutoLimitState() {
        return autoLimitState;
    }

    /**
     * Legt den Wert der autoLimitState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PausableActivation }
     *     
     */
    public void setAutoLimitState(PausableActivation value) {
        this.autoLimitState = value;
    }

}
