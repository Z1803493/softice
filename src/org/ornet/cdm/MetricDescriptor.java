//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * An abstract descriptor for a metric.
 * 
 * <p>Java-Klasse für AbstractMetricDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractMetricDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}Unit"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}BodySite" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}MetricCategory"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}Availability"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MaxDelayTime" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMetricDescriptor", propOrder = {
    "unit",
    "bodySites",
    "category",
    "availability"
})
@XmlSeeAlso({
    NumericMetricDescriptor.class,
    StringMetricDescriptor.class,
    RealTimeSampleArrayMetricDescriptor.class
})
public class MetricDescriptor
    extends Descriptor
{

    @XmlElement(name = "Unit", required = true)
    protected CodedValue unit;
    @XmlElement(name = "BodySite")
    protected List<CodedValue> bodySites;
    @XmlElement(name = "MetricCategory", required = true)
    @XmlSchemaType(name = "string")
    protected MetricCategory category;
    @XmlElement(name = "Availability", required = true)
    @XmlSchemaType(name = "string")
    protected MetricAvailability availability;
    @XmlAttribute(name = "MaxDelayTime")
    protected Duration maxDelayTime;

    /**
     * Ruft den Wert der unit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getUnit() {
        return unit;
    }

    /**
     * Legt den Wert der unit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setUnit(CodedValue value) {
        this.unit = value;
    }

    /**
     * Optional list of codes that describe the body sites.Gets the value of the bodySites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bodySites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBodySites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodedValue }
     * 
     * 
     */
    public List<CodedValue> getBodySites() {
        if (bodySites == null) {
            bodySites = new ArrayList<CodedValue>();
        }
        return this.bodySites;
    }

    /**
     * Ruft den Wert der category-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetricCategory }
     *     
     */
    public MetricCategory getCategory() {
        return category;
    }

    /**
     * Legt den Wert der category-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetricCategory }
     *     
     */
    public void setCategory(MetricCategory value) {
        this.category = value;
    }

    /**
     * Ruft den Wert der availability-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MetricAvailability }
     *     
     */
    public MetricAvailability getAvailability() {
        return availability;
    }

    /**
     * Legt den Wert der availability-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MetricAvailability }
     *     
     */
    public void setAvailability(MetricAvailability value) {
        this.availability = value;
    }

    /**
     * Ruft den Wert der maxDelayTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMaxDelayTime() {
        return maxDelayTime;
    }

    /**
     * Legt den Wert der maxDelayTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMaxDelayTime(Duration value) {
        this.maxDelayTime = value;
    }

}
