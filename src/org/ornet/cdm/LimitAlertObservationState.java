//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MonitoredAlertLimits.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MonitoredAlertLimits"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="All"/&gt;
 *     &lt;enumeration value="LoOff"/&gt;
 *     &lt;enumeration value="HiOff"/&gt;
 *     &lt;enumeration value="None"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MonitoredAlertLimits")
@XmlEnum
public enum LimitAlertObservationState {


    /**
     * Both AlertLimits are monitored.
     * 
     */
    @XmlEnumValue("All")
    ALL_ON("All"),

    /**
     * LoOff = Low-Off. Low-limit violation detection is either currrently turned off if the state possesses a low-limit value or is not supported at all.
     * 
     */
    @XmlEnumValue("LoOff")
    LOW_OFF("LoOff"),

    /**
     * HiOff = Hi-Off. High-limit violation detection is either currently turned off if the state possesses a high-limit value or is not supported at all.
     * 
     */
    @XmlEnumValue("HiOff")
    HIGH_OFF("HiOff"),

    /**
     * No AlertLimits are monitored. 
     * Note: This is not equal to the state that AlertConditionState is in an Off state, although the result w.r.t. to alarm signlization will be the same.
     * 
     */
    @XmlEnumValue("None")
    ALL_OFF("None");
    private final String value;

    LimitAlertObservationState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LimitAlertObservationState fromValue(String v) {
        for (LimitAlertObservationState c: LimitAlertObservationState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
