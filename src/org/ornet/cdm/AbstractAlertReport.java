//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * A report that contains information about alerts.
 * 
 * <p>Java-Klasse für AbstractAlertReport complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractAlertReport"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractReport"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AlertReportDetail" type="{http://message-model-uri/15/04}AlertReportPart" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractAlertReport", namespace = "http://message-model-uri/15/04", propOrder = {
    "reportParts"
})
@XmlSeeAlso({
    EpisodicAlertReport.class,
    PeriodicAlertReport.class
})
public class AbstractAlertReport
    extends AbstractReport
{

    @XmlElement(name = "AlertReportDetail", required = true)
    protected List<AlertReportPart> reportParts;

    /**
     * Gets the value of the reportParts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportParts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportParts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlertReportPart }
     * 
     * 
     */
    public List<AlertReportPart> getReportParts() {
        if (reportParts == null) {
            reportParts = new ArrayList<AlertReportPart>();
        }
        return this.reportParts;
    }

}
