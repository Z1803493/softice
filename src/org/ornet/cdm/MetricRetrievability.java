//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MetricRetrievability.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MetricRetrievability"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Get"/&gt;
 *     &lt;enumeration value="Per"/&gt;
 *     &lt;enumeration value="Ep"/&gt;
 *     &lt;enumeration value="Strm"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MetricRetrievability", namespace = "http://message-model-uri/15/04")
@XmlEnum
public enum MetricRetrievability {


    /**
     * The metric is retrievable via a Get request. Use the corresponding get message (e.g.GetMetrics).
     * 
     */
    @XmlEnumValue("Get")
    GET("Get"),

    /**
     * Per = Periodic. The metric is retrievable via a periodic event report (TODO: 11073: MDC_NOTI_BUF_SCAN_RPT (1:3331), even though not configurable). Use the corresponding periodic event report message (e.g. EpisodicMetricReport) . 
     * 
     */
    @XmlEnumValue("Per")
    PERIODIC("Per"),

    /**
     * Ep = Episodic. The metric is retrievable via a periodic event report (MDC_NOTI_BUF_SCAN_RPT (1:3331) ???, not configurable). Use the corresponding episodic event report message (e.g. PeriodicMetricReport) .
     * 
     */
    @XmlEnumValue("Ep")
    EPISODIC("Ep"),

    /**
     * Strm = Stream. The metric is retrievable via a WaveformStream. Use the WaveformStream message.
     * 
     */
    @XmlEnumValue("Strm")
    STREAM("Strm");
    private final String value;

    MetricRetrievability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MetricRetrievability fromValue(String v) {
        for (MetricRetrievability c: MetricRetrievability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
