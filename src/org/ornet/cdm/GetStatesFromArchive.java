//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * GetStatesFromArchive message is used by the archive service to request states related to a specific state version and/or time frame.
 * 
 * GetStatesFromArchive SHALL at least expect a version or a time frame filter. If both a version and a time frame are defined, the filter SHALL apply by conjunction. If none are defined, the message is invalid. Clue: It is not sufficient to provide handles only.
 * 
 * <p>Java-Klasse für GetStatesFromArchive complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="GetStatesFromArchive"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractGet"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StateRevision" type="{http://domain-model-uri/15/04}ReferencedVersion"/&gt;
 *         &lt;element name="TimeFrame" type="{http://message-model-uri/15/04}TimeFrame" minOccurs="0"/&gt;
 *         &lt;element name="Handle" type="{http://domain-model-uri/15/04}HandleRef" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStatesFromArchive", namespace = "http://message-model-uri/15/04", propOrder = {
    "stateRevision",
    "timeFrame",
    "handle"
})
public class GetStatesFromArchive
    extends AbstractGet
{

    @XmlElement(name = "StateRevision", required = true)
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger stateRevision;
    @XmlElement(name = "TimeFrame")
    protected TimeFrame timeFrame;
    @XmlElement(name = "Handle")
    protected List<String> handle;

    /**
     * Ruft den Wert der stateRevision-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStateRevision() {
        return stateRevision;
    }

    /**
     * Legt den Wert der stateRevision-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStateRevision(BigInteger value) {
        this.stateRevision = value;
    }

    /**
     * Ruft den Wert der timeFrame-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TimeFrame }
     *     
     */
    public TimeFrame getTimeFrame() {
        return timeFrame;
    }

    /**
     * Legt den Wert der timeFrame-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeFrame }
     *     
     */
    public void setTimeFrame(TimeFrame value) {
        this.timeFrame = value;
    }

    /**
     * Gets the value of the handle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the handle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHandle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getHandle() {
        if (handle == null) {
            handle = new ArrayList<String>();
        }
        return this.handle;
    }

}
