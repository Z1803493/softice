//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstract state of a metric.
 * 
 * <p>Java-Klasse für AbstractMetricValue complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractMetricValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element name="MeasurementState" type="{http://domain-model-uri/15/04}MeasurementState"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Start-Time" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *       &lt;attribute name="Stop-Time" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *       &lt;attribute name="ObservationTime" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMetricValue", propOrder = {
    "extension",
    "measurementState"
})
@XmlSeeAlso({
    NumericValue.class,
    RealTimeSampleArrayValue.class,
    StringMetricValue.class
})
public class AbstractMetricValue {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "MeasurementState", required = true)
    protected MeasurementState measurementState;
    @XmlAttribute(name = "Start-Time")
    protected BigInteger startTime;
    @XmlAttribute(name = "Stop-Time")
    protected BigInteger stopTime;
    @XmlAttribute(name = "ObservationTime")
    protected BigInteger timeOfObservation;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der measurementState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MeasurementState }
     *     
     */
    public MeasurementState getMeasurementState() {
        return measurementState;
    }

    /**
     * Legt den Wert der measurementState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasurementState }
     *     
     */
    public void setMeasurementState(MeasurementState value) {
        this.measurementState = value;
    }

    /**
     * Ruft den Wert der startTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartTime() {
        return startTime;
    }

    /**
     * Legt den Wert der startTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartTime(BigInteger value) {
        this.startTime = value;
    }

    /**
     * Ruft den Wert der stopTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStopTime() {
        return stopTime;
    }

    /**
     * Legt den Wert der stopTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStopTime(BigInteger value) {
        this.stopTime = value;
    }

    /**
     * Ruft den Wert der timeOfObservation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTimeOfObservation() {
        return timeOfObservation;
    }

    /**
     * Legt den Wert der timeOfObservation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTimeOfObservation(BigInteger value) {
        this.timeOfObservation = value;
    }

}
