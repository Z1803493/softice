//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PrimaryAlertSignalLocation.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PrimaryAlertSignalLocation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Loc"/&gt;
 *     &lt;enumeration value="Rem"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PrimaryAlertSignalLocation")
@XmlEnum
public enum AlertSignalLocation {


    /**
     * Loc = Local. The AlertSignal is perceivable on the machine where the AlertCondition has been detected.
     * 
     */
    @XmlEnumValue("Loc")
    LOCAL("Loc"),

    /**
     * Rem = Remote. The AlertSignal is perceivable on a remote machine.
     * 
     */
    @XmlEnumValue("Rem")
    REMOTE("Rem");
    private final String value;

    AlertSignalLocation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AlertSignalLocation fromValue(String v) {
        for (AlertSignalLocation c: AlertSignalLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
