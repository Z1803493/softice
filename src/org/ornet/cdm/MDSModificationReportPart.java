//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A container for a list of MDS descriptors that belong to one parent and that have changed.
 * 
 * <p>Java-Klasse für MDSModificationReportPart complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MDSModificationReportPart"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractReportPart"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MDS" type="{http://domain-model-uri/15/04}AbstractMDSDescriptor" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MDSModificationReportPart", namespace = "http://message-model-uri/15/04", propOrder = {
    "mdsDescriptors"
})
public class MDSModificationReportPart
    extends ReportPart
{

    @XmlElement(name = "MDS", required = true)
    protected List<MDSDescriptor> mdsDescriptors;

    /**
     * Gets the value of the mdsDescriptors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mdsDescriptors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMDSDescriptors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MDSDescriptor }
     * 
     * 
     */
    public List<MDSDescriptor> getMDSDescriptors() {
        if (mdsDescriptors == null) {
            mdsDescriptors = new ArrayList<MDSDescriptor>();
        }
        return this.mdsDescriptors;
    }

}
