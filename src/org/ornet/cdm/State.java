//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * AbstractState defines foundational meta information of any object that is included in the state part of the MDIB (hence, MDState includes a set of AbstractState objects). Any state object is derived from AbstractState. The AbstractState's counterpart is AbstractDescriptor.
 * 
 * <p>Java-Klasse für AbstractState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Handle" type="{http://domain-model-uri/15/04}Handle" /&gt;
 *       &lt;attribute name="StateVersion" type="{http://domain-model-uri/15/04}VersionCounter" /&gt;
 *       &lt;attribute name="DescriptorHandle" use="required" type="{http://domain-model-uri/15/04}HandleRef" /&gt;
 *       &lt;attribute name="DescriptorVersion" type="{http://domain-model-uri/15/04}ReferencedVersion" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractState", propOrder = {
    "extension"
})
@XmlSeeAlso({
    AbstractAlertState.class,
    ComponentState.class,
    OperationState.class,
    ClockState.class,
    AbstractContextState.class
})
public class State {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlAttribute(name = "Handle")
    protected String handle;
    @XmlAttribute(name = "StateVersion")
    protected BigInteger stateVersion;
    @XmlAttribute(name = "DescriptorHandle", required = true)
    protected String referencedDescriptor;
    @XmlAttribute(name = "DescriptorVersion")
    protected BigInteger descriptorVersion;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der handle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandle() {
        return handle;
    }

    /**
     * Legt den Wert der handle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandle(String value) {
        this.handle = value;
    }

    /**
     * Ruft den Wert der stateVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStateVersion() {
        return stateVersion;
    }

    /**
     * Legt den Wert der stateVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStateVersion(BigInteger value) {
        this.stateVersion = value;
    }

    /**
     * Ruft den Wert der referencedDescriptor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencedDescriptor() {
        return referencedDescriptor;
    }

    /**
     * Legt den Wert der referencedDescriptor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencedDescriptor(String value) {
        this.referencedDescriptor = value;
    }

    /**
     * Ruft den Wert der descriptorVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDescriptorVersion() {
        return descriptorVersion;
    }

    /**
     * Legt den Wert der descriptorVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDescriptorVersion(BigInteger value) {
        this.descriptorVersion = value;
    }

}
