//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * State of component that are part of an MDS.
 * 
 * <p>Java-Klasse für ComponentState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ComponentState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CalibrationInfo" type="{http://domain-model-uri/15/04}CalibrationInfo" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute ref="{http://domain-model-uri/15/04}ComponentActivationState"/&gt;
 *       &lt;attribute name="OperatingHours" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="OperatingCycles" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentState", propOrder = {
    "calibrationInfo"
})
@XmlSeeAlso({
    MDSState.class,
    AbstractMetricState.class
})
public class ComponentState
    extends State
{

    @XmlElement(name = "CalibrationInfo")
    protected CalibrationInfo calibrationInfo;
    @XmlAttribute(name = "ComponentActivationState", namespace = "http://domain-model-uri/15/04")
    protected ComponentActivation state;
    @XmlAttribute(name = "OperatingHours")
    protected Integer operatingHours;
    @XmlAttribute(name = "OperatingCycles")
    protected Integer operatingCycles;

    /**
     * Ruft den Wert der calibrationInfo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CalibrationInfo }
     *     
     */
    public CalibrationInfo getCalibrationInfo() {
        return calibrationInfo;
    }

    /**
     * Legt den Wert der calibrationInfo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CalibrationInfo }
     *     
     */
    public void setCalibrationInfo(CalibrationInfo value) {
        this.calibrationInfo = value;
    }

    /**
     * Ruft den Wert der state-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ComponentActivation }
     *     
     */
    public ComponentActivation getState() {
        return state;
    }

    /**
     * Legt den Wert der state-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentActivation }
     *     
     */
    public void setState(ComponentActivation value) {
        this.state = value;
    }

    /**
     * Ruft den Wert der operatingHours-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOperatingHours() {
        return operatingHours;
    }

    /**
     * Legt den Wert der operatingHours-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOperatingHours(Integer value) {
        this.operatingHours = value;
    }

    /**
     * Ruft den Wert der operatingCycles-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getOperatingCycles() {
        return operatingCycles;
    }

    /**
     * Legt den Wert der operatingCycles-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setOperatingCycles(Integer value) {
        this.operatingCycles = value;
    }

}
