//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * The AlertConditionState type contains the dynamic/volatile information of an alert condition. See AlertConditionDescriptor for static information.
 * 
 * <p>Java-Klasse für AlertConditionState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertConditionState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractAlertState"&gt;
 *       &lt;attribute name="ActivationState" use="required" type="{http://domain-model-uri/15/04}PausableActivation" /&gt;
 *       &lt;attribute name="ActualPriority" type="{http://domain-model-uri/15/04}AlertConditionPriority" /&gt;
 *       &lt;attribute name="Rank" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="Presence" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="ObservationTime" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertConditionState")
@XmlSeeAlso({
    CurrentLimitAlertCondition.class
})
public class CurrentAlertCondition
    extends AbstractAlertState
{

    @XmlAttribute(name = "ActivationState", required = true)
    protected PausableActivation state;
    @XmlAttribute(name = "ActualPriority")
    protected AlertConditionPriority degradationPrio;
    @XmlAttribute(name = "Rank")
    protected Integer rank;
    @XmlAttribute(name = "Presence", required = true)
    protected boolean presence;
    @XmlAttribute(name = "ObservationTime")
    protected BigInteger timeOfObservation;

    /**
     * Ruft den Wert der state-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PausableActivation }
     *     
     */
    public PausableActivation getState() {
        return state;
    }

    /**
     * Legt den Wert der state-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PausableActivation }
     *     
     */
    public void setState(PausableActivation value) {
        this.state = value;
    }

    /**
     * Ruft den Wert der degradationPrio-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertConditionPriority }
     *     
     */
    public AlertConditionPriority getDegradationPrio() {
        return degradationPrio;
    }

    /**
     * Legt den Wert der degradationPrio-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertConditionPriority }
     *     
     */
    public void setDegradationPrio(AlertConditionPriority value) {
        this.degradationPrio = value;
    }

    /**
     * Ruft den Wert der rank-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * Legt den Wert der rank-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRank(Integer value) {
        this.rank = value;
    }

    /**
     * Ruft den Wert der presence-Eigenschaft ab.
     * 
     */
    public boolean isPresence() {
        return presence;
    }

    /**
     * Legt den Wert der presence-Eigenschaft fest.
     * 
     */
    public void setPresence(boolean value) {
        this.presence = value;
    }

    /**
     * Ruft den Wert der timeOfObservation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTimeOfObservation() {
        return timeOfObservation;
    }

    /**
     * Legt den Wert der timeOfObservation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTimeOfObservation(BigInteger value) {
        this.timeOfObservation = value;
    }

}
