//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * The SetPatientAssociationState method allows the modification of the association state of the patient.
 * 
 * <p>Java-Klasse für SetPatientAssociationState element declaration.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;element name="SetPatientAssociationState"&gt;
 *   &lt;complexType&gt;
 *     &lt;complexContent&gt;
 *       &lt;extension base="{http://message-model-uri/15/04}AbstractSet"&gt;
 *         &lt;sequence&gt;
 *           &lt;element name="RequestedAssociation" type="{http://domain-model-uri/15/04}ContextAssociation"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/extension&gt;
 *     &lt;/complexContent&gt;
 *   &lt;/complexType&gt;
 * &lt;/element&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "patientAssociationState"
})
@XmlRootElement(name = "SetPatientAssociationState", namespace = "http://message-model-uri/15/04")
public class SetPatientState
    extends AbstractSet
{

    @XmlElement(name = "RequestedAssociation", namespace = "http://message-model-uri/15/04", required = true)
    @XmlSchemaType(name = "string")
    protected ContextAssociationStateValue patientAssociationState;

    /**
     * Ruft den Wert der patientAssociationState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContextAssociationStateValue }
     *     
     */
    public ContextAssociationStateValue getPatientAssociationState() {
        return patientAssociationState;
    }

    /**
     * Legt den Wert der patientAssociationState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextAssociationStateValue }
     *     
     */
    public void setPatientAssociationState(ContextAssociationStateValue value) {
        this.patientAssociationState = value;
    }

}
