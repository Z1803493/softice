//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für InvocationError.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="InvocationError"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unspec"/&gt;
 *     &lt;enumeration value="Unkn"/&gt;
 *     &lt;enumeration value="Inv"/&gt;
 *     &lt;enumeration value="Oth"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "InvocationError", namespace = "http://message-model-uri/15/04")
@XmlEnum
public enum InvocationError {


    /**
     * Unspec = Unspecified. An unspecified error has occurred.
     * 
     */
    @XmlEnumValue("Unspec")
    UNSPECIFIED("Unspec"),

    /**
     * Unkn = Unknown Operation. The handle to the operation object is not known.
     * 
     */
    @XmlEnumValue("Unkn")
    UNKNOWN_OPERATION("Unkn"),

    /**
     * Inv = Invalid Value. The handle to the operation object does not match the invocation request message.
     * 
     */
    @XmlEnumValue("Inv")
    INVALID_VALUE("Inv"),

    /**
     * Oth = Other. Another type of error has occurred
     * 
     */
    @XmlEnumValue("Oth")
    OTHER("Oth");
    private final String value;

    InvocationError(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InvocationError fromValue(String v) {
        for (InvocationError c: InvocationError.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
