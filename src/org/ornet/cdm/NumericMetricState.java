//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A state of a numeric metric.
 * 
 * <p>Java-Klasse für NumericMetricState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NumericMetricState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractMetricState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ObservedValue" type="{http://domain-model-uri/15/04}NumericMetricValue" minOccurs="0"/&gt;
 *         &lt;element name="PhysiologicalRange" type="{http://domain-model-uri/15/04}Range" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NumericMetricState", propOrder = {
    "observedValue",
    "physiologicalRanges"
})
public class NumericMetricState
    extends AbstractMetricState
{

    @XmlElement(name = "ObservedValue")
    protected NumericValue observedValue;
    @XmlElement(name = "PhysiologicalRange")
    protected List<Range> physiologicalRanges;

    /**
     * Ruft den Wert der observedValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NumericValue }
     *     
     */
    public NumericValue getObservedValue() {
        return observedValue;
    }

    /**
     * Legt den Wert der observedValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NumericValue }
     *     
     */
    public void setObservedValue(NumericValue value) {
        this.observedValue = value;
    }

    /**
     * Gets the value of the physiologicalRanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the physiologicalRanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhysiologicalRanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Range }
     * 
     * 
     */
    public List<Range> getPhysiologicalRanges() {
        if (physiologicalRanges == null) {
            physiologicalRanges = new ArrayList<Range>();
        }
        return this.physiologicalRanges;
    }

}
