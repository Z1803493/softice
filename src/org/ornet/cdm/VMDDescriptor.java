//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A Virtual Medical Device (VMD) is an abstraction for a module (medical-related subsystem) of a MDS. According to 11073 a MDS with one VMD is a single purpose device in contrast to a MDS with multiple VMDs that has multipurposes.
 * 
 * Example: An anesthesia workstation (one MDS) with a ventilation unit (one VMD), a patient monitoring unit (another VMD), and gas delivery/monitor system (another VMD). In the 11073 a VMD MUST not be a hardware module, it also can be pure software.
 * 			
 * 
 * <p>Java-Klasse für VMDDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="VMDDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDeviceComponent"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Channel" type="{http://domain-model-uri/15/04}ChannelDescriptor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AlertSystem" type="{http://domain-model-uri/15/04}AlertSystemDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VMDDescriptor", propOrder = {
    "channels",
    "alertSystem"
})
public class VMDDescriptor
    extends DeviceComponent
{

    @XmlElement(name = "Channel")
    protected List<ChannelDescriptor> channels;
    @XmlElement(name = "AlertSystem")
    protected AlertSystemDescriptor alertSystem;

    /**
     * Gets the value of the channels property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the channels property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChannels().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChannelDescriptor }
     * 
     * 
     */
    public List<ChannelDescriptor> getChannels() {
        if (channels == null) {
            channels = new ArrayList<ChannelDescriptor>();
        }
        return this.channels;
    }

    /**
     * Ruft den Wert der alertSystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public AlertSystemDescriptor getAlertSystem() {
        return alertSystem;
    }

    /**
     * Legt den Wert der alertSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertSystemDescriptor }
     *     
     */
    public void setAlertSystem(AlertSystemDescriptor value) {
        this.alertSystem = value;
    }

}
