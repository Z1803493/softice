//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * The MDSModificationReport is sent if at least one MDS descriptor item has been changed during runtime,
 * 
 * <p>Java-Klasse für MDSModificationReport complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MDSModificationReport"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractReport"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReportDetail" type="{http://message-model-uri/15/04}MDSModificationReportPart" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MDSModificationReport", namespace = "http://message-model-uri/15/04", propOrder = {
    "reportDetails"
})
@XmlSeeAlso({
    MDSDeletedReport.class,
    MDSCreatedReport.class
})
public class MDSModificationReport
    extends AbstractReport
{

    @XmlElement(name = "ReportDetail")
    protected List<MDSModificationReportPart> reportDetails;

    /**
     * Gets the value of the reportDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MDSModificationReportPart }
     * 
     * 
     */
    public List<MDSModificationReportPart> getReportDetails() {
        if (reportDetails == null) {
            reportDetails = new ArrayList<MDSModificationReportPart>();
        }
        return this.reportDetails;
    }

}
