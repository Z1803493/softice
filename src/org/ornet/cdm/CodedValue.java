//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * In general, in an interoperability format, objects, attributes, and methods are identified by nomenclature codes. CodedValue offers the ability to represent such nomenclature codes.
 * 
 * <p>Java-Klasse für CodedValue complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="CodedValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}CodingSystemId" minOccurs="0"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}CodingSystemName" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}VersionId" minOccurs="0"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}CodeId"/&gt;
 *         &lt;element name="ConceptDescription" type="{http://domain-model-uri/15/04}LocalizedText" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CodedValue", propOrder = {
    "extension",
    "codingSystem",
    "codingSystemName",
    "version",
    "code",
    "descriptions"
})
@XmlSeeAlso({
    CodedWithEquivalents.class
})
public class CodedValue {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "CodingSystemId")
    @XmlSchemaType(name = "anyURI")
    protected String codingSystem;
    @XmlElement(name = "CodingSystemName")
    protected List<LocalizedText> codingSystemName;
    @XmlElement(name = "VersionId")
    protected String version;
    @XmlElement(name = "CodeId", required = true)
    protected String code;
    @XmlElement(name = "ConceptDescription")
    protected List<LocalizedText> descriptions;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Optional unique identifier of the coding system that the code originating from. If no identifier is defined, the default value SHALL be "urn:oid:1.3.6.1.4.1.19376.1.6.7.1" which references to ISO/IEC 11073-10101:2004.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodingSystem() {
        return codingSystem;
    }

    /**
     * Legt den Wert der codingSystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodingSystem(String value) {
        this.codingSystem = value;
    }

    /**
     * Gets the value of the codingSystemName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codingSystemName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodingSystemName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedText }
     * 
     * 
     */
    public List<LocalizedText> getCodingSystemName() {
        if (codingSystemName == null) {
            codingSystemName = new ArrayList<LocalizedText>();
        }
        return this.codingSystemName;
    }

    /**
     * Describes a particular version of the coding system defined by CodingSystem. VersionId SHALL be set if multiple versions of the underlying coding system exist and a unique identification of the coded value is not possible by other means.
     * 
     * Example: "20041215" for the ISO/IEC 11073-10101:2004, as it is the release date of this first edition of the standard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Ruft den Wert der code-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Legt den Wert der code-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the descriptions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descriptions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescriptions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedText }
     * 
     * 
     */
    public List<LocalizedText> getDescriptions() {
        if (descriptions == null) {
            descriptions = new ArrayList<LocalizedText>();
        }
        return this.descriptions;
    }

}
