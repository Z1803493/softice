//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * TODO, from FHIR spec
 * 
 * <p>Java-Klasse für ProductionSpecification complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ProductionSpecification"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="specType" type="{http://domain-model-uri/15/04}CodedValue"/&gt;
 *         &lt;element name="productionSpec" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="componentId" type="{http://domain-model-uri/15/04}InstanceIdentifier" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductionSpecification", propOrder = {
    "specType",
    "productionSpec",
    "componentId"
})
public class ProductionSpecification {

    @XmlElement(required = true)
    protected CodedValue specType;
    @XmlElement(required = true)
    protected String productionSpec;
    protected InstanceIdentifier componentId;

    /**
     * Ruft den Wert der specType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getSpecType() {
        return specType;
    }

    /**
     * Legt den Wert der specType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setSpecType(CodedValue value) {
        this.specType = value;
    }

    /**
     * Ruft den Wert der productionSpec-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductionSpec() {
        return productionSpec;
    }

    /**
     * Legt den Wert der productionSpec-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductionSpec(String value) {
        this.productionSpec = value;
    }

    /**
     * Ruft den Wert der componentId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getComponentId() {
        return componentId;
    }

    /**
     * Legt den Wert der componentId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setComponentId(InstanceIdentifier value) {
        this.componentId = value;
    }

}
