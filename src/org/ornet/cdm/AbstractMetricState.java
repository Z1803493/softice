//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * Abstract state of a Metric.
 * 
 * <p>Java-Klasse für AbstractMetricState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractMetricState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}ComponentState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MeasurementPeriod" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *         &lt;element name="AveragingPeriod" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}BodySite" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMetricState", propOrder = {
    "measurementPeriod",
    "averagingPeriod",
    "bodySites"
})
@XmlSeeAlso({
    NumericMetricState.class,
    RealTimeSampleArrayMetricState.class,
    StringMetricState.class
})
public class AbstractMetricState
    extends ComponentState
{

    @XmlElement(name = "MeasurementPeriod")
    protected Duration measurementPeriod;
    @XmlElement(name = "AveragingPeriod")
    protected Duration averagingPeriod;
    @XmlElement(name = "BodySite")
    protected List<CodedValue> bodySites;

    /**
     * Ruft den Wert der measurementPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMeasurementPeriod() {
        return measurementPeriod;
    }

    /**
     * Legt den Wert der measurementPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMeasurementPeriod(Duration value) {
        this.measurementPeriod = value;
    }

    /**
     * Ruft den Wert der averagingPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAveragingPeriod() {
        return averagingPeriod;
    }

    /**
     * Legt den Wert der averagingPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAveragingPeriod(Duration value) {
        this.averagingPeriod = value;
    }

    /**
     * Optional list of codes that describe the body sites where this measurement is performed.Gets the value of the bodySites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bodySites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBodySites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodedValue }
     * 
     * 
     */
    public List<CodedValue> getBodySites() {
        if (bodySites == null) {
            bodySites = new ArrayList<CodedValue>();
        }
        return this.bodySites;
    }

}
