//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für DescriptionModificationType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="DescriptionModificationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Crt"/&gt;
 *     &lt;enumeration value="Upt"/&gt;
 *     &lt;enumeration value="Del"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DescriptionModificationType", namespace = "http://message-model-uri/15/04")
@XmlEnum
public enum DescriptionModificationType {


    /**
     * Crt = Created. Indicates that the object transmitted by a modification message has been created (inserted into the MDIB).
     * 
     */
    @XmlEnumValue("Crt")
    CRT("Crt"),

    /**
     * Upt = Updated. Indicates that the object transmitted by a modification message has been updated.
     * 
     */
    @XmlEnumValue("Upt")
    UPT("Upt"),

    /**
     * Del = Deleted. Indicates that the object transmitted by a modification message has been deleted (removed from the MDIB).
     * 
     */
    @XmlEnumValue("Del")
    DEL("Del");
    private final String value;

    DescriptionModificationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DescriptionModificationType fromValue(String v) {
        for (DescriptionModificationType c: DescriptionModificationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
