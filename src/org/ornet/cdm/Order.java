//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An order for a clinical treatment or diagnostic procedure or monitoring procedure.
 * 
 * <p>Java-Klasse für Order complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Order"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element name="VisitNumber" type="{http://domain-model-uri/15/04}InstanceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="PlacerOrderNumber" type="{http://domain-model-uri/15/04}InstanceIdentifier"/&gt;
 *         &lt;element name="FillerOrderNumber" type="{http://domain-model-uri/15/04}InstanceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="Patient" type="{http://domain-model-uri/15/04}PersonReference"/&gt;
 *         &lt;element name="ReferringPhysician" type="{http://domain-model-uri/15/04}PersonReference" minOccurs="0"/&gt;
 *         &lt;element name="RequestingPhysician" type="{http://domain-model-uri/15/04}PersonReference" minOccurs="0"/&gt;
 *         &lt;element name="Reason" type="{http://domain-model-uri/15/04}ClinicalInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DangerCode" type="{http://domain-model-uri/15/04}CodedValue" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RelevantClinicalInfo" type="{http://domain-model-uri/15/04}ClinicalInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ImagingProcedure" type="{http://domain-model-uri/15/04}ImagingProcedure" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RequestedOrderDetail" type="{http://domain-model-uri/15/04}OrderDetail" minOccurs="0"/&gt;
 *         &lt;element name="PerformedOrderDetail" type="{http://domain-model-uri/15/04}OrderDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", propOrder = {
    "extension",
    "visitNumber",
    "placerOrderNumber",
    "fillerOrderNumber",
    "patient",
    "referringPhysician",
    "requestingPhysician",
    "reason",
    "dangerCode",
    "relevantClinicalInfo",
    "imagingProcedure",
    "requestedOrderDetail",
    "performedOrderDetail"
})
public class Order {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "VisitNumber")
    protected InstanceIdentifier visitNumber;
    @XmlElement(name = "PlacerOrderNumber", required = true)
    protected InstanceIdentifier placerOrderNumber;
    @XmlElement(name = "FillerOrderNumber")
    protected InstanceIdentifier fillerOrderNumber;
    @XmlElement(name = "Patient", required = true)
    protected PersonReference patient;
    @XmlElement(name = "ReferringPhysician")
    protected PersonReference referringPhysician;
    @XmlElement(name = "RequestingPhysician")
    protected PersonReference requestingPhysician;
    @XmlElement(name = "Reason")
    protected List<ClinicalInfo> reason;
    @XmlElement(name = "DangerCode")
    protected List<CodedValue> dangerCode;
    @XmlElement(name = "RelevantClinicalInfo")
    protected List<ClinicalInfo> relevantClinicalInfo;
    @XmlElement(name = "ImagingProcedure")
    protected List<ImagingProcedure> imagingProcedure;
    @XmlElement(name = "RequestedOrderDetail")
    protected OrderDetail requestedOrderDetail;
    @XmlElement(name = "PerformedOrderDetail")
    protected OrderDetail performedOrderDetail;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der visitNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getVisitNumber() {
        return visitNumber;
    }

    /**
     * Legt den Wert der visitNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setVisitNumber(InstanceIdentifier value) {
        this.visitNumber = value;
    }

    /**
     * Ruft den Wert der placerOrderNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    /**
     * Legt den Wert der placerOrderNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setPlacerOrderNumber(InstanceIdentifier value) {
        this.placerOrderNumber = value;
    }

    /**
     * Ruft den Wert der fillerOrderNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InstanceIdentifier }
     *     
     */
    public InstanceIdentifier getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    /**
     * Legt den Wert der fillerOrderNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InstanceIdentifier }
     *     
     */
    public void setFillerOrderNumber(InstanceIdentifier value) {
        this.fillerOrderNumber = value;
    }

    /**
     * Ruft den Wert der patient-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonReference }
     *     
     */
    public PersonReference getPatient() {
        return patient;
    }

    /**
     * Legt den Wert der patient-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonReference }
     *     
     */
    public void setPatient(PersonReference value) {
        this.patient = value;
    }

    /**
     * Ruft den Wert der referringPhysician-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonReference }
     *     
     */
    public PersonReference getReferringPhysician() {
        return referringPhysician;
    }

    /**
     * Legt den Wert der referringPhysician-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonReference }
     *     
     */
    public void setReferringPhysician(PersonReference value) {
        this.referringPhysician = value;
    }

    /**
     * Ruft den Wert der requestingPhysician-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PersonReference }
     *     
     */
    public PersonReference getRequestingPhysician() {
        return requestingPhysician;
    }

    /**
     * Legt den Wert der requestingPhysician-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonReference }
     *     
     */
    public void setRequestingPhysician(PersonReference value) {
        this.requestingPhysician = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClinicalInfo }
     * 
     * 
     */
    public List<ClinicalInfo> getReason() {
        if (reason == null) {
            reason = new ArrayList<ClinicalInfo>();
        }
        return this.reason;
    }

    /**
     * Gets the value of the dangerCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dangerCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDangerCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CodedValue }
     * 
     * 
     */
    public List<CodedValue> getDangerCode() {
        if (dangerCode == null) {
            dangerCode = new ArrayList<CodedValue>();
        }
        return this.dangerCode;
    }

    /**
     * Gets the value of the relevantClinicalInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relevantClinicalInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelevantClinicalInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClinicalInfo }
     * 
     * 
     */
    public List<ClinicalInfo> getRelevantClinicalInfo() {
        if (relevantClinicalInfo == null) {
            relevantClinicalInfo = new ArrayList<ClinicalInfo>();
        }
        return this.relevantClinicalInfo;
    }

    /**
     * Gets the value of the imagingProcedure property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the imagingProcedure property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImagingProcedure().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImagingProcedure }
     * 
     * 
     */
    public List<ImagingProcedure> getImagingProcedure() {
        if (imagingProcedure == null) {
            imagingProcedure = new ArrayList<ImagingProcedure>();
        }
        return this.imagingProcedure;
    }

    /**
     * Ruft den Wert der requestedOrderDetail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrderDetail }
     *     
     */
    public OrderDetail getRequestedOrderDetail() {
        return requestedOrderDetail;
    }

    /**
     * Legt den Wert der requestedOrderDetail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderDetail }
     *     
     */
    public void setRequestedOrderDetail(OrderDetail value) {
        this.requestedOrderDetail = value;
    }

    /**
     * Ruft den Wert der performedOrderDetail-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OrderDetail }
     *     
     */
    public OrderDetail getPerformedOrderDetail() {
        return performedOrderDetail;
    }

    /**
     * Legt den Wert der performedOrderDetail-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderDetail }
     *     
     */
    public void setPerformedOrderDetail(OrderDetail value) {
        this.performedOrderDetail = value;
    }

}
