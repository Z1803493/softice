//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * The AlertSystemState type contains the dynamic/volatile information of an alert system. See: AlertSystemDescriptor for static information.
 * 
 * <p>Java-Klasse für AlertSystemState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertSystemState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractAlertState"&gt;
 *       &lt;attribute name="ActivationState" use="required" type="{http://domain-model-uri/15/04}PausableActivation" /&gt;
 *       &lt;attribute name="LastSelfCheck" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *       &lt;attribute name="SelfCheckCount" type="{http://www.w3.org/2001/XMLSchema}long" /&gt;
 *       &lt;attribute name="PresentPhysiologicalAlarmConditions" type="{http://domain-model-uri/15/04}AlertConditionReference" /&gt;
 *       &lt;attribute name="PresentTechnicalAlarmConditions" type="{http://domain-model-uri/15/04}AlertConditionReference" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertSystemState")
public class CurrentAlertSystem
    extends AbstractAlertState
{

    @XmlAttribute(name = "ActivationState", required = true)
    protected PausableActivation state;
    @XmlAttribute(name = "LastSelfCheck")
    protected BigInteger timeOfSelfCheck;
    @XmlAttribute(name = "SelfCheckCount")
    protected Long selfCheckCnt;
    @XmlAttribute(name = "PresentPhysiologicalAlarmConditions")
    protected List<String> presentPhysiologicalAlarmConditions;
    @XmlAttribute(name = "PresentTechnicalAlarmConditions")
    protected List<String> presentTechnicalAlarmConditions;

    /**
     * Ruft den Wert der state-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PausableActivation }
     *     
     */
    public PausableActivation getState() {
        return state;
    }

    /**
     * Legt den Wert der state-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PausableActivation }
     *     
     */
    public void setState(PausableActivation value) {
        this.state = value;
    }

    /**
     * Ruft den Wert der timeOfSelfCheck-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTimeOfSelfCheck() {
        return timeOfSelfCheck;
    }

    /**
     * Legt den Wert der timeOfSelfCheck-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTimeOfSelfCheck(BigInteger value) {
        this.timeOfSelfCheck = value;
    }

    /**
     * Ruft den Wert der selfCheckCnt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSelfCheckCnt() {
        return selfCheckCnt;
    }

    /**
     * Legt den Wert der selfCheckCnt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSelfCheckCnt(Long value) {
        this.selfCheckCnt = value;
    }

    /**
     * Gets the value of the presentPhysiologicalAlarmConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the presentPhysiologicalAlarmConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPresentPhysiologicalAlarmConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPresentPhysiologicalAlarmConditions() {
        if (presentPhysiologicalAlarmConditions == null) {
            presentPhysiologicalAlarmConditions = new ArrayList<String>();
        }
        return this.presentPhysiologicalAlarmConditions;
    }

    /**
     * Gets the value of the presentTechnicalAlarmConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the presentTechnicalAlarmConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPresentTechnicalAlarmConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPresentTechnicalAlarmConditions() {
        if (presentTechnicalAlarmConditions == null) {
            presentTechnicalAlarmConditions = new ArrayList<String>();
        }
        return this.presentTechnicalAlarmConditions;
    }

}
