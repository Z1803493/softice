//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * AbstractDescriptor defines foundational meta information of any object that is included in the descriptive part of the MDIB (hence, MDDescription includes a set of AbstractDescriptor objects). Any descriptor object is derived from AbstractDescriptor. The AbstractDescriptor's counterpart is AbstractState.
 * 
 * <p>Java-Klasse für AbstractDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element name="Type" type="{http://domain-model-uri/15/04}CodedValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Handle" use="required" type="{http://domain-model-uri/15/04}Handle" /&gt;
 *       &lt;attribute name="DescriptorVersion" type="{http://domain-model-uri/15/04}VersionCounter" /&gt;
 *       &lt;attribute name="IntendedUse" type="{http://domain-model-uri/15/04}IntendedUse" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractDescriptor", propOrder = {
    "extension",
    "type"
})
@XmlSeeAlso({
    SystemContext.class,
    AbstractAlertDescriptor.class,
    MetricDescriptor.class,
    ArgumentDescriptorType.class,
    DeviceComponent.class,
    OperationDescriptor.class,
    ClockDescriptor.class,
    AbstractContextDescriptor.class
})
public class Descriptor {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "Type")
    protected CodedValue type;
    @XmlAttribute(name = "Handle", required = true)
    protected String handle;
    @XmlAttribute(name = "DescriptorVersion")
    protected BigInteger descriptorVersion;
    @XmlAttribute(name = "IntendedUse")
    protected IntendedUse intendedUse;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der type-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getType() {
        return type;
    }

    /**
     * Legt den Wert der type-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setType(CodedValue value) {
        this.type = value;
    }

    /**
     * Ruft den Wert der handle-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandle() {
        return handle;
    }

    /**
     * Legt den Wert der handle-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandle(String value) {
        this.handle = value;
    }

    /**
     * Ruft den Wert der descriptorVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDescriptorVersion() {
        return descriptorVersion;
    }

    /**
     * Legt den Wert der descriptorVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDescriptorVersion(BigInteger value) {
        this.descriptorVersion = value;
    }

    /**
     * Ruft den Wert der intendedUse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link IntendedUse }
     *     
     */
    public IntendedUse getIntendedUse() {
        return intendedUse;
    }

    /**
     * Legt den Wert der intendedUse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link IntendedUse }
     *     
     */
    public void setIntendedUse(IntendedUse value) {
        this.intendedUse = value;
    }

}
