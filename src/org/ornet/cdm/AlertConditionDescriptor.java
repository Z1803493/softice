//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * An alert condition contains the information about a potentially or actually hazardous situation. Examples: An alert limit has been exceeded or a sensor has been unplugged.
 * 
 * <p>Java-Klasse für AlertConditionDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertConditionDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractAlertDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Kind" type="{http://domain-model-uri/15/04}AlertConditionKind"/&gt;
 *         &lt;element name="Source" type="{http://domain-model-uri/15/04}HandleRef" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Priority" type="{http://domain-model-uri/15/04}AlertConditionPriority"/&gt;
 *         &lt;element name="CauseInfo" type="{http://domain-model-uri/15/04}CauseInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertConditionDescriptor", propOrder = {
    "conditionType",
    "sources",
    "priority",
    "causeInfo"
})
@XmlSeeAlso({
    LimitAlertConditionDescriptor.class
})
public class AlertConditionDescriptor
    extends AbstractAlertDescriptor
{

    @XmlElement(name = "Kind", required = true)
    @XmlSchemaType(name = "string")
    protected AlertConditionType conditionType;
    @XmlElement(name = "Source")
    protected List<String> sources;
    @XmlElement(name = "Priority", required = true)
    @XmlSchemaType(name = "string")
    protected AlertConditionPriority priority;
    @XmlElement(name = "CauseInfo")
    protected List<CauseInfo> causeInfo;

    /**
     * Ruft den Wert der conditionType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertConditionType }
     *     
     */
    public AlertConditionType getConditionType() {
        return conditionType;
    }

    /**
     * Legt den Wert der conditionType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertConditionType }
     *     
     */
    public void setConditionType(AlertConditionType value) {
        this.conditionType = value;
    }

    /**
     * Gets the value of the sources property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sources property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSources().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSources() {
        if (sources == null) {
            sources = new ArrayList<String>();
        }
        return this.sources;
    }

    /**
     * Ruft den Wert der priority-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertConditionPriority }
     *     
     */
    public AlertConditionPriority getPriority() {
        return priority;
    }

    /**
     * Legt den Wert der priority-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertConditionPriority }
     *     
     */
    public void setPriority(AlertConditionPriority value) {
        this.priority = value;
    }

    /**
     * Gets the value of the causeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the causeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCauseInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CauseInfo }
     * 
     * 
     */
    public List<CauseInfo> getCauseInfo() {
        if (causeInfo == null) {
            causeInfo = new ArrayList<CauseInfo>();
        }
        return this.causeInfo;
    }

}
