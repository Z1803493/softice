//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Argument description for an activate operation.
 * 
 * <p>Java-Klasse für ArgumentDescriptorType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ArgumentDescriptorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ArgName" type="{http://domain-model-uri/15/04}CodedValue"/&gt;
 *         &lt;element name="ArgType" type="{http://www.w3.org/2001/XMLSchema}QName"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArgumentDescriptorType", propOrder = {
    "argName",
    "argType"
})
public class ArgumentDescriptorType
    extends Descriptor
{

    @XmlElement(name = "ArgName", required = true)
    protected CodedValue argName;
    @XmlElement(name = "ArgType", required = true)
    protected QName argType;

    /**
     * Ruft den Wert der argName-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link CodedValue }
     *     
     */
    public CodedValue getArgName() {
        return argName;
    }

    /**
     * Legt den Wert der argName-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link CodedValue }
     *     
     */
    public void setArgName(CodedValue value) {
        this.argName = value;
    }

    /**
     * Ruft den Wert der argType-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link QName }
     *     
     */
    public QName getArgType() {
        return argType;
    }

    /**
     * Legt den Wert der argType-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link QName }
     *     
     */
    public void setArgType(QName value) {
        this.argType = value;
    }

}
