//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Describes a SetRange operation to alter a range property.
 * 
 * <p>Java-Klasse für SetRangeOperationDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SetRangeOperationDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractOperationDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AllowedMaxRange" type="{http://domain-model-uri/15/04}Range"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SetRangeOperationDescriptor", propOrder = {
    "allowedMaxRange"
})
public class SetRangeOperationDescriptor
    extends OperationDescriptor
{

    @XmlElement(name = "AllowedMaxRange", required = true)
    protected Range allowedMaxRange;

    /**
     * Ruft den Wert der allowedMaxRange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Range }
     *     
     */
    public Range getAllowedMaxRange() {
        return allowedMaxRange;
    }

    /**
     * Legt den Wert der allowedMaxRange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Range }
     *     
     */
    public void setAllowedMaxRange(Range value) {
        this.allowedMaxRange = value;
    }

}
