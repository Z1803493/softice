//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Base type of a context state. Every context state can be stated valid by a validator instance. Moreover, a context state's lifecycle is determinated by a start and end. AbstractContextState bundles these information.
 * 
 * <p>Java-Klasse für AbstractContextState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractContextState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Validator" type="{http://domain-model-uri/15/04}InstanceIdentifier" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="ContextAssociation" type="{http://domain-model-uri/15/04}ContextAssociation" /&gt;
 *       &lt;attribute name="BindingMDIBVersion" use="required" type="{http://domain-model-uri/15/04}ReferencedVersion" /&gt;
 *       &lt;attribute name="UnbindingMDIBVersion" type="{http://domain-model-uri/15/04}ReferencedVersion" /&gt;
 *       &lt;attribute name="BindingStartTime" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *       &lt;attribute name="BindingEndTime" type="{http://domain-model-uri/15/04}Timestamp" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractContextState", propOrder = {
    "validator"
})
@XmlSeeAlso({
    AbstractIdentifiableContextState.class
})
public class AbstractContextState
    extends State
{

    @XmlElement(name = "Validator")
    protected List<InstanceIdentifier> validator;
    @XmlAttribute(name = "ContextAssociation")
    protected ContextAssociationStateValue contextAssociation;
    @XmlAttribute(name = "BindingMDIBVersion", required = true)
    protected BigInteger bindingMDIBVersion;
    @XmlAttribute(name = "UnbindingMDIBVersion")
    protected BigInteger unbindingMDIBVersion;
    @XmlAttribute(name = "BindingStartTime")
    protected BigInteger bindingStartTime;
    @XmlAttribute(name = "BindingEndTime")
    protected BigInteger bindingEndTime;

    /**
     * Gets the value of the validator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstanceIdentifier }
     * 
     * 
     */
    public List<InstanceIdentifier> getValidator() {
        if (validator == null) {
            validator = new ArrayList<InstanceIdentifier>();
        }
        return this.validator;
    }

    /**
     * Ruft den Wert der contextAssociation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ContextAssociationStateValue }
     *     
     */
    public ContextAssociationStateValue getContextAssociation() {
        return contextAssociation;
    }

    /**
     * Legt den Wert der contextAssociation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextAssociationStateValue }
     *     
     */
    public void setContextAssociation(ContextAssociationStateValue value) {
        this.contextAssociation = value;
    }

    /**
     * Ruft den Wert der bindingMDIBVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBindingMDIBVersion() {
        return bindingMDIBVersion;
    }

    /**
     * Legt den Wert der bindingMDIBVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBindingMDIBVersion(BigInteger value) {
        this.bindingMDIBVersion = value;
    }

    /**
     * Ruft den Wert der unbindingMDIBVersion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnbindingMDIBVersion() {
        return unbindingMDIBVersion;
    }

    /**
     * Legt den Wert der unbindingMDIBVersion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnbindingMDIBVersion(BigInteger value) {
        this.unbindingMDIBVersion = value;
    }

    /**
     * Ruft den Wert der bindingStartTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBindingStartTime() {
        return bindingStartTime;
    }

    /**
     * Legt den Wert der bindingStartTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBindingStartTime(BigInteger value) {
        this.bindingStartTime = value;
    }

    /**
     * Ruft den Wert der bindingEndTime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBindingEndTime() {
        return bindingEndTime;
    }

    /**
     * Legt den Wert der bindingEndTime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBindingEndTime(BigInteger value) {
        this.bindingEndTime = value;
    }

}
