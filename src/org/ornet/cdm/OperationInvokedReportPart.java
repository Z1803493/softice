//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * A container for information on the state of the requested execution of an opeation.
 * 
 * <p>Java-Klasse für OperationInvokedReportPart complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="OperationInvokedReportPart"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://message-model-uri/15/04}AbstractReportPart"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionId" type="{http://message-model-uri/15/04}TransactionID"/&gt;
 *         &lt;element name="OperationHandleRef" type="{http://domain-model-uri/15/04}HandleRef"/&gt;
 *         &lt;element name="OperationTarget" type="{http://domain-model-uri/15/04}HandleRef" minOccurs="0"/&gt;
 *         &lt;element name="OperationState" type="{http://message-model-uri/15/04}InvocationState"/&gt;
 *         &lt;element name="OperationError" type="{http://message-model-uri/15/04}InvocationError" minOccurs="0"/&gt;
 *         &lt;element name="OperationErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationInvokedReportPart", namespace = "http://message-model-uri/15/04", propOrder = {
    "transactionId",
    "operation",
    "operationTarget",
    "operationState",
    "operationError",
    "operationErrorMessage"
})
public class OperationInvokedReportPart
    extends ReportPart
{

    @XmlElement(name = "TransactionId")
    @XmlSchemaType(name = "unsignedInt")
    protected long transactionId;
    @XmlElement(name = "OperationHandleRef", required = true)
    protected String operation;
    @XmlElement(name = "OperationTarget")
    protected String operationTarget;
    @XmlElement(name = "OperationState", required = true)
    @XmlSchemaType(name = "string")
    protected InvocationState operationState;
    @XmlElement(name = "OperationError")
    @XmlSchemaType(name = "string")
    protected InvocationError operationError;
    @XmlElement(name = "OperationErrorMessage")
    protected String operationErrorMessage;

    /**
     * Ruft den Wert der transactionId-Eigenschaft ab.
     * 
     */
    public long getTransactionId() {
        return transactionId;
    }

    /**
     * Legt den Wert der transactionId-Eigenschaft fest.
     * 
     */
    public void setTransactionId(long value) {
        this.transactionId = value;
    }

    /**
     * Ruft den Wert der operation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Legt den Wert der operation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

    /**
     * Ruft den Wert der operationTarget-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationTarget() {
        return operationTarget;
    }

    /**
     * Legt den Wert der operationTarget-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationTarget(String value) {
        this.operationTarget = value;
    }

    /**
     * Ruft den Wert der operationState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InvocationState }
     *     
     */
    public InvocationState getOperationState() {
        return operationState;
    }

    /**
     * Legt den Wert der operationState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InvocationState }
     *     
     */
    public void setOperationState(InvocationState value) {
        this.operationState = value;
    }

    /**
     * Ruft den Wert der operationError-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InvocationError }
     *     
     */
    public InvocationError getOperationError() {
        return operationError;
    }

    /**
     * Legt den Wert der operationError-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InvocationError }
     *     
     */
    public void setOperationError(InvocationError value) {
        this.operationError = value;
    }

    /**
     * Ruft den Wert der operationErrorMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationErrorMessage() {
        return operationErrorMessage;
    }

    /**
     * Legt den Wert der operationErrorMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationErrorMessage(String value) {
        this.operationErrorMessage = value;
    }

}
