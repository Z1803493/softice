//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An EnumStringMetric represents a textual status or annotation information with a constrained set of possible values.
 * 
 * Example: The current ventilation mode may be provided as an EnumStringMetric.
 * 
 * <p>Java-Klasse für EnumStringMetricDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="EnumStringMetricDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}StringMetricDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AllowedValue" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *         &lt;element name="EnumCodes" type="{http://domain-model-uri/15/04}EnumNomenRef" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnumStringMetricDescriptor", propOrder = {
    "allowedValues",
    "enumCodes"
})
public class EnumStringMetricDescriptor
    extends StringMetricDescriptor
{

    @XmlElement(name = "AllowedValue", required = true)
    protected List<String> allowedValues;
    @XmlElement(name = "EnumCodes")
    protected List<EnumNomenRef> enumCodes;

    /**
     * Gets the value of the allowedValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowedValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowedValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAllowedValues() {
        if (allowedValues == null) {
            allowedValues = new ArrayList<String>();
        }
        return this.allowedValues;
    }

    /**
     * Gets the value of the enumCodes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the enumCodes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnumCodes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnumNomenRef }
     * 
     * 
     */
    public List<EnumNomenRef> getEnumCodes() {
        if (enumCodes == null) {
            enumCodes = new ArrayList<EnumNomenRef>();
        }
        return this.enumCodes;
    }

}
