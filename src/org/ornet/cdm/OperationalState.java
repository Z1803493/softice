//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für OperatingMode.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="OperatingMode"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Dis"/&gt;
 *     &lt;enumeration value="En"/&gt;
 *     &lt;enumeration value="NA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OperatingMode")
@XmlEnum
public enum OperationalState {


    /**
     * Dis = Disabled. Object is disabled.
     * 
     */
    @XmlEnumValue("Dis")
    DISABLED("Dis"),

    /**
     * En = Enabled. Object is enabled
     * 
     */
    @XmlEnumValue("En")
    ENABLED("En"),

    /**
     * NA = Not Available. Object is not available for interaction. This means that it is defined but currently not in a mode so that it can be interacted with.
     * 
     */
    @XmlEnumValue("NA")
    NOT_AVAILABLE("NA");
    private final String value;

    OperationalState(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OperationalState fromValue(String v) {
        for (OperationalState c: OperationalState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
