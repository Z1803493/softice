//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * State of a streaming metric descriptor. Contains a list of sample values. This sample array is used to transport waveform streaming information. It is not intended to be requested by GET or EVENT REPORT.
 * 
 * <p>Java-Klasse für RealTimeSampleArrayMetricState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RealTimeSampleArrayMetricState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractMetricState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ObservedValue" type="{http://domain-model-uri/15/04}RealTimeSampleArrayValue" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RealTimeSampleArrayMetricState", propOrder = {
    "observedValue"
})
public class RealTimeSampleArrayMetricState
    extends AbstractMetricState
{

    @XmlElement(name = "ObservedValue")
    protected RealTimeSampleArrayValue observedValue;

    /**
     * Ruft den Wert der observedValue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RealTimeSampleArrayValue }
     *     
     */
    public RealTimeSampleArrayValue getObservedValue() {
        return observedValue;
    }

    /**
     * Legt den Wert der observedValue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RealTimeSampleArrayValue }
     *     
     */
    public void setObservedValue(RealTimeSampleArrayValue value) {
        this.observedValue = value;
    }

}
