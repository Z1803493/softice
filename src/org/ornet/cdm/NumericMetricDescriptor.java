//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * Specification of a MetricDescriptor type that represents a single numerical measurement and status information. An example for a numeric metric would be the heart rate measurement.
 * 
 * <p>Java-Klasse für NumericMetricDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="NumericMetricDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractMetricDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://domain-model-uri/15/04}Resolution"/&gt;
 *         &lt;element name="MeasurePeriod" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *         &lt;element name="AveragingPeriod" type="{http://www.w3.org/2001/XMLSchema}duration" minOccurs="0"/&gt;
 *         &lt;element name="TechnicalRange" type="{http://domain-model-uri/15/04}Range" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NumericMetricDescriptor", propOrder = {
    "resolution",
    "measurePeriod",
    "averagingPeriod",
    "technicalRanges"
})
public class NumericMetricDescriptor
    extends MetricDescriptor
{

    @XmlElement(name = "Resolution", required = true)
    protected BigDecimal resolution;
    @XmlElement(name = "MeasurePeriod")
    protected Duration measurePeriod;
    @XmlElement(name = "AveragingPeriod")
    protected Duration averagingPeriod;
    @XmlElement(name = "TechnicalRange")
    protected List<Range> technicalRanges;

    /**
     * The resolution of the means to determine the metric's value. The resolution is the minimum determinable difference between two observed values. A negative value indicates that the resolution could not be determined.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getResolution() {
        return resolution;
    }

    /**
     * Legt den Wert der resolution-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setResolution(BigDecimal value) {
        this.resolution = value;
    }

    /**
     * Ruft den Wert der measurePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMeasurePeriod() {
        return measurePeriod;
    }

    /**
     * Legt den Wert der measurePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMeasurePeriod(Duration value) {
        this.measurePeriod = value;
    }

    /**
     * Ruft den Wert der averagingPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAveragingPeriod() {
        return averagingPeriod;
    }

    /**
     * Legt den Wert der averagingPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAveragingPeriod(Duration value) {
        this.averagingPeriod = value;
    }

    /**
     * Gets the value of the technicalRanges property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the technicalRanges property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTechnicalRanges().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Range }
     * 
     * 
     */
    public List<Range> getTechnicalRanges() {
        if (technicalRanges == null) {
            technicalRanges = new ArrayList<Range>();
        }
        return this.technicalRanges;
    }

}
