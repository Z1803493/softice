//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * An alert system groups alert conditions and their related alert signals. It therefore holds a list of AlertConditionDescriptor and AlertSignalDescriptor elements. It detects alert conditions and - as appropriate - generates alert signals.
 * 
 * <p>Java-Klasse für AlertSystemDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertSystemDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractAlertDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AlertCondition" type="{http://domain-model-uri/15/04}AlertConditionDescriptor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AlertSignal" type="{http://domain-model-uri/15/04}AlertSignalDescriptor" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MaxPhysiologicalAlarmListEntries" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="MaxTechnicalAlarmListEntries" type="{http://www.w3.org/2001/XMLSchema}int" /&gt;
 *       &lt;attribute name="SelfCheckPeriod" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertSystemDescriptor", propOrder = {
    "alertConditions",
    "alertSignals"
})
public class AlertSystemDescriptor
    extends AbstractAlertDescriptor
{

    @XmlElement(name = "AlertCondition")
    protected List<AlertConditionDescriptor> alertConditions;
    @XmlElement(name = "AlertSignal")
    protected List<AlertSignalDescriptor> alertSignals;
    @XmlAttribute(name = "MaxPhysiologicalAlarmListEntries")
    protected Integer maxPhysiologicalAlarmListEntries;
    @XmlAttribute(name = "MaxTechnicalAlarmListEntries")
    protected Integer maxTechnicalAlarmListEntries;
    @XmlAttribute(name = "SelfCheckPeriod")
    protected Duration selfCheckPeriod;

    /**
     * Gets the value of the alertConditions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alertConditions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlertConditions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlertConditionDescriptor }
     * 
     * 
     */
    public List<AlertConditionDescriptor> getAlertConditions() {
        if (alertConditions == null) {
            alertConditions = new ArrayList<AlertConditionDescriptor>();
        }
        return this.alertConditions;
    }

    /**
     * Gets the value of the alertSignals property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the alertSignals property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAlertSignals().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AlertSignalDescriptor }
     * 
     * 
     */
    public List<AlertSignalDescriptor> getAlertSignals() {
        if (alertSignals == null) {
            alertSignals = new ArrayList<AlertSignalDescriptor>();
        }
        return this.alertSignals;
    }

    /**
     * Ruft den Wert der maxPhysiologicalAlarmListEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxPhysiologicalAlarmListEntries() {
        return maxPhysiologicalAlarmListEntries;
    }

    /**
     * Legt den Wert der maxPhysiologicalAlarmListEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxPhysiologicalAlarmListEntries(Integer value) {
        this.maxPhysiologicalAlarmListEntries = value;
    }

    /**
     * Ruft den Wert der maxTechnicalAlarmListEntries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaxTechnicalAlarmListEntries() {
        return maxTechnicalAlarmListEntries;
    }

    /**
     * Legt den Wert der maxTechnicalAlarmListEntries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaxTechnicalAlarmListEntries(Integer value) {
        this.maxTechnicalAlarmListEntries = value;
    }

    /**
     * Ruft den Wert der selfCheckPeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getSelfCheckPeriod() {
        return selfCheckPeriod;
    }

    /**
     * Legt den Wert der selfCheckPeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setSelfCheckPeriod(Duration value) {
        this.selfCheckPeriod = value;
    }

}
