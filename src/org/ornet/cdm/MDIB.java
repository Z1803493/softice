//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The Medical Device information Base (MDIB) is the root object of the Domain Information Model (DIM). It comprises the capability description of the medical device(s) it represents in the MDDescription (descriptive part) as well as the current status in MDState (state part).
 * 			
 * 
 * <p>Java-Klasse für MDIB complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MDIB"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element name="MDDescription" type="{http://domain-model-uri/15/04}MDDescription"/&gt;
 *         &lt;element name="MDState" type="{http://domain-model-uri/15/04}MDState"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute ref="{http://domain-model-uri/15/04}MDIBVersion use="required""/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MDIB", propOrder = {
    "extension",
    "description",
    "states"
})
public class MDIB {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "MDDescription", required = true)
    protected MDDescription description;
    @XmlElement(name = "MDState", required = true)
    protected MDState states;
    @XmlAttribute(name = "MDIBVersion", namespace = "http://domain-model-uri/15/04", required = true)
    protected BigInteger sequenceNumber;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der description-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MDDescription }
     *     
     */
    public MDDescription getDescription() {
        return description;
    }

    /**
     * Legt den Wert der description-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MDDescription }
     *     
     */
    public void setDescription(MDDescription value) {
        this.description = value;
    }

    /**
     * Ruft den Wert der states-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MDState }
     *     
     */
    public MDState getStates() {
        return states;
    }

    /**
     * Legt den Wert der states-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MDState }
     *     
     */
    public void setStates(MDState value) {
        this.states = value;
    }

    /**
     * Ruft den Wert der sequenceNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Legt den Wert der sequenceNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNumber(BigInteger value) {
        this.sequenceNumber = value;
    }

}
