//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.ornet.cdm package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CodingSystemId_QNAME = new QName("http://domain-model-uri/15/04", "CodingSystemId");
    private final static QName _CodingSystemName_QNAME = new QName("http://domain-model-uri/15/04", "CodingSystemName");
    private final static QName _VersionId_QNAME = new QName("http://domain-model-uri/15/04", "VersionId");
    private final static QName _CodeId_QNAME = new QName("http://domain-model-uri/15/04", "CodeId");
    private final static QName _Type_QNAME = new QName("http://domain-model-uri/15/04", "Type");
    private final static QName _Unit_QNAME = new QName("http://domain-model-uri/15/04", "Unit");
    private final static QName _BodySite_QNAME = new QName("http://domain-model-uri/15/04", "BodySite");
    private final static QName _Resolution_QNAME = new QName("http://domain-model-uri/15/04", "Resolution");
    private final static QName _MetricCategory_QNAME = new QName("http://domain-model-uri/15/04", "MetricCategory");
    private final static QName _Availability_QNAME = new QName("http://domain-model-uri/15/04", "Availability");
    private final static QName _MDIBContainer_QNAME = new QName("http://domain-model-uri/15/04", "MDIBContainer");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.ornet.cdm
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LocalizedText }
     * 
     */
    public LocalizedText createLocalizedText() {
        return new LocalizedText();
    }

    /**
     * Create an instance of {@link CodedValue }
     * 
     */
    public CodedValue createCodedValue() {
        return new CodedValue();
    }

    /**
     * Create an instance of {@link MDIBContainmentTree }
     * 
     */
    public MDIBContainmentTree createMDIBContainmentTree() {
        return new MDIBContainmentTree();
    }

    /**
     * Create an instance of {@link MDIB }
     * 
     */
    public MDIB createMDIB() {
        return new MDIB();
    }

    /**
     * Create an instance of {@link InstanceIdentifier }
     * 
     */
    public InstanceIdentifier createInstanceIdentifier() {
        return new InstanceIdentifier();
    }

    /**
     * Create an instance of {@link CodedWithEquivalents }
     * 
     */
    public CodedWithEquivalents createCodedWithEquivalents() {
        return new CodedWithEquivalents();
    }

    /**
     * Create an instance of {@link CauseInfo }
     * 
     */
    public CauseInfo createCauseInfo() {
        return new CauseInfo();
    }

    /**
     * Create an instance of {@link RemedyInfo }
     * 
     */
    public RemedyInfo createRemedyInfo() {
        return new RemedyInfo();
    }

    /**
     * Create an instance of {@link Range }
     * 
     */
    public Range createRange() {
        return new Range();
    }

    /**
     * Create an instance of {@link Measure }
     * 
     */
    public Measure createMeasure() {
        return new Measure();
    }

    /**
     * Create an instance of {@link Descriptor }
     * 
     */
    public Descriptor createDescriptor() {
        return new Descriptor();
    }

    /**
     * Create an instance of {@link State }
     * 
     */
    public State createState() {
        return new State();
    }

    /**
     * Create an instance of {@link MDDescription }
     * 
     */
    public MDDescription createMDDescription() {
        return new MDDescription();
    }

    /**
     * Create an instance of {@link MDState }
     * 
     */
    public MDState createMDState() {
        return new MDState();
    }

    /**
     * Create an instance of {@link ProductionSpecification }
     * 
     */
    public ProductionSpecification createProductionSpecification() {
        return new ProductionSpecification();
    }

    /**
     * Create an instance of {@link SystemMetaData }
     * 
     */
    public SystemMetaData createSystemMetaData() {
        return new SystemMetaData();
    }

    /**
     * Create an instance of {@link SystemContext }
     * 
     */
    public SystemContext createSystemContext() {
        return new SystemContext();
    }

    /**
     * Create an instance of {@link DeviceComponent }
     * 
     */
    public DeviceComponent createDeviceComponent() {
        return new DeviceComponent();
    }

    /**
     * Create an instance of {@link MDSDescriptor }
     * 
     */
    public MDSDescriptor createMDSDescriptor() {
        return new MDSDescriptor();
    }

    /**
     * Create an instance of {@link HydraMDSDescriptor }
     * 
     */
    public HydraMDSDescriptor createHydraMDSDescriptor() {
        return new HydraMDSDescriptor();
    }

    /**
     * Create an instance of {@link MDSState }
     * 
     */
    public MDSState createMDSState() {
        return new MDSState();
    }

    /**
     * Create an instance of {@link HydraMDSState }
     * 
     */
    public HydraMDSState createHydraMDSState() {
        return new HydraMDSState();
    }

    /**
     * Create an instance of {@link AbstractAlertDescriptor }
     * 
     */
    public AbstractAlertDescriptor createAbstractAlertDescriptor() {
        return new AbstractAlertDescriptor();
    }

    /**
     * Create an instance of {@link AlertSystemDescriptor }
     * 
     */
    public AlertSystemDescriptor createAlertSystemDescriptor() {
        return new AlertSystemDescriptor();
    }

    /**
     * Create an instance of {@link AlertConditionDescriptor }
     * 
     */
    public AlertConditionDescriptor createAlertConditionDescriptor() {
        return new AlertConditionDescriptor();
    }

    /**
     * Create an instance of {@link AlertSignalDescriptor }
     * 
     */
    public AlertSignalDescriptor createAlertSignalDescriptor() {
        return new AlertSignalDescriptor();
    }

    /**
     * Create an instance of {@link LimitAlertConditionDescriptor }
     * 
     */
    public LimitAlertConditionDescriptor createLimitAlertConditionDescriptor() {
        return new LimitAlertConditionDescriptor();
    }

    /**
     * Create an instance of {@link AbstractAlertState }
     * 
     */
    public AbstractAlertState createAbstractAlertState() {
        return new AbstractAlertState();
    }

    /**
     * Create an instance of {@link CurrentAlertCondition }
     * 
     */
    public CurrentAlertCondition createCurrentAlertCondition() {
        return new CurrentAlertCondition();
    }

    /**
     * Create an instance of {@link CurrentAlertSignal }
     * 
     */
    public CurrentAlertSignal createCurrentAlertSignal() {
        return new CurrentAlertSignal();
    }

    /**
     * Create an instance of {@link CurrentAlertSystem }
     * 
     */
    public CurrentAlertSystem createCurrentAlertSystem() {
        return new CurrentAlertSystem();
    }

    /**
     * Create an instance of {@link CurrentLimitAlertCondition }
     * 
     */
    public CurrentLimitAlertCondition createCurrentLimitAlertCondition() {
        return new CurrentLimitAlertCondition();
    }

    /**
     * Create an instance of {@link VMDDescriptor }
     * 
     */
    public VMDDescriptor createVMDDescriptor() {
        return new VMDDescriptor();
    }

    /**
     * Create an instance of {@link ChannelDescriptor }
     * 
     */
    public ChannelDescriptor createChannelDescriptor() {
        return new ChannelDescriptor();
    }

    /**
     * Create an instance of {@link MetricDescriptor }
     * 
     */
    public MetricDescriptor createMetricDescriptor() {
        return new MetricDescriptor();
    }

    /**
     * Create an instance of {@link NumericMetricDescriptor }
     * 
     */
    public NumericMetricDescriptor createNumericMetricDescriptor() {
        return new NumericMetricDescriptor();
    }

    /**
     * Create an instance of {@link StringMetricDescriptor }
     * 
     */
    public StringMetricDescriptor createStringMetricDescriptor() {
        return new StringMetricDescriptor();
    }

    /**
     * Create an instance of {@link EnumNomenRef }
     * 
     */
    public EnumNomenRef createEnumNomenRef() {
        return new EnumNomenRef();
    }

    /**
     * Create an instance of {@link EnumStringMetricDescriptor }
     * 
     */
    public EnumStringMetricDescriptor createEnumStringMetricDescriptor() {
        return new EnumStringMetricDescriptor();
    }

    /**
     * Create an instance of {@link RealTimeSampleArrayMetricDescriptor }
     * 
     */
    public RealTimeSampleArrayMetricDescriptor createRealTimeSampleArrayMetricDescriptor() {
        return new RealTimeSampleArrayMetricDescriptor();
    }

    /**
     * Create an instance of {@link ComponentState }
     * 
     */
    public ComponentState createComponentState() {
        return new ComponentState();
    }

    /**
     * Create an instance of {@link CalibrationInfo }
     * 
     */
    public CalibrationInfo createCalibrationInfo() {
        return new CalibrationInfo();
    }

    /**
     * Create an instance of {@link AbstractMetricState }
     * 
     */
    public AbstractMetricState createAbstractMetricState() {
        return new AbstractMetricState();
    }

    /**
     * Create an instance of {@link NumericMetricState }
     * 
     */
    public NumericMetricState createNumericMetricState() {
        return new NumericMetricState();
    }

    /**
     * Create an instance of {@link RealTimeSampleArrayMetricState }
     * 
     */
    public RealTimeSampleArrayMetricState createRealTimeSampleArrayMetricState() {
        return new RealTimeSampleArrayMetricState();
    }

    /**
     * Create an instance of {@link StringMetricState }
     * 
     */
    public StringMetricState createStringMetricState() {
        return new StringMetricState();
    }

    /**
     * Create an instance of {@link EnumStringMetricState }
     * 
     */
    public EnumStringMetricState createEnumStringMetricState() {
        return new EnumStringMetricState();
    }

    /**
     * Create an instance of {@link MeasurementState }
     * 
     */
    public MeasurementState createMeasurementState() {
        return new MeasurementState();
    }

    /**
     * Create an instance of {@link Annotation }
     * 
     */
    public Annotation createAnnotation() {
        return new Annotation();
    }

    /**
     * Create an instance of {@link AbstractMetricValue }
     * 
     */
    public AbstractMetricValue createAbstractMetricValue() {
        return new AbstractMetricValue();
    }

    /**
     * Create an instance of {@link NumericValue }
     * 
     */
    public NumericValue createNumericValue() {
        return new NumericValue();
    }

    /**
     * Create an instance of {@link RealTimeSampleArrayValue }
     * 
     */
    public RealTimeSampleArrayValue createRealTimeSampleArrayValue() {
        return new RealTimeSampleArrayValue();
    }

    /**
     * Create an instance of {@link StringMetricValue }
     * 
     */
    public StringMetricValue createStringMetricValue() {
        return new StringMetricValue();
    }

    /**
     * Create an instance of {@link ArgumentDescriptorType }
     * 
     */
    public ArgumentDescriptorType createArgumentDescriptorType() {
        return new ArgumentDescriptorType();
    }

    /**
     * Create an instance of {@link SCODescriptor }
     * 
     */
    public SCODescriptor createSCODescriptor() {
        return new SCODescriptor();
    }

    /**
     * Create an instance of {@link OperationDescriptor }
     * 
     */
    public OperationDescriptor createOperationDescriptor() {
        return new OperationDescriptor();
    }

    /**
     * Create an instance of {@link SetValueOperationDescriptor }
     * 
     */
    public SetValueOperationDescriptor createSetValueOperationDescriptor() {
        return new SetValueOperationDescriptor();
    }

    /**
     * Create an instance of {@link SetStringOperationDescriptor }
     * 
     */
    public SetStringOperationDescriptor createSetStringOperationDescriptor() {
        return new SetStringOperationDescriptor();
    }

    /**
     * Create an instance of {@link ActivateOperationDescriptor }
     * 
     */
    public ActivateOperationDescriptor createActivateOperationDescriptor() {
        return new ActivateOperationDescriptor();
    }

    /**
     * Create an instance of {@link NonGenericOperationDescriptor }
     * 
     */
    public NonGenericOperationDescriptor createNonGenericOperationDescriptor() {
        return new NonGenericOperationDescriptor();
    }

    /**
     * Create an instance of {@link SetAlertStateOperationDescriptor }
     * 
     */
    public SetAlertStateOperationDescriptor createSetAlertStateOperationDescriptor() {
        return new SetAlertStateOperationDescriptor();
    }

    /**
     * Create an instance of {@link SetRangeOperationDescriptor }
     * 
     */
    public SetRangeOperationDescriptor createSetRangeOperationDescriptor() {
        return new SetRangeOperationDescriptor();
    }

    /**
     * Create an instance of {@link SetContextOperationDescriptor }
     * 
     */
    public SetContextOperationDescriptor createSetContextOperationDescriptor() {
        return new SetContextOperationDescriptor();
    }

    /**
     * Create an instance of {@link OperationState }
     * 
     */
    public OperationState createOperationState() {
        return new OperationState();
    }

    /**
     * Create an instance of {@link SetValueOperationState }
     * 
     */
    public SetValueOperationState createSetValueOperationState() {
        return new SetValueOperationState();
    }

    /**
     * Create an instance of {@link SetRangeOperationState }
     * 
     */
    public SetRangeOperationState createSetRangeOperationState() {
        return new SetRangeOperationState();
    }

    /**
     * Create an instance of {@link ClockDescriptor }
     * 
     */
    public ClockDescriptor createClockDescriptor() {
        return new ClockDescriptor();
    }

    /**
     * Create an instance of {@link ClockState }
     * 
     */
    public ClockState createClockState() {
        return new ClockState();
    }

    /**
     * Create an instance of {@link AbstractContextDescriptor }
     * 
     */
    public AbstractContextDescriptor createAbstractContextDescriptor() {
        return new AbstractContextDescriptor();
    }

    /**
     * Create an instance of {@link LocationContextDescriptor }
     * 
     */
    public LocationContextDescriptor createLocationContextDescriptor() {
        return new LocationContextDescriptor();
    }

    /**
     * Create an instance of {@link EnsembleContextDescriptor }
     * 
     */
    public EnsembleContextDescriptor createEnsembleContextDescriptor() {
        return new EnsembleContextDescriptor();
    }

    /**
     * Create an instance of {@link OperatorContextDescriptor }
     * 
     */
    public OperatorContextDescriptor createOperatorContextDescriptor() {
        return new OperatorContextDescriptor();
    }

    /**
     * Create an instance of {@link WorkflowContextDescriptor }
     * 
     */
    public WorkflowContextDescriptor createWorkflowContextDescriptor() {
        return new WorkflowContextDescriptor();
    }

    /**
     * Create an instance of {@link AbstractContextState }
     * 
     */
    public AbstractContextState createAbstractContextState() {
        return new AbstractContextState();
    }

    /**
     * Create an instance of {@link AbstractIdentifiableContextState }
     * 
     */
    public AbstractIdentifiableContextState createAbstractIdentifiableContextState() {
        return new AbstractIdentifiableContextState();
    }

    /**
     * Create an instance of {@link LocationContextState }
     * 
     */
    public LocationContextState createLocationContextState() {
        return new LocationContextState();
    }

    /**
     * Create an instance of {@link EnsembleContextState }
     * 
     */
    public EnsembleContextState createEnsembleContextState() {
        return new EnsembleContextState();
    }

    /**
     * Create an instance of {@link WorkflowContextState }
     * 
     */
    public WorkflowContextState createWorkflowContextState() {
        return new WorkflowContextState();
    }

    /**
     * Create an instance of {@link OperatorContextState }
     * 
     */
    public OperatorContextState createOperatorContextState() {
        return new OperatorContextState();
    }

    /**
     * Create an instance of {@link PatientAssociationDescriptor }
     * 
     */
    public PatientAssociationDescriptor createPatientAssociationDescriptor() {
        return new PatientAssociationDescriptor();
    }

    /**
     * Create an instance of {@link BaseDemographics }
     * 
     */
    public BaseDemographics createBaseDemographics() {
        return new BaseDemographics();
    }

    /**
     * Create an instance of {@link PatientDemographicsCoreData }
     * 
     */
    public PatientDemographicsCoreData createPatientDemographicsCoreData() {
        return new PatientDemographicsCoreData();
    }

    /**
     * Create an instance of {@link PersonReference }
     * 
     */
    public PersonReference createPersonReference() {
        return new PersonReference();
    }

    /**
     * Create an instance of {@link PersonParticipation }
     * 
     */
    public PersonParticipation createPersonParticipation() {
        return new PersonParticipation();
    }

    /**
     * Create an instance of {@link NeonatalPatientDemographicsCoreData }
     * 
     */
    public NeonatalPatientDemographicsCoreData createNeonatalPatientDemographicsCoreData() {
        return new NeonatalPatientDemographicsCoreData();
    }

    /**
     * Create an instance of {@link PatientContextState }
     * 
     */
    public PatientContextState createPatientContextState() {
        return new PatientContextState();
    }

    /**
     * Create an instance of {@link ContainmentTree }
     * 
     */
    public ContainmentTree createContainmentTree() {
        return new ContainmentTree();
    }

    /**
     * Create an instance of {@link ContainmentTreeEntry }
     * 
     */
    public ContainmentTreeEntry createContainmentTreeEntry() {
        return new ContainmentTreeEntry();
    }

    /**
     * Create an instance of {@link ClinicalInfo }
     * 
     */
    public ClinicalInfo createClinicalInfo() {
        return new ClinicalInfo();
    }

    /**
     * Create an instance of {@link Order }
     * 
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Create an instance of {@link OrderDetail }
     * 
     */
    public OrderDetail createOrderDetail() {
        return new OrderDetail();
    }

    /**
     * Create an instance of {@link ImagingProcedure }
     * 
     */
    public ImagingProcedure createImagingProcedure() {
        return new ImagingProcedure();
    }

    /**
     * Create an instance of {@link Extension }
     * 
     */
    public Extension createExtension() {
        return new Extension();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "CodingSystemId")
    public JAXBElement<String> createCodingSystemId(String value) {
        return new JAXBElement<String>(_CodingSystemId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalizedText }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "CodingSystemName")
    public JAXBElement<LocalizedText> createCodingSystemName(LocalizedText value) {
        return new JAXBElement<LocalizedText>(_CodingSystemName_QNAME, LocalizedText.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "VersionId")
    public JAXBElement<String> createVersionId(String value) {
        return new JAXBElement<String>(_VersionId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "CodeId")
    public JAXBElement<String> createCodeId(String value) {
        return new JAXBElement<String>(_CodeId_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodedValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "Type")
    public JAXBElement<CodedValue> createType(CodedValue value) {
        return new JAXBElement<CodedValue>(_Type_QNAME, CodedValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodedValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "Unit")
    public JAXBElement<CodedValue> createUnit(CodedValue value) {
        return new JAXBElement<CodedValue>(_Unit_QNAME, CodedValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodedValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "BodySite")
    public JAXBElement<CodedValue> createBodySite(CodedValue value) {
        return new JAXBElement<CodedValue>(_BodySite_QNAME, CodedValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "Resolution")
    public JAXBElement<BigDecimal> createResolution(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Resolution_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetricCategory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "MetricCategory")
    public JAXBElement<MetricCategory> createMetricCategory(MetricCategory value) {
        return new JAXBElement<MetricCategory>(_MetricCategory_QNAME, MetricCategory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetricAvailability }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "Availability")
    public JAXBElement<MetricAvailability> createAvailability(MetricAvailability value) {
        return new JAXBElement<MetricAvailability>(_Availability_QNAME, MetricAvailability.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MDIB }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://domain-model-uri/15/04", name = "MDIBContainer")
    public JAXBElement<MDIB> createMDIBContainer(MDIB value) {
        return new JAXBElement<MDIB>(_MDIBContainer_QNAME, MDIB.class, null, value);
    }

}
