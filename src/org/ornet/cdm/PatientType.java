//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für PatientType.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="PatientType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unspec"/&gt;
 *     &lt;enumeration value="Ad"/&gt;
 *     &lt;enumeration value="Ped"/&gt;
 *     &lt;enumeration value="Neo"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "PatientType")
@XmlEnum
public enum PatientType {


    /**
     * Unspec = Unspecified. Unspecified type.
     * 
     */
    @XmlEnumValue("Unspec")
    UNSPECIFIED("Unspec"),

    /**
     * Ad = Adult. Indicates an adult patient.
     * 
     */
    @XmlEnumValue("Ad")
    ADULT("Ad"),

    /**
     * Ped = Pediatric. Indicates a pediatric patient.
     * 
     */
    @XmlEnumValue("Ped")
    PEDIATRIC("Ped"),

    /**
     * Neo = Neonatal. Indicates a neonatal patient.
     * 
     */
    @XmlEnumValue("Neo")
    NEONATAL("Neo");
    private final String value;

    PatientType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PatientType fromValue(String v) {
        for (PatientType c: PatientType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
