//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:22:07 PM CEST 
//


package org.ornet.cdm;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Abstract super class of all setter operation response messages.
 * 
 * <p>Java-Klasse für AbstractSetResponse complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AbstractSetResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://extension-point-uri/15/03}Extension" minOccurs="0"/&gt;
 *         &lt;element name="TransactionId" type="{http://message-model-uri/15/04}TransactionID"/&gt;
 *         &lt;element name="InvocationState" type="{http://message-model-uri/15/04}InvocationState"/&gt;
 *         &lt;element name="OperationError" type="{http://message-model-uri/15/04}InvocationError" minOccurs="0"/&gt;
 *         &lt;element name="OperationErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute ref="{http://domain-model-uri/15/04}MDIBVersion use="required""/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractSetResponse", namespace = "http://message-model-uri/15/04", propOrder = {
    "extension",
    "transactionId",
    "invocationState",
    "operationError",
    "operationErrorMessage"
})
@XmlSeeAlso({
    SetContextStateResponse.class,
    SetPatientStateResponse.class,
    ActivateResponse.class,
    SetAlertStateResponse.class,
    SetStringResponse.class,
    SetValueResponse.class,
    SetRangeResponse.class
})
public class AbstractSetResponse {

    @XmlElement(name = "Extension", namespace = "http://extension-point-uri/15/03")
    protected Extension extension;
    @XmlElement(name = "TransactionId")
    @XmlSchemaType(name = "unsignedInt")
    protected long transactionId;
    @XmlElement(name = "InvocationState", required = true)
    @XmlSchemaType(name = "string")
    protected InvocationState invocationState;
    @XmlElement(name = "OperationError")
    @XmlSchemaType(name = "string")
    protected InvocationError operationError;
    @XmlElement(name = "OperationErrorMessage")
    protected String operationErrorMessage;
    @XmlAttribute(name = "MDIBVersion", namespace = "http://domain-model-uri/15/04", required = true)
    protected BigInteger sequenceNumber;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Extension }
     *     
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Extension }
     *     
     */
    public void setExtension(Extension value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der transactionId-Eigenschaft ab.
     * 
     */
    public long getTransactionId() {
        return transactionId;
    }

    /**
     * Legt den Wert der transactionId-Eigenschaft fest.
     * 
     */
    public void setTransactionId(long value) {
        this.transactionId = value;
    }

    /**
     * Ruft den Wert der invocationState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InvocationState }
     *     
     */
    public InvocationState getInvocationState() {
        return invocationState;
    }

    /**
     * Legt den Wert der invocationState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InvocationState }
     *     
     */
    public void setInvocationState(InvocationState value) {
        this.invocationState = value;
    }

    /**
     * Ruft den Wert der operationError-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link InvocationError }
     *     
     */
    public InvocationError getOperationError() {
        return operationError;
    }

    /**
     * Legt den Wert der operationError-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link InvocationError }
     *     
     */
    public void setOperationError(InvocationError value) {
        this.operationError = value;
    }

    /**
     * Ruft den Wert der operationErrorMessage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationErrorMessage() {
        return operationErrorMessage;
    }

    /**
     * Legt den Wert der operationErrorMessage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationErrorMessage(String value) {
        this.operationErrorMessage = value;
    }

    /**
     * Ruft den Wert der sequenceNumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Legt den Wert der sequenceNumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNumber(BigInteger value) {
        this.sequenceNumber = value;
    }

}
