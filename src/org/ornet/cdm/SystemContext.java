//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The context of an MDS that lists the possible relationship of a device into its usage environment by means of context descriptors. Context descriptors do not contain any stateful information. They only assert that the underlying MDS can provide corresponding context state information.
 * 
 * <p>Java-Klasse für SystemContext complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SystemContext"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://domain-model-uri/15/04}AbstractDescriptor"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PatientContext" type="{http://domain-model-uri/15/04}PatientContextDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="LocationContext" type="{http://domain-model-uri/15/04}LocationContextDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="EnsembleContext" type="{http://domain-model-uri/15/04}EnsembleContextDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="OperatorContext" type="{http://domain-model-uri/15/04}OperatorContextDescriptor" minOccurs="0"/&gt;
 *         &lt;element name="WorkflowContext" type="{http://domain-model-uri/15/04}WorkflowContextDescriptor" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SystemContext", propOrder = {
    "patientContext",
    "locationContext",
    "ensembleContext",
    "operatorContext",
    "workflowContext"
})
public class SystemContext
    extends Descriptor
{

    @XmlElement(name = "PatientContext")
    protected PatientAssociationDescriptor patientContext;
    @XmlElement(name = "LocationContext")
    protected LocationContextDescriptor locationContext;
    @XmlElement(name = "EnsembleContext")
    protected EnsembleContextDescriptor ensembleContext;
    @XmlElement(name = "OperatorContext")
    protected OperatorContextDescriptor operatorContext;
    @XmlElement(name = "WorkflowContext")
    protected WorkflowContextDescriptor workflowContext;

    /**
     * Ruft den Wert der patientContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PatientAssociationDescriptor }
     *     
     */
    public PatientAssociationDescriptor getPatientContext() {
        return patientContext;
    }

    /**
     * Legt den Wert der patientContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientAssociationDescriptor }
     *     
     */
    public void setPatientContext(PatientAssociationDescriptor value) {
        this.patientContext = value;
    }

    /**
     * Ruft den Wert der locationContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link LocationContextDescriptor }
     *     
     */
    public LocationContextDescriptor getLocationContext() {
        return locationContext;
    }

    /**
     * Legt den Wert der locationContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationContextDescriptor }
     *     
     */
    public void setLocationContext(LocationContextDescriptor value) {
        this.locationContext = value;
    }

    /**
     * Ruft den Wert der ensembleContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link EnsembleContextDescriptor }
     *     
     */
    public EnsembleContextDescriptor getEnsembleContext() {
        return ensembleContext;
    }

    /**
     * Legt den Wert der ensembleContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnsembleContextDescriptor }
     *     
     */
    public void setEnsembleContext(EnsembleContextDescriptor value) {
        this.ensembleContext = value;
    }

    /**
     * Ruft den Wert der operatorContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link OperatorContextDescriptor }
     *     
     */
    public OperatorContextDescriptor getOperatorContext() {
        return operatorContext;
    }

    /**
     * Legt den Wert der operatorContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link OperatorContextDescriptor }
     *     
     */
    public void setOperatorContext(OperatorContextDescriptor value) {
        this.operatorContext = value;
    }

    /**
     * Ruft den Wert der workflowContext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link WorkflowContextDescriptor }
     *     
     */
    public WorkflowContextDescriptor getWorkflowContext() {
        return workflowContext;
    }

    /**
     * Legt den Wert der workflowContext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link WorkflowContextDescriptor }
     *     
     */
    public void setWorkflowContext(WorkflowContextDescriptor value) {
        this.workflowContext = value;
    }

}
