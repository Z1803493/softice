//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.09.25 um 01:23:33 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für MetricCategory.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="MetricCategory"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Unspec"/&gt;
 *     &lt;enumeration value="Msrmt"/&gt;
 *     &lt;enumeration value="Clc"/&gt;
 *     &lt;enumeration value="Set"/&gt;
 *     &lt;enumeration value="Preset"/&gt;
 *     &lt;enumeration value="Rcmm"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MetricCategory")
@XmlEnum
public enum MetricCategory {


    /**
     * Unspec = Unspecified. Non of the following categories is valid for the metric.
     * 
     */
    @XmlEnumValue("Unspec")
    UNSPECIFIED("Unspec"),

    /**
     * Msrmt = Measurement. The metric has been derived by measurement.
     * 
     */
    @XmlEnumValue("Msrmt")
    MEASUREMENT("Msrmt"),

    /**
     * Clc = Calculation. The metric has been derived by calculation only.
     * 
     */
    @XmlEnumValue("Clc")
    CALCULATION("Clc"),

    /**
     * Set = Setting. The metric has a value that is adjustable by some (local or remote) control means.
     * 
     */
    @XmlEnumValue("Set")
    SETTING("Set"),

    /**
     * Preset = Presetting. The metric has a value that is adjustable by some (local or remote) control means. It modifies other values only temporarily for preview purposes until it is commited the related metric from the Setting category. 
     * 
     */
    @XmlEnumValue("Preset")
    PRESETTING("Preset"),

    /**
     * Rcmm = Recommendation. The metric is a proposol for a Setting or Presetting.
     * 
     */
    @XmlEnumValue("Rcmm")
    RECOMMENDATION("Rcmm");
    private final String value;

    MetricCategory(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MetricCategory fromValue(String v) {
        for (MetricCategory c: MetricCategory.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
