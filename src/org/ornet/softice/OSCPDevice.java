/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.util.HashMap;
import java.util.Iterator;
import org.yads.java.communication.AutoBindingFactory;
import org.yads.java.communication.CommunicationManagerRegistry;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPDiscoveryDomain;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.communication.structures.DiscoveryAutoBinding;
import org.yads.java.communication.structures.IPDiscoveryAutoBinding;
import org.yads.java.communication.structures.IPDiscoveryBinding;
import org.yads.java.service.DefaultDevice;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.types.QName;
import org.yads.java.types.QNameSet;

public final class OSCPDevice extends DefaultDevice {

    public static final String EVENTREPORTWSDL = "eventreport.wsdl";
    public static final String WAVEREPORTWSDL = "wavereport.wsdl";
    public static final String PHISERVICEWSDL = "phiservice.wsdl";
    public static final String SETSERVICEWSDL = "setservice.wsdl";
    public static final String GETSERVICEWSDL = "getservice.wsdl";
    
    private boolean initialized = false;
    private String manufacturer;
    private String modelName;
    private String friendlyName;
    private OSCPEventService streamService;
    private final int port;

    public OSCPDevice() {
        super(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
        friendlyName = "OR.NET device";
        modelName = "OR.NET model";
        manufacturer = "OR.NET consortium";
        port = SoftICE.getInstance().extractNextPort();
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }
    
    public void init(HashMap<String, HashMap<String, InvokeDelegate>> invokeDelegates, 
            HashMap<String, HashMap<String, EventDelegate>> eventDelegates) 
            throws IllegalArgumentException, UnsupportedOperationException, Exception {
        if (initialized) {
            throw new Exception("OSCP device already initilized!");
        }
        initialized = true;
        QNameSet qns = new QNameSet();
        qns.add(new QName("MedicalDevice", "http://www.draeger.com/projects/DSC/CMDM/2012/05"));
        qns.add(new QName("MedicalDevice", "http://message-model-uri/15/04"));
        setPortTypes(qns);
        this.addFriendlyName("en-US", friendlyName);
        this.addModelName("en-US", modelName);
        this.addManufacturer("en-US", manufacturer);
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        final String bi = SoftICE.getInstance().getBindInterface();
        while (adrIter.hasNext()) {
            IPAddress next = adrIter.next();
            if (bi == null || next.getAddressWithoutNicId().startsWith(bi) || bi.equals("0.0.0.0")) {
                HTTPBinding binding = new HTTPBinding(next, port, getEndpointReference().getAddress().toString(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
                this.addBinding(binding);                            
            }
        }		        
        Iterator discDoms = IPNetworkDetection.getInstance().getAllAvailableDiscoveryDomains();
        while (discDoms.hasNext()) {
            final IPDiscoveryBinding ipDiscoveryBinding = new IPDiscoveryBinding(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, (IPDiscoveryDomain) discDoms.next()); 
            if (!ipDiscoveryBinding.getHostIPAddress().isIPv6())
                this.addBinding(ipDiscoveryBinding);
        }    
        AutoBindingFactory abf = CommunicationManagerRegistry.getCommunicationManager(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID).getAutoBindingFactory();
        if (abf != null) {
            final String[] networkInterfaceNames = IPNetworkDetection.getInstance().getNetworkInterfaceNames();
            DiscoveryAutoBinding dab = abf.createDiscoveryMulticastAutoBinding(networkInterfaceNames, new String [] {IPDiscoveryAutoBinding.IPADDRESS_FAMILY_IPV4}, true);
            addOutgoingDiscoveryInfo(dab);
        }                            
        createServices(invokeDelegates, eventDelegates);
    }

    private void createServices(HashMap<String, HashMap<String, InvokeDelegate>> invokeDelegates, HashMap<String, HashMap<String, EventDelegate>> eventDelegates) {
        if (invokeDelegates.containsKey(GETSERVICEWSDL))
            addService(new OSCPDefaultService(GETSERVICEWSDL, invokeDelegates.get(GETSERVICEWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "GetService";
                }
            });
        if (invokeDelegates.containsKey(SETSERVICEWSDL))
            addService(new OSCPDefaultService(SETSERVICEWSDL, invokeDelegates.get(SETSERVICEWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "SetService";
                }
            });
        if (invokeDelegates.containsKey(PHISERVICEWSDL))
            addService(new OSCPMixedService(PHISERVICEWSDL, invokeDelegates.get(PHISERVICEWSDL), eventDelegates.get(PHISERVICEWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "PHI";
                }
            });        
        if (eventDelegates.containsKey(EVENTREPORTWSDL))
            addService(new OSCPEventService(EVENTREPORTWSDL, eventDelegates.get(EVENTREPORTWSDL), SoftICE.getInstance().extractNextPort()) {
                @Override
                public String getOSCPServiceId() {
                    return "EventReport";
                }
            });         
        streamService = new OSCPEventService(WAVEREPORTWSDL, eventDelegates.get(WAVEREPORTWSDL), SoftICE.getInstance().extractNextPort()) {
            @Override
            public String getOSCPServiceId() {
                return "WaveformEventReport";
            }
        };
        if (eventDelegates.containsKey(WAVEREPORTWSDL))
            addService(streamService);         
    }

    public OSCPEventService getStreamService() {
        return streamService;
    }
    
}
