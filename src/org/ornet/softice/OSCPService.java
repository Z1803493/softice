/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.communication.protocol.http.HTTPBinding;

import org.yads.java.io.fs.FileResource;
import org.yads.java.service.DefaultService;
import org.yads.java.types.ContentType;
import org.yads.java.types.URI;

public abstract class OSCPService extends DefaultService {

    public static final String MESSAGEMODEL_NAMESPACE = "http://message-model-uri/15/04";

    public static final String EXTENSION_POINTXSD = "/org/ornet/softice/resources/ExtensionPoint.xsd";
    public static final String BICEPS__MESSAGE_MODELXSD = "/org/ornet/softice/resources/BICEPS_MessageModel.xsd";
    public static final String BICEPS__DOMAIN_MODELXSD = "/org/ornet/softice/resources/BICEPS_DomainModel.xsd";
    public static final String WSDL_RESOURCE_BASE_PATH = "/org/ornet/softice/resources/";
    
    protected final String wsdl;
    private final int port;
    
    public OSCPService(String wsdl, String communicationManagerId, int port) {
        super(communicationManagerId);
        this.wsdl = wsdl;
        this.port = port;
    }
       
    public abstract String getOSCPServiceId();

    @Override
    public synchronized void start() throws IOException {
        setServiceId(new URI(getOSCPServiceId()));
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        final String bi = SoftICE.getInstance().getBindInterface();        
        while (adrIter.hasNext()) {
            IPAddress next = adrIter.next();
            if (bi == null || next.getAddressWithoutNicId().startsWith(bi) || bi.equals("0.0.0.0")) {
                HTTPBinding binding = new HTTPBinding(next, port, getServiceId().toString(), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
                this.addBinding(binding);                
            }
        }        
        super.start();
        try {
            String wsdlPath = WSDL_RESOURCE_BASE_PATH + wsdl;
            final FileResource fileResourceWSDL = new FileResource(wsdlPath, "description.wsdl");
            fileResourceWSDL.setContentType(new ContentType("application", "xml", "file"));
            // No generated WSDL: override an existing WSDL resource with the fixed name "description.wsdl"
            registerFileResource(fileResourceWSDL, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceDomainModel = new FileResource(BICEPS__DOMAIN_MODELXSD);
            fileResourceDomainModel.setContentType(new ContentType("application", "xml", "file"));
            // No generated schema: add original XSD schema file resources
            registerFileResource(fileResourceDomainModel, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceMessageModel = new FileResource(BICEPS__MESSAGE_MODELXSD);
            fileResourceMessageModel.setContentType(new ContentType("application", "xml", "file"));
            registerFileResource(fileResourceMessageModel, MESSAGEMODEL_NAMESPACE);
            final FileResource fileResourceExtensionPoint = new FileResource(EXTENSION_POINTXSD);
            fileResourceExtensionPoint.setContentType(new ContentType("application", "xml", "file"));
            registerFileResource(fileResourceExtensionPoint, MESSAGEMODEL_NAMESPACE);
        } catch (Exception ex) {
            Logger.getLogger(OSCPService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  

}
