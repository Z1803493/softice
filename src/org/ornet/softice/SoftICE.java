/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.ornet.mdpws.MDPWSStreamingManager;
import org.yads.java.YADSFramework;
import org.yads.java.io.fs.FileResource;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.util.Log;

public class SoftICE {

    private static SoftICE instance;
    private static JAXBUtil jaxbUtil;
    private final AtomicInteger portStart = new AtomicInteger(20000);
    private static final AtomicBoolean schemaValidation = new AtomicBoolean(false);
    private String bindInterface;
    
    static class ValidationHandler implements ValidationEventHandler {
        
        @Override
        public boolean handleEvent(ValidationEvent event) {
            StringBuilder sb = new StringBuilder();
            sb.append("XML schema validation alert");
            sb.append("\nSEVERITY:  ").append(event.getSeverity());
            sb.append("\nMESSAGE:  ").append(event.getMessage());
            sb.append("\nLINKED EXCEPTION:  ").append(event.getLinkedException());
            sb.append("\nLOCATOR");
            sb.append("\n    LINE NUMBER:  ").append(event.getLocator().getLineNumber());
            sb.append("\n    COLUMN NUMBER:  ").append(event.getLocator().getColumnNumber());
            sb.append("\n    OFFSET:  ").append(event.getLocator().getOffset());
            sb.append("\n    OBJECT:  ").append(event.getLocator().getObject());
            sb.append("\n    NODE:  ").append(event.getLocator().getNode());
            sb.append("\n    URL:  ").append(event.getLocator().getURL());
            Log.error(sb.toString());
            return false;
        }
        
    }    
    
    private SoftICE() {
    }
    
    public static SoftICE getInstance() {
        if (instance == null) {
            Log.info("SoftICE 0.91b (C) SurgiTAIX AG");
            instance = new SoftICE();
            System.setProperty("com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl.fastBoot", "true");
            Log.info("Initializing class loader context...");
            try {
                jaxbUtil = JAXBUtil.getInstance();
                jaxbUtil.setSkipParameterValueParsing(true);
                jaxbUtil.setStaticContext(JAXBContext.newInstance("org.ornet.cdm"));
                if (schemaValidation.get()) {
                    enableSchemaValidation();
                }
                else {
                    disableSchemaValidation();
                }
            } catch (JAXBException ex) {
                Logger.getLogger(SoftICE.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            MDPWSStreamingManager.getInstance().init();
        }
        return instance;
    }
    
    private static void enableSchemaValidation() {
        try {
            Log.info("Building schema...");
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);            
            List<Source> sources = new ArrayList<>();
            sources.add(new StreamSource(FileResource.class.getResourceAsStream(OSCPService.EXTENSION_POINTXSD)));
            sources.add(new StreamSource(FileResource.class.getResourceAsStream(OSCPService.BICEPS__DOMAIN_MODELXSD)));
            sources.add(new StreamSource(FileResource.class.getResourceAsStream(OSCPService.BICEPS__MESSAGE_MODELXSD)));
            Schema schema = sf.newSchema(sources.toArray(new Source[0]));
            jaxbUtil.setValidationEventHandler(new ValidationHandler(), schema);
            Log.info("Schema validation is enabled.");
        } catch (Exception ex) {
            Logger.getLogger(SoftICE.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void disableSchemaValidation() {
        jaxbUtil.setValidationEventHandler(null, null);
    }
    
    public void setSchemaValidationEnabled(boolean validate) {
        schemaValidation.set(validate);
        if (jaxbUtil != null) {
            if (validate) {
                enableSchemaValidation();
            }
            else {
                disableSchemaValidation();
            }
        }
    }

    public boolean isSchemaValidationEnabled() {
        return schemaValidation.get();
    }
    
    public int extractNextPort() {
        return portStart.incrementAndGet();
    }

    public void setPortStart(int portStart) {
        this.portStart.set(portStart);
    }   
    
    public void startup() {
        startup(Log.DEBUG_LEVEL_INFO, false);
    }

    public void setBindInterface(String bindInterface) {
        this.bindInterface = bindInterface;
    }

    public String getBindInterface() {
        return bindInterface;
    }
    
    public void startup(int logLevel, boolean logStackTrace) {
        Log.setLogLevel(logLevel);
        Log.setLogStackTrace(logStackTrace);
        Log.info("SoftICE startup...");
        YADSFramework.start(null);
    }
    
    public void shutdown() {
        Log.info("SoftICE shutdown...");
        final MDPWSStreamingManager sm = MDPWSStreamingManager.getInstance();
        sm.removeAllStreamListeners();
        sm.removeAllStreamSenders();
        YADSFramework.stop();
    }
    
}
