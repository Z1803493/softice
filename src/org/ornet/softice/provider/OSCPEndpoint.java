/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.util.List;
import org.ornet.cdm.MDDescription;
import org.ornet.cdm.MDIB;
import org.ornet.cdm.MDState;

/**
 *
 * @author besting
 */
public interface OSCPEndpoint {

    /**
     * Get deep clone of MD description.
     *
     * @return The description.
     */
    MDDescription getMDDescription();

    /**
     * Create an MDIB container object. Modifications of this structure will not be reflected into the internal MDIB representation.
     *
     * @return The MDIB.
     */
    MDIB getMDIB();

    /**
     * Get deep clone of all MD states.
     *
     * @return The states.
     */
    MDState getMDState();

    /**
     * Get deep clone of MD states.
     *
     * @param handles List of handles to match.
     * @return The states.
     */
    MDState getMDState(List<String> handles);
    
    /**
     * Get ID of endpoint
     *
     * @return the ID.
     */
    String getReferenceId();
    
}
