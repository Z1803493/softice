/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.yads.java.service.DefaultEventSource;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.EventSourceStub;
import org.yads.java.service.ServiceSubscription;
import org.yads.java.service.parameter.ParameterValue;

public class OSCPEventDelegate implements EventDelegate {

    private EventSourceStub eventSourceStub;

    public EventSourceStub getEventSourceStub() {
        return eventSourceStub;
    }

    public void setEventSourceStub(EventSourceStub eventSourceStub) {
        this.eventSourceStub = eventSourceStub;
    }
    
    @Override
    public void solicitResponseReceived(DefaultEventSource event, ParameterValue paramValue, int eventNumber, ServiceSubscription subscription) {
    }    
   
}
