/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.ornet.cdm.InvocationState;
import org.ornet.cdm.State;

public abstract class OSCPProviderHandler<T extends State> {
    
    protected OSCPProvider provider;
    private final String descriptorHandle;

    public OSCPProviderHandler(String descriptorHandle) {
        this.descriptorHandle = descriptorHandle;
    }

    public void updateState(T state) {
        provider.updateState(state);
    }  
    
    protected void setProvider(OSCPProvider provider) {
        this.provider = provider;
    }  
   
    public void notifyOperationInvoked(OperationInvocationContext oic, InvocationState is) {
        provider.notifyOperationInvoked(oic, is, null);
    }    
    
    public String getDescriptorHandle() {
        return descriptorHandle;            
    }
    
}
