/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.math.BigInteger;
import java.util.List;
import org.ornet.cdm.GetContextStates;
import org.ornet.cdm.GetContextStatesResponse;
import org.ornet.cdm.GetMDDescriptionResponse;
import org.ornet.cdm.GetMDIBResponse;
import org.ornet.cdm.GetMDState;
import org.ornet.cdm.GetMDStateResponse;
import org.ornet.cdm.MDIB;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.service.Operation;
import org.yads.java.service.parameter.ParameterValue;

public class ProviderGetOperationFactory {
    
    static InvokeDelegate createGetMDIBOperation(final OSCPProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetMDIBResponse response = new GetMDIBResponse();
            final MDIB mdib = provider.getMDIB();
            response.setMDIB(mdib);
            response.setSequenceNumber(mdib.getSequenceNumber());
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }

    static InvokeDelegate createGetMDDescriptionOperation(final OSCPProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetMDDescriptionResponse response = new GetMDDescriptionResponse();
            response.setStaticDescription(provider.getMDDescription());
            response.setSequenceNumber(BigInteger.valueOf(provider.getMdibVersion()));
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }

    static InvokeDelegate createGetMDStateOperation(final OSCPProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetMDState request = JAXBUtil.getInstance().createInputParameterValue(arguments, GetMDState.class);
            List<String> handles = request.getHandles();
            GetMDStateResponse response = new GetMDStateResponse();
            response.setSequenceNumber(BigInteger.valueOf(provider.getMdibVersion()));
            response.setMDState(handles == null || handles.isEmpty()? provider.getMDState() : provider.getMDState(handles));
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }
    
    static InvokeDelegate createGetContextsOperation(final OSCPProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetContextStates request = JAXBUtil.getInstance().createInputParameterValue(arguments, GetContextStates.class);
            List<String> handles = request.getHandles();
            GetContextStatesResponse response = new GetContextStatesResponse();
            response.setSequenceNumber(BigInteger.valueOf(provider.getMdibVersion()));
            response.getContextStates().addAll(provider.getContextStates(handles));
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }       
    
}
