/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.ornet.cdm.AbstractAlertDescriptor;
import org.ornet.cdm.AbstractContextDescriptor;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.ActivateOperationDescriptor;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.ComponentActivation;
import org.ornet.cdm.Descriptor;
import org.ornet.cdm.HydraMDSDescriptor;
import org.ornet.cdm.MDDescription;
import org.ornet.cdm.MDSDescriptor;
import org.ornet.cdm.MDState;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.MetricDescriptor;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.OperationDescriptor;
import org.ornet.cdm.OperationState;
import org.ornet.cdm.OperationalState;
import org.ornet.cdm.SCODescriptor;
import org.ornet.cdm.SetAlertStateOperationDescriptor;
import org.ornet.cdm.SetContextOperationDescriptor;
import org.ornet.cdm.SetStringOperationDescriptor;
import org.ornet.cdm.SetValueOperationDescriptor;
import org.ornet.cdm.State;
import org.ornet.cdm.StringMetricDescriptor;
import org.ornet.cdm.VMDDescriptor;
import org.yads.java.structures.MaxHashMap;

public class OSCPToolbox {
        
    private static final Map<String, String> operationTargetCache = Collections.synchronizedMap(new MaxHashMap<>(64));
    private static final Map<String, String> operationHandleCache = Collections.synchronizedMap(new MaxHashMap<>(64));
    
    private static String getKey(OSCPEndpoint ep, String value) {
        return ep.getReferenceId() + ":" + value;
    }
    
    public static String getOperationTargetForOperationHandle(OSCPEndpoint ep, String operationHandle) {
        String res;
        if ((res = operationTargetCache.get(getKey(ep, operationHandle))) != null)
            return res;
        MDDescription mdd = ep.getMDDescription();
        for (MDSDescriptor nextMDS : mdd.getMDSDescriptors()) {
            if (!(nextMDS instanceof HydraMDSDescriptor))
                continue;
            HydraMDSDescriptor hMDS = (HydraMDSDescriptor)nextMDS;
            if (hMDS.getSCO() == null)
                continue;
            for (OperationDescriptor op : hMDS.getSCO().getOperations()) {
                if (op.getHandle().equals(operationHandle)) {
                    operationTargetCache.put(getKey(ep, operationHandle), op.getOperationTarget());
                    return op.getOperationTarget();
                }
            }
        }
        return null;
    }   
    
    public static String getFirstOperationHandleForOperationTarget(OSCPEndpoint ep, String operationTarget) {
        String res;
        if ((res = operationHandleCache.get(getKey(ep, operationTarget))) != null)
            return res;
        MDDescription mdd = ep.getMDDescription();
        for (MDSDescriptor nextMDS : mdd.getMDSDescriptors()) {
            if (!(nextMDS instanceof HydraMDSDescriptor))
                continue;
            HydraMDSDescriptor hMDS = (HydraMDSDescriptor)nextMDS;
            if (hMDS.getSCO() == null)
                continue;
            for (OperationDescriptor op : hMDS.getSCO().getOperations()) {
                if (op.getOperationTarget().equals(operationTarget)) {
                    operationHandleCache.put(getKey(ep, operationTarget), op.getHandle());
                    return op.getHandle();
                }
            }
        }
        return null;
    }    

    public static MetricDescriptor findMetricDescritor(OSCPEndpoint ep, String handle) {
        MDDescription mdd = ep.getMDDescription();
        for (MDSDescriptor nextMDS : mdd.getMDSDescriptors()) {
            if (!(nextMDS instanceof HydraMDSDescriptor))
                continue;
            HydraMDSDescriptor hMDS = (HydraMDSDescriptor)nextMDS;
            for (VMDDescriptor nextVmd : hMDS.getVMDs())
                for (ChannelDescriptor nextChn : nextVmd.getChannels())
                    for (MetricDescriptor nextMet : nextChn.getMetrics())  
                        if (nextMet.getHandle().equals(handle))
                            return nextMet;
        }
        return null;
    }
    
    public static AbstractMetricState findMetricState(OSCPEndpoint ep, String handle) {
        return findState(ep, handle, AbstractMetricState.class);
    }
    
    public static AbstractContextState findContextState(OSCPEndpoint ep, String handle) {
        return findState(ep, handle, AbstractContextState.class);
    }

    public static <T extends State> T findState(OSCPEndpoint ep, String handle, Class<T> type) {
        List<String> handles = new ArrayList<>();
        handles.add(handle);
        MDState mdState = ep.getMDState(handles);
        List<State> stateList = mdState.getStates();
        if (stateList.isEmpty())
            return null;
        return type.cast(stateList.get(0));
    }    
    
    public static boolean isMetricChangeAllowed(OSCPEndpoint ep, AbstractMetricState state) {
        MetricDescriptor md = findMetricDescritor(ep, state.getReferencedDescriptor());       
        if (md == null)
            return false;
        if (md.getCategory() == MetricCategory.MEASUREMENT)
            return false;
        ComponentActivation ca = state.getState();
        if (ca != null)
            return ca.equals(ComponentActivation.ON);
        return true;
    }
    
    static abstract class AlertDescriptorGrabber<T extends AbstractAlertDescriptor>{
        abstract List<T> getAlertDescriptors(AlertSystemDescriptor as);
    }
    
    private static <T extends AbstractAlertDescriptor> Set<T> collectAllAlertDescriptors(OSCPEndpoint ep, AlertDescriptorGrabber<T> adg) {
        Set<T> descriptors = new HashSet<>();
        MDDescription mdd = ep.getMDDescription();
        for (MDSDescriptor mds : mdd.getMDSDescriptors()) {
            if (!(mds instanceof HydraMDSDescriptor))
                continue;
            HydraMDSDescriptor hmds = (HydraMDSDescriptor)mds;
            if (hmds.getAlertSystem() != null)
                descriptors.addAll(adg.getAlertDescriptors(hmds.getAlertSystem()));
            for (VMDDescriptor vmd : hmds.getVMDs()) {
                if (vmd.getAlertSystem() != null)
                    descriptors.addAll(adg.getAlertDescriptors(vmd.getAlertSystem()));
                for (ChannelDescriptor chd : vmd.getChannels()) {
                    if (chd.getAlertSystem() != null)
                        descriptors.addAll(adg.getAlertDescriptors(chd.getAlertSystem()));
                }                 
            }                
        }
        return descriptors;
    }
    
    public static Set<AlertConditionDescriptor> collectAllAlertConditionDescriptors(OSCPEndpoint ep) {
        return collectAllAlertDescriptors(ep, new AlertDescriptorGrabber<AlertConditionDescriptor>() {
            @Override
            List<AlertConditionDescriptor> getAlertDescriptors(AlertSystemDescriptor as) {
                return as.getAlertConditions();
            }
        });
    }
    
    public static Set<AlertSignalDescriptor> collectAllAlertSignalDescriptors(OSCPEndpoint ep) {
        return collectAllAlertDescriptors(ep, new AlertDescriptorGrabber<AlertSignalDescriptor>() {
            @Override
            List<AlertSignalDescriptor> getAlertDescriptors(AlertSystemDescriptor as) {
                return as.getAlertSignals();
            }
        });
    }    
    
    protected static void createOperationDescriptor(List<OperationState> operationStates, Descriptor descr, HydraMDSDescriptor mds) {
        SCODescriptor sco = mds.getSCO();
        if (sco == null) {
            sco = new SCODescriptor();
            sco.setHandle(mds.getHandle() + "_sco");
            mds.setSCO(sco);
        }
        List<OperationDescriptor> opDescr = sco.getOperations();
        boolean opDescrFound = false;
        for (OperationDescriptor nextopDescr : opDescr) {
            if (nextopDescr.getOperationTarget().equals(descr.getHandle()))
                opDescrFound = true;
        }
        if (!opDescrFound) {
            OSCPToolbox.createOperationDescriptor(descr, opDescr);
        }
        synchronized(operationStates) {
            Iterator<OperationState> it = operationStates.iterator();
            while (it.hasNext()) {
                OperationState next = it.next();
                if (next.getReferencedDescriptor().equals(descr.getHandle()))
                    return;
            }            
        }
        OperationState os = new OperationState();
        os.setReferencedDescriptor(descr.getHandle() + "_sco");
        os.setState(OperationalState.ENABLED);
        operationStates.add(os);
    }

    private static void createOperationDescriptor(Descriptor descr, List<OperationDescriptor> opDescr) {
        if (descr instanceof NumericMetricDescriptor) {
            SetValueOperationDescriptor setOpDescr = new SetValueOperationDescriptor();
            setOpDescr.setHandle(descr.getHandle() + "_sco");
            setOpDescr.setOperationTarget(descr.getHandle());
            opDescr.add(setOpDescr);
        }
        else if (descr instanceof StringMetricDescriptor) {
            SetStringOperationDescriptor setOpDescr = new SetStringOperationDescriptor();
            setOpDescr.setHandle(descr.getHandle() + "_sco");
            setOpDescr.setOperationTarget(descr.getHandle());
            opDescr.add(setOpDescr);
        }
        else if (descr instanceof AbstractContextDescriptor) {
            SetContextOperationDescriptor setOpDescr = new SetContextOperationDescriptor();
            setOpDescr.setHandle(descr.getHandle() + "_sco");
            setOpDescr.setOperationTarget(descr.getHandle());
            opDescr.add(setOpDescr);
        }
        else if (descr instanceof AbstractAlertDescriptor) {
            SetAlertStateOperationDescriptor setOpDescr = new SetAlertStateOperationDescriptor();
            setOpDescr.setHandle(descr.getHandle() + "_sco");
            setOpDescr.setOperationTarget(descr.getHandle());
            opDescr.add(setOpDescr);
        }
        else if (descr instanceof ActivateOperationDescriptor) {
            opDescr.add((ActivateOperationDescriptor)descr);
        }
    }    

}
