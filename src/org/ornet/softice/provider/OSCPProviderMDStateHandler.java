/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import com.rits.cloning.Cloner;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.State;

public abstract class OSCPProviderMDStateHandler<T extends State> extends OSCPProviderHandler<T> {
    
    public OSCPProviderMDStateHandler(String descriptorHandle) {
        super(descriptorHandle);
    }
      
    public InvocationState onStateChangeRequest(T state, OperationInvocationContext oic) {
        return InvocationState.FAILED;
    }    
    
    protected final State getInitalClonedState() {
        Cloner cloner = new Cloner();
        State cloned = cloner.deepClone(getInitialState());
        return cloned;
    }
    
    protected abstract T getInitialState();
    
}
