/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.math.BigInteger;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AlertReportPart;
import org.ornet.cdm.ContextChangedReportPart;
import org.ornet.cdm.EpisodicAlertReport;
import org.ornet.cdm.EpisodicContextChangedReport;
import org.ornet.cdm.EpisodicMetricReport;
import org.ornet.cdm.InvocationError;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MetricReportPart;
import org.ornet.cdm.OperationInvokedReport;
import org.ornet.cdm.OperationInvokedReportPart;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.parameter.ParameterValue;

public class ProviderEventSourceFactory {
    
    public static OSCPEventDelegate episodicMetricReportSource;
    public static OSCPEventDelegate episodicContextReportSource;
    public static OSCPEventDelegate operationInvokedReportSource;
    public static OSCPEventDelegate waveformStreamReportSource;
    public static OSCPEventDelegate episodicAlertReportSource;
    
    static EventDelegate createEpisodicMetricReportSource(final OSCPProvider provider) {
        episodicMetricReportSource = new OSCPEventDelegate();
        return episodicMetricReportSource;
    }  
    
    static EventDelegate createOperationInvokedReportSource(final OSCPProvider provider) {
        operationInvokedReportSource = new OSCPEventDelegate();
        return operationInvokedReportSource;
    }       
    
    static EventDelegate createEpisodicContextReportSource(final OSCPProvider provider) {
        episodicContextReportSource = new OSCPEventDelegate();
        return episodicContextReportSource;
    }
    
    static EventDelegate createStreamReportSource(final OSCPProvider provider) {
        waveformStreamReportSource = new OSCPEventDelegate();
        return waveformStreamReportSource;
    } 
    
    static EventDelegate createEpisodicAlertReportSource(final OSCPProvider provider) {
        episodicAlertReportSource = new OSCPEventDelegate();
        return episodicAlertReportSource;
    }     

    static synchronized void fireEpisodicMetricEventReport(AbstractMetricState state, BigInteger mdibVersion) {
        MetricReportPart mrp = new MetricReportPart();
        mrp.getMetrics().add(state);
        EpisodicMetricReport report = new EpisodicMetricReport();
        report.getReportParts().add(mrp);
        report.setSequenceNumber(mdibVersion);
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(report);
        episodicMetricReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);        
    }
    
    static synchronized void fireEpisodicAlertEventReport(AbstractAlertState state, BigInteger mdibVersion) {
        AlertReportPart arp = new AlertReportPart();
        arp.getAlertStates().add(state);
        EpisodicAlertReport report = new EpisodicAlertReport();
        report.getReportParts().add(arp);
        report.setSequenceNumber(mdibVersion);
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(report);
        episodicAlertReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);     
    }    

    static synchronized void fireOperationInvokedReport(OSCPProvider provider, OperationInvocationContext oic, InvocationState is, BigInteger mdibVersion, String operationErrorMsg) {
        OperationInvokedReportPart orp = new OperationInvokedReportPart();
        orp.setTransactionId(oic.getTransactionId());
        orp.setOperation(oic.getOperationHandle());
        orp.setOperationTarget(OSCPToolbox.getOperationTargetForOperationHandle(provider, oic.getOperationHandle()));
        orp.setOperationState(is);
        if (operationErrorMsg != null) {
            orp.setOperationError(InvocationError.OTHER);
            orp.setOperationErrorMessage(operationErrorMsg);
        }
        OperationInvokedReport oir = new OperationInvokedReport();
        oir.getReportDetails().add(orp);
        oir.setSequenceNumber(mdibVersion);
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(oir);
        operationInvokedReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);            
    }

    static synchronized void fireEpisodicContextChangedReport(AbstractContextState acs, BigInteger mdibVersion) {
        ContextChangedReportPart crp = new ContextChangedReportPart();
        if (acs.getHandle() != null)
            crp.getChangedContextStates().add(acs.getHandle());
        else
            crp.getChangedContextStates().add(acs.getReferencedDescriptor());
        EpisodicContextChangedReport ecr = new EpisodicContextChangedReport();
        ecr.getReportParts().add(crp);
        ecr.setSequenceNumber(mdibVersion);
        ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(ecr);        
        episodicContextReportSource.getEventSourceStub().fire(pv, 0, CredentialInfo.EMPTY_CREDENTIAL_INFO);    
    }

}
