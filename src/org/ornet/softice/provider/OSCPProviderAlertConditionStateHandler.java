/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.CurrentAlertCondition;
import org.ornet.cdm.CurrentAlertSignal;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.SignalPresence;

public abstract class OSCPProviderAlertConditionStateHandler<T extends CurrentAlertCondition> extends OSCPProviderMDStateHandler<T> {

    public OSCPProviderAlertConditionStateHandler(String descriptorHandle) {
        super(descriptorHandle);
    }
    
    public void sourceHasChanged(AbstractMetricState source, CurrentAlertCondition currentCondition) {
    }
    
    public void setAlertConditionPresence(CurrentAlertCondition currentState, boolean trigger) {
        // Modify and update condition state in internal MDIB
        currentState.setPresence(trigger);
        if (onStateChangeRequest((T)currentState, null) == InvocationState.FINISHED) {
            updateState((T)currentState);
        } else {
            return;
        }
        
        // Search all alert signal descriptors with reference to the alert condition descriptor handler
        Map<String, Boolean> signalHandlesLatching = new HashMap<>();
        Set<AlertSignalDescriptor> signalDescriptors = OSCPToolbox.collectAllAlertSignalDescriptors(provider);
        for (AlertSignalDescriptor next : signalDescriptors) {
            if (next.getConditionSignaled().equals(currentState.getReferencedDescriptor())) {
                signalHandlesLatching.put(next.getHandle(), next.isIsLatching());
            }
        }
        
        // Search all relevant signal states
        List<AbstractAlertState> alertStates = provider.getAlertStates(Arrays.asList(signalHandlesLatching.keySet().toArray(new String [0])));
        for (AbstractAlertState next : alertStates) {
            if (!(next instanceof CurrentAlertSignal))
                continue;
            CurrentAlertSignal cas = (CurrentAlertSignal)next;
            if (trigger) {
                inform(cas, SignalPresence.ON);
            } else {
                if (cas.getPresence() == null) {
                    inform(cas, SignalPresence.OFF);
                }
                else if (signalHandlesLatching.get(cas.getReferencedDescriptor())) { // is latching?
                    switch(cas.getPresence()) {
                        case ON: inform(cas, SignalPresence.LATCHED); break;
                        default: break;
                    }                        
                }
                else {
                    inform(cas, SignalPresence.OFF);
                }
            }
        }
    }    

    private void inform(CurrentAlertSignal cas, SignalPresence signalPresence) {
        cas.setPresence(signalPresence);
        OSCPProviderHandler handler = provider.getHandler(cas.getReferencedDescriptor());
        if (handler == null || !(handler instanceof OSCPProviderMDStateHandler)) {
            Logger.getLogger(OSCPProvider.class.getName()).log(Level.SEVERE, "Error in updating alert signals. Handler missing for: {0}", cas.getReferencedDescriptor());
            return;
        }
        OSCPProviderMDStateHandler stateHandler = (OSCPProviderMDStateHandler)handler;
        if (stateHandler.onStateChangeRequest(cas, null) == InvocationState.FINISHED) {
            stateHandler.updateState(cas);
        }
    }
    
}
