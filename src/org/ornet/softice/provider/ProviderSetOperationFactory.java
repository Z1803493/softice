/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */

package org.ornet.softice.provider;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractSet;
import org.ornet.cdm.AbstractSetResponse;
import org.ornet.cdm.Activate;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.OperationState;
import org.ornet.cdm.OperationalState;
import org.ornet.cdm.SetAlertState;
import org.ornet.cdm.SetContextState;
import org.ornet.cdm.SetString;
import org.ornet.cdm.SetValue;
import org.ornet.cdm.State;
import org.ornet.cdm.StringMetricState;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.service.Operation;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.structures.MaxHashMap;

public class ProviderSetOperationFactory {
    
    private static class SetableStateHandler {
        public OSCPProviderMDStateHandler handler;
        public State state;
    }
    
    private static final AtomicLong transactionId = new AtomicLong();
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    private static final LinkedBlockingQueue<SetNotification> queue = new LinkedBlockingQueue<>();
    private static final Map<String, SetableStateHandler> checkStateCache = Collections.synchronizedMap(new MaxHashMap<>(64));
    
    public static void init() {
        if (executor.isShutdown())
            return;
        executor.execute(new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        SetNotification sn = queue.poll(100, TimeUnit.MILLISECONDS);
                        if (sn == null)
                            continue;
                        if (super.isInterrupted())
                            break;
                        Object request = sn.getRequest();
                        if (request instanceof SetValue) {
                            trySetValue(sn.getProvider(), (SetValue)request, sn.getContext());
                        }
                        else if (request instanceof SetString) {
                            trySetString(sn.getProvider(), (SetString)request, sn.getContext());
                        }                        
                        else if (request instanceof SetContextState) {
                            trySetContext(sn.getProvider(), (SetContextState)request, sn.getContext());
                        }                        
                        else if (request instanceof SetAlertState) {
                            trySetAlert(sn.getProvider(), (SetAlertState)request, sn.getContext());
                        }                        
                        else if (request instanceof Activate) {
                            tryActivate(sn.getProvider(), (Activate)request, sn.getContext());
                        }                        
                    } catch (InterruptedException ie) {
                        break;
                    } 
                    catch (Exception ex) {
                        Logger.getLogger(OSCPProvider.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
    }
    
    protected static void shutdown() {
        executor.shutdownNow();
    }
 
    private static class SetNotification<R> {
        private final R request;
        private final OperationInvocationContext context;
        private final OSCPProvider provider;

        public SetNotification(OSCPProvider provider, R request, OperationInvocationContext context) {
            this.provider = provider;
            this.request = request;
            this.context = context;
        }

        public R getRequest() {
            return request;
        }

        public OperationInvocationContext getContext() {
            return context;
        }

        public OSCPProvider getProvider() {
            return provider;
        }
    }
    
    protected static void updateSetableStateCache(OSCPProvider provider, State state) {
        executor.execute(() -> {
            String oh = OSCPToolbox.getFirstOperationHandleForOperationTarget(provider, state.getReferencedDescriptor());
            if (oh == null) {
                oh = OSCPToolbox.getFirstOperationHandleForOperationTarget(provider, state.getHandle());
            }
            if (oh == null) {
                // State was never intended to be setable due to missing SCO, skip this
                return;
            }
            OperationInvocationContext c = new OperationInvocationContext(oh, 0);
            getSetableStateHandler(provider, c);
        });
    }
    
    private static SetableStateHandler getSetableStateHandler(OSCPProvider provider, OperationInvocationContext context) {
        SetableStateHandler sh = checkStateCache.get(context.getOperationHandle());
        if (sh == null) {
            String targetHandle = OSCPToolbox.getOperationTargetForOperationHandle(provider, context.getOperationHandle());
            if (targetHandle == null) {
                provider.notifyOperationInvoked(context, InvocationState.FAILED, "No set-operation descriptor found");
                return null;
            }
            OperationState os = OSCPToolbox.findState(provider, context.getOperationHandle(), OperationState.class);
            if (os == null || os.getState() != OperationalState.ENABLED) {
                provider.notifyOperationInvoked(context, InvocationState.FAILED, "No operation target state or wrong operational state (must be enabled)");
                return null;
            }
            sh = new SetableStateHandler();
            sh.state = OSCPToolbox.findState(provider, targetHandle, State.class);
            if (sh.state == null) {
                provider.notifyOperationInvoked(context, InvocationState.FAILED, "No state (probably wrong handle)");
                return null;
            }
            if (sh.state instanceof AbstractMetricState) {
                if (!OSCPToolbox.isMetricChangeAllowed(provider, (AbstractMetricState) sh.state)) {
                    provider.notifyOperationInvoked(context, InvocationState.FAILED, "Requirements of metric violated");
                    return null;
                }             
            }
            Map<String, OSCPProviderHandler> handlers = provider.getStateHandlers();
            for (Entry<String, OSCPProviderHandler> nextEntry : handlers.entrySet()) {
               if (nextEntry.getKey().equals(targetHandle) && nextEntry.getValue() instanceof OSCPProviderMDStateHandler) {        
                   sh.handler = (OSCPProviderMDStateHandler)nextEntry.getValue();
                   break;
               }
            }
            if (sh.handler == null) {
                provider.notifyOperationInvoked(context, InvocationState.FAILED, "No state handler found on provider's side");
                return null;
            }
            // State is setable
            checkStateCache.put(context.getOperationHandle(), sh);            
        }
        return sh;
    }  
    
    private static void trySetState(OSCPProviderMDStateHandler handler, State state, OperationInvocationContext context, OSCPProvider provider) {
        InvocationState is = handler.onStateChangeRequest(state, context);
        if (is.equals(InvocationState.FINISHED)) {
            provider.updateState(state);
            provider.notifyOperationInvoked(context, InvocationState.FINISHED, null);
        } else {
            provider.notifyOperationInvoked(context, is, null);
        }
    }    
    
    private static void trySetValue(OSCPProvider provider, SetValue sv, OperationInvocationContext context) {
        SetableStateHandler sh;
        if ((sh = getSetableStateHandler(provider, context)) == null)
            return;
        provider.notifyOperationInvoked(context, InvocationState.STARTED, null);
        NumericMetricState nms = (NumericMetricState)sh.state;
        if (nms.getObservedValue() == null) {
            provider.notifyOperationInvoked(context, InvocationState.FAILED, "Observed value is null");
            return;
        }
        nms.getObservedValue().setValue(sv.getValue());
        trySetState(sh.handler, nms, context, provider);
    }
    
    private static void trySetString(OSCPProvider provider, SetString ss, OperationInvocationContext context) {
        SetableStateHandler sh;
        if ((sh = getSetableStateHandler(provider, context)) == null)
            return;
        provider.notifyOperationInvoked(context, InvocationState.STARTED, null);
        StringMetricState sms = (StringMetricState)sh.state;
        if (sms.getObservedValue() == null) {
            provider.notifyOperationInvoked(context, InvocationState.FAILED, "Observed value is null");
            return;
        }
        sms.getObservedValue().setValue(ss.getString());
        trySetState(sh.handler, sms, context, provider);
    }    
    
    private static void trySetContext(OSCPProvider provider, SetContextState sc, OperationInvocationContext context) {
        for (AbstractContextState nextCS : sc.getProposedContextStates()) {
            SetableStateHandler sh;
            if ((sh = getSetableStateHandler(provider, context)) != null) {
                provider.notifyOperationInvoked(context, InvocationState.STARTED, null);
                trySetState(sh.handler, nextCS, context, provider);           
            }
        }
    }
    
    private static void trySetAlert(OSCPProvider provider, SetAlertState sa, OperationInvocationContext context) {
        SetableStateHandler sh;
        if ((sh = getSetableStateHandler(provider, context)) == null)
            return;
        provider.notifyOperationInvoked(context, InvocationState.STARTED, null);
        trySetState(sh.handler, sa.getState(), context, provider);       
    }    
    
    private static void tryActivate(OSCPProvider provider, Activate activate, OperationInvocationContext context) {
        OSCPProviderActivateOperationHandler handler = null;
        Map<String, OSCPProviderHandler> handlers = provider.getStateHandlers();
        for (Entry<String, OSCPProviderHandler> nextEntry : handlers.entrySet()) {
           if (nextEntry.getKey().equals(context.getOperationHandle()) && nextEntry.getValue() instanceof OSCPProviderActivateOperationHandler) {        
               handler = (OSCPProviderActivateOperationHandler)nextEntry.getValue();
               break;
           }
        }
        if (handler == null) {
            provider.notifyOperationInvoked(context, InvocationState.FAILED, "Activate handler is null");
            return;
        }        
        provider.notifyOperationInvoked(context, InvocationState.STARTED, null);
        InvocationState is = handler.onActivateRequest(provider.getMDIB(), context);
        if (is.equals(InvocationState.FINISHED)) {
            provider.notifyOperationInvoked(context, InvocationState.FINISHED, null);
        } else {
            provider.notifyOperationInvoked(context, is, null);
        }        
    }    
    
    static <U extends AbstractSet, V extends AbstractSetResponse> InvokeDelegate createOSCPSetOperation(final OSCPProvider provider, final Class<U> req, final Class<V> res) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            U request = JAXBUtil.getInstance().createInputParameterValue(arguments, req);
            long currentTid = queueRequest(provider, request);
            
            V response;
            try {
                response = res.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(ProviderSetOperationFactory.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            prepareResponse(response, currentTid);
            response.setSequenceNumber(BigInteger.valueOf(provider.getMdibVersion()));
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }
    
    private static long queueRequest(final OSCPProvider provider, AbstractSet request) {
        long currentTid = transactionId.addAndGet(1);
        OperationInvocationContext oic = new OperationInvocationContext(request.getOperationHandle(), currentTid);
        provider.notifyOperationInvoked(oic, InvocationState.WAITING, null);
        queue.add(new SetNotification<>(provider, request, oic));
        return currentTid;
    }    
    
    private static void prepareResponse(AbstractSetResponse response, long currentTid) {
        response.setTransactionId(currentTid);
        response.setInvocationState(InvocationState.WAITING);
    }    
           
    
}
