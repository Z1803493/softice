/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import com.rits.cloning.Cloner;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.Activate;
import org.ornet.cdm.ActivateOperationDescriptor;
import org.ornet.cdm.ActivateResponse;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.CurrentAlertCondition;
import org.ornet.cdm.Descriptor;
import org.ornet.cdm.GetMDIBResponse;
import org.ornet.cdm.HydraMDSDescriptor;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MDDescription;
import org.ornet.cdm.MDIB;
import org.ornet.cdm.MDState;
import org.ornet.cdm.OperationState;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.SetAlertState;
import org.ornet.cdm.SetAlertStateResponse;
import org.ornet.cdm.SetContextState;
import org.ornet.cdm.SetContextStateResponse;
import org.ornet.cdm.SetString;
import org.ornet.cdm.SetStringResponse;
import org.ornet.cdm.SetValue;
import org.ornet.cdm.SetValueResponse;
import org.ornet.cdm.State;
import org.ornet.mdpws.MDPWSStreamingManager;
import org.ornet.softice.OSCPDevice;
import org.ornet.softice.SoftICE;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.service.AppSequenceManager;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.URI;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.util.Log;

public final class OSCPProvider implements OSCPEndpoint {

    private final OSCPDevice dev = new OSCPDevice();
    private final AtomicLong mdibVersion = new AtomicLong();
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    
    private final ConcurrentMap<String, HydraMDSDescriptor> hMDSMap = new ConcurrentHashMap<>();
    
    private final List<State> mdibStates = Collections.synchronizedList(new ArrayList<>());
    private final List<OperationState> operationStates = Collections.synchronizedList(new ArrayList<>());
    
    private final Map<String, OSCPProviderHandler> providerHandlers = Collections.synchronizedMap(new HashMap<>());
    
    public OSCPProvider() {
    }

    @Override
    @SuppressWarnings("FinalizeDeclaration")
    protected void finalize() throws Throwable {
        try {
            if (isRunning()) {
                shutdown();
            }
        } finally {
            super.finalize();
        }
    }
       
    /**
     * Get deep clone of MD description.
     * 
     * @return The description.
     */
    @Override
    public MDDescription getMDDescription() {
        MDDescription mdd = new MDDescription();
        synchronized(hMDSMap) {
            for (Entry<String, HydraMDSDescriptor> nextEntry : hMDSMap.entrySet()) {
                mdd.getMDSDescriptors().add(nextEntry.getValue());
            }            
        }
        Cloner cloner = new Cloner();
        return cloner.deepClone(mdd);
    }

    public long getMdibVersion() {
        return mdibVersion.get();
    }  
    
    public void addHydraMDS(HydraMDSDescriptor hmds) {
        hMDSMap.put(hmds.getHandle(), hmds);
        mdibVersion.incrementAndGet();
    }
    
    public void removeHydraMDS(String handle) {
        hMDSMap.remove(handle);
        mdibVersion.incrementAndGet();
    }
    
    /**
     * Get deep clone of all MD states.
     * 
     * @return The states.
     */
    @Override
    public MDState getMDState() {
        MDState states = new MDState();
        Cloner cloner = new Cloner();
        synchronized(mdibStates) {
            states.getStates().addAll(cloner.deepClone(mdibStates));
        }
        synchronized(operationStates) {
            states.getStates().addAll(cloner.deepClone(operationStates));
        }
        return states;
    }
    
    public List<AbstractContextState> getContextStates() {
        return getContextStates(null);
    }
    
    public List<AbstractContextState> getContextStates(List<String> handles) {
        return getTypedStates(handles, AbstractContextState.class);
    } 
    
    public List<AbstractAlertState> getAlertStates() {
        return getTypedStates(null, AbstractAlertState.class);
    }     
    
    public List<AbstractAlertState> getAlertStates(List<String> handles) {
        return getTypedStates(handles, AbstractAlertState.class);
    } 

    private <T> List<T> getTypedStates(List<String> handles, Class<T> type) {
        MDState states = handles == null || handles.isEmpty()? getMDState() : getMDState(handles);
        List<T> targetStates = new LinkedList<>();
        Iterator<State> it = states.getStates().iterator();
        while (it.hasNext()) {
            State next = it.next();
            if (type.isAssignableFrom(next.getClass())) {
                targetStates.add(type.cast(next));                
            }
        }
        return targetStates;
    }       
    
    /**
     * Get deep clone of MD states.
     * 
     * @param handles List of handles to match.
     * @return The states.
     */
    @Override
    public MDState getMDState(List<String> handles) {
        MDState states = new MDState();
        Set<String> handleSet = new HashSet<>(handles);
        synchronized(mdibStates) {
            addToStateContainer(handleSet, states, mdibStates.iterator());
        }   
        synchronized(operationStates) {
            addToStateContainer(handleSet, states, operationStates.iterator());
        }          
        return states;
    }

    private <T extends State> void addToStateContainer(Set<String> handleSet, MDState states, Iterator<T> it) {
        Cloner cloner = new Cloner();
        while (it.hasNext()) {
            T next = it.next();
            if (handleSet.contains(next.getReferencedDescriptor()) || (next.getHandle() != null && handleSet.contains(next.getHandle()))) {
                State cloned = cloner.deepClone(next);
                states.getStates().add(cloned);
            }
        }
    }
    
    public void createSetOperationForDescriptor(Descriptor descr, HydraMDSDescriptor mds) {
        OSCPToolbox.createOperationDescriptor(operationStates, descr, mds);
    }
    
    public void addActivateOperationForDescriptor(ActivateOperationDescriptor descr, HydraMDSDescriptor mds) {
        OSCPToolbox.createOperationDescriptor(operationStates, descr, mds);
    }    

    /**
     * Create an MDIB container object. Modifications of this structure will not be reflected into the internal MDIB representation.
     * 
     * @return The MDIB.
     */
    @Override
    public final MDIB getMDIB() {
        MDIB mdib = new MDIB();
        mdib.setDescription(getMDDescription());
        mdib.setStates(getMDState());
        mdib.setSequenceNumber(BigInteger.valueOf(mdibVersion.get()));
        return mdib;
    }
   
    public void startup() {
        Log.info("Provider startup...");
        try {
            dev.init(createOperations(), createEventSources());
            synchronized(providerHandlers) {
                for (OSCPProviderHandler next : providerHandlers.values()) {
                    if (next instanceof OSCPProviderMDStateHandler) {
                        final State newState = setDefaultStateValues(((OSCPProviderMDStateHandler)next).getInitalClonedState());
                        mdibStates.add(newState);
                        if (newState instanceof RealTimeSampleArrayMetricState) {
                            UnknownDataContainer streamMetadata = new UnknownDataContainer();
                            MDPWSStreamingManager.getInstance().addNewStreamSenderEndpoint(newState.getReferencedDescriptor(), streamMetadata, this);
                            dev.getStreamService().addCustomMData(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, streamMetadata);
                        }                    
                    }
                }
            }
            dev.start();
            // Check for valid provider MDIB
            if (SoftICE.getInstance().isSchemaValidationEnabled()) {
                GetMDIBResponse response = new GetMDIBResponse();
                final MDIB mdib = getMDIB();
                response.setMDIB(mdib);
                response.setSequenceNumber(mdib.getSequenceNumber());
                JAXBUtil.getInstance().createOutputParameterValue(response);                            
            }
        } catch (Exception ex) {
            Logger.getLogger(OSCPProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isRunning() {
        return dev.isRunning();
    }
    
    public void shutdown() {
        Log.info("Provider shutdown...");
        try {
            dev.stop();
            ProviderSetOperationFactory.shutdown();
            MDPWSStreamingManager.getInstance().removeStreamSenders(this);
        } catch (IOException ex) {
            Logger.getLogger(OSCPProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private <T extends State> boolean updateInternalMatchingState(State newState, List<T> list) {
        State temp = null;
        synchronized(list) {
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
               State next = it.next();
               if (next.getReferencedDescriptor().equals(newState.getReferencedDescriptor()) 
                   && (next.getHandle() == null || 
                       (next.getHandle() != null && newState.getHandle() != null && next.getHandle().equals(newState.getHandle())))) {
                   temp = next;
                   break;
               }
            }
        }
        if (temp == null)
            return false;
        list.remove((T)temp);
        newState.setStateVersion(temp.getStateVersion().add(BigInteger.ONE));
        Cloner cloner = new Cloner();
        list.add((T)cloner.deepClone(setDefaultStateValues(newState)));
        mdibVersion.incrementAndGet();        
        return true;
    }    
    
    private boolean updateInternalMatchingState(State newState) {
        if (!updateInternalMatchingState(newState, mdibStates))
            return updateInternalMatchingState(newState, operationStates);
        return true;
    }
    
    public void updateState(State state) {
        if (!updateInternalMatchingState(state))
            throw new RuntimeException("No state to update with given descriptor handle (state handle): " + state.getReferencedDescriptor() + " (" + state.getHandle() + ")");
        // update setable state cache
        ProviderSetOperationFactory.updateSetableStateCache(this, state);
        // Eventing
        if (state instanceof RealTimeSampleArrayMetricState) {
            MDPWSStreamingManager.getInstance().sendStreamPacket((RealTimeSampleArrayMetricState)state);
        }        
        else if (state instanceof AbstractMetricState) {
            ProviderEventSourceFactory.fireEpisodicMetricEventReport((AbstractMetricState) state, BigInteger.valueOf(mdibVersion.get()));
            evaluateAlertConditions(((AbstractMetricState) state));
        }
        else if (state instanceof AbstractContextState)
            ProviderEventSourceFactory.fireEpisodicContextChangedReport((AbstractContextState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractAlertState)
            ProviderEventSourceFactory.fireEpisodicAlertEventReport((AbstractAlertState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof OperationState)
            throw new RuntimeException("Eventing not yet supported for operation states, handle: " + state.getReferencedDescriptor());
    }  
    
    public synchronized void notifyOperationInvoked(OperationInvocationContext oic, InvocationState is, String operationErrorMsg) {
        ProviderEventSourceFactory.fireOperationInvokedReport(this, oic, is, BigInteger.valueOf(mdibVersion.get()), operationErrorMsg);
    }
    
    public void setEndpointReference(String epr) {
        dev.setEndpointReference(new EndpointReference(new URI(epr)));
    }
    
    public String getEndpointReference() {
        return dev.getEndpointReference().toString();
    }
    
    public void addHandler(OSCPProviderHandler handler) {
        if (dev.isRunning()) 
            throw new RuntimeException("Provider is running!");
        handler.setProvider(this);
        providerHandlers.put(handler.getDescriptorHandle(), handler);
    }

    public void removeHandler(OSCPProviderHandler handler) {
        if (dev.isRunning()) 
            throw new RuntimeException("Provider is running!");
        providerHandlers.remove(handler.getDescriptorHandle());
    }
    
    public OSCPProviderHandler getHandler(String descriptorHandle) {
        return providerHandlers.get(descriptorHandle);
    }

    protected Map<String, OSCPProviderHandler> getStateHandlers() {
        return providerHandlers;
    }
    
    /**
     * Create operations
     * 
     * @return Hashmap WSDL -> operation name -> invoke delegate
     */
    private HashMap<String, HashMap<String, InvokeDelegate>> createOperations() {
        HashMap<String, HashMap<String, InvokeDelegate>> map = new HashMap<>();
        // Get operations
        map.put(OSCPDevice.GETSERVICEWSDL, new HashMap<String, InvokeDelegate>());
        map.get(OSCPDevice.GETSERVICEWSDL).put("GetMDIB", ProviderGetOperationFactory.createGetMDIBOperation(this));
        map.get(OSCPDevice.GETSERVICEWSDL).put("GetMDDescription", ProviderGetOperationFactory.createGetMDDescriptionOperation(this));
        map.get(OSCPDevice.GETSERVICEWSDL).put("GetMDState", ProviderGetOperationFactory.createGetMDStateOperation(this));
        // Set operations
        ProviderSetOperationFactory.init();
        map.put(OSCPDevice.SETSERVICEWSDL, new HashMap<String, InvokeDelegate>());
        map.get(OSCPDevice.SETSERVICEWSDL).put("SetValue", ProviderSetOperationFactory.createOSCPSetOperation(this, SetValue.class, SetValueResponse.class));
        map.get(OSCPDevice.SETSERVICEWSDL).put("SetString", ProviderSetOperationFactory.createOSCPSetOperation(this, SetString.class, SetStringResponse.class));
        map.get(OSCPDevice.SETSERVICEWSDL).put("Activate", ProviderSetOperationFactory.createOSCPSetOperation(this, Activate.class, ActivateResponse.class));
        map.get(OSCPDevice.SETSERVICEWSDL).put("SetAlertState", ProviderSetOperationFactory.createOSCPSetOperation(this, SetAlertState.class, SetAlertStateResponse.class));
        // Context operations
        if (!map.containsKey(OSCPDevice.PHISERVICEWSDL))
            map.put(OSCPDevice.PHISERVICEWSDL, new HashMap<String, InvokeDelegate>());
        map.get(OSCPDevice.PHISERVICEWSDL).put("GetContextStates", ProviderGetOperationFactory.createGetContextsOperation(this));
        map.get(OSCPDevice.PHISERVICEWSDL).put("SetContextState", ProviderSetOperationFactory.createOSCPSetOperation(this, SetContextState.class, SetContextStateResponse.class));   
        return map;
    }
    
    /**
     * Create event sources
     * 
     * @return Hashmap WSDL -> event name -> event delegate
     */
    private HashMap<String, HashMap<String, EventDelegate>> createEventSources() {
        HashMap<String, HashMap<String, EventDelegate>> map = new HashMap<>();
        // Event operations
        map.put(OSCPDevice.EVENTREPORTWSDL, new HashMap<String, EventDelegate>());
        map.get(OSCPDevice.EVENTREPORTWSDL).put("EpisodicMetricReport", ProviderEventSourceFactory.createEpisodicMetricReportSource(this));
        map.get(OSCPDevice.EVENTREPORTWSDL).put("OperationInvokedReport", ProviderEventSourceFactory.createOperationInvokedReportSource(this));
        map.get(OSCPDevice.EVENTREPORTWSDL).put("EpisodicAlertReport", ProviderEventSourceFactory.createEpisodicAlertReportSource(this));
        if (!map.containsKey(OSCPDevice.PHISERVICEWSDL))
            map.put(OSCPDevice.PHISERVICEWSDL, new HashMap<String, EventDelegate>());
        map.get(OSCPDevice.PHISERVICEWSDL).put("EpisodicContextChangedReport", ProviderEventSourceFactory.createEpisodicContextReportSource(this));
        // MDPWS streaming
        map.put(OSCPDevice.WAVEREPORTWSDL, new HashMap<String, EventDelegate>());
        map.get(OSCPDevice.WAVEREPORTWSDL).put("WaveformStreamReport", ProviderEventSourceFactory.createStreamReportSource(this));
        return map;
    }   
    
   public String getManufacturer() {
        return dev.getManufacturer();
    }

    public void setManufacturer(String manufacturer) {
        this.dev.setManufacturer(manufacturer);
    }

    public String getModelName() {
        return dev.getModelName();
    }

    public void setModelName(String modelName) {
        this.dev.setModelName(modelName);
    }

    public String getFriendlyName() {
        return dev.getFriendlyName();
    }

    public void setFriendlyName(String friendlyName) {
        this.dev.setFriendlyName(friendlyName);
    }    

    private State setDefaultStateValues(State state) {
        if (state.getStateVersion() == null)
            state.setStateVersion(BigInteger.ZERO);
        if (state instanceof AbstractContextState) {
            AbstractContextState acs = (AbstractContextState)state;
            if (acs.getBindingMDIBVersion() == null) {
                acs.setBindingMDIBVersion(BigInteger.ZERO);
            }
        }
        return state;
    }
    
    public AppSequenceManager getAppSequencer() {
        return dev.getAppSequencer();
    }

    private void evaluateAlertConditions(AbstractMetricState state) {
        executor.execute(() -> {
            Set<AlertConditionDescriptor> acds = OSCPToolbox.collectAllAlertConditionDescriptors(this);
            for (AlertConditionDescriptor next : acds) {
                if (next.getSources().contains(state.getReferencedDescriptor()) || next.getSources().contains(state.getHandle())) {
                    OSCPProviderHandler handler = getHandler(next.getHandle());
                    if (handler == null) {
                        Logger.getLogger(OSCPProvider.class.getName()).log(Level.SEVERE, "Error in evaluating alert conditions. Handler missing for: {0}", state.getReferencedDescriptor());
                        return;
                    }
                    if (handler instanceof OSCPProviderAlertConditionStateHandler) {
                        List<AbstractAlertState> handlerStates = getAlertStates(new ArrayList<>(Arrays.asList(new String [] {next.getHandle()})));
                        if (handlerStates.size() > 0) {
                            ((OSCPProviderAlertConditionStateHandler)handler).sourceHasChanged(state, (CurrentAlertCondition)handlerStates.get(0));
                        }
                    }
                }
            }            
        });
    }

    @Override
    public String getReferenceId() {
        return getEndpointReference();
    }
   
}
