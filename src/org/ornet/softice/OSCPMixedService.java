/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.softice.provider.OSCPEventDelegate;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.EventSourceStub;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.service.OperationStub;
import org.yads.java.types.URI;

public abstract class OSCPMixedService extends OSCPService {
    
    public OSCPMixedService(String wsdl, HashMap<String, InvokeDelegate> invokeDelegates, HashMap<String, EventDelegate> eventDelegates, int port) {
        super(wsdl, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, port);
        try {
            define(new URI("local:/org/ornet/softice/resources/" + wsdl), CredentialInfo.EMPTY_CREDENTIAL_INFO, DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
        } catch (IOException ex) {
            Logger.getLogger(OSCPService.class.getName()).log(Level.SEVERE, null, ex);
        }        
        if (invokeDelegates != null)
            prepareWebserviceInvokeInterface(invokeDelegates);
        if (eventDelegates != null)
            prepareWebserviceEventInterface(eventDelegates);
    }
    
    private void prepareWebserviceInvokeInterface(HashMap<String, InvokeDelegate> invokeDelegates) {
        // Init operations
        for (Map.Entry<String, InvokeDelegate> next : invokeDelegates.entrySet()) {
            OperationStub op = (OperationStub) this.getOperation(next.getKey());
            if (op == null) {
                Logger.getLogger(OSCPService.class.getName()).log(Level.SEVERE, "Operation not found in service description: {0}", next.getKey());
                continue;
            }
            op.setDelegate(next.getValue());
        }
    }     
    
    private void prepareWebserviceEventInterface(HashMap<String, EventDelegate> eventDelegates) {
        // Init event sources
        for (Map.Entry<String, EventDelegate> next : eventDelegates.entrySet()) {
            EventSourceStub ev = (EventSourceStub) this.getEventSource(next.getKey());
            if (ev == null) {
                Logger.getLogger(OSCPService.class.getName()).log(Level.SEVERE, "Event source not found in service description: {0}", next.getKey());
                continue;
            }
            ev.setDelegate(next.getValue());
            if (next.getValue() instanceof OSCPEventDelegate) {
                ((OSCPEventDelegate)next.getValue()).setEventSourceStub(ev);
            }
        }
    }       
    
}
