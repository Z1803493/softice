/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.yads.java.client.DefaultClient;
import org.yads.java.client.SearchManager;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.types.QName;
import org.yads.java.types.QNameSet;
import org.yads.java.types.SearchParameter;

public class OSCPServiceManager {
    
    private static final int DEFAULT_TIMEOUT = 16000;
    private static final int MIN_GAP_TIME = 4000;
    
    private static OSCPServiceManager instance;
    
    private OSCPServiceManager() {
        
    }
    
    public static OSCPServiceManager getInstance() {
        if (instance == null)
            instance = new OSCPServiceManager();
        return instance;
    }
    
    public List<OSCPConsumer> discoverOSCP() {
        return discoverEPR(null, DEFAULT_TIMEOUT, MIN_GAP_TIME);
    }    
    
    public OSCPConsumer discoverEPR(String epr) {
        return OSCPServiceManager.this.discoverEPR(epr, DEFAULT_TIMEOUT);
    }      
    
    public OSCPConsumer discoverEPR(String epr, int timeout) {
        Set<String> eprSet = new HashSet<>();
        eprSet.add(epr);
        List<OSCPConsumer> c = discoverEPR(eprSet, timeout, MIN_GAP_TIME);
        if (c.isEmpty())
            return null;
        return c.get(0);
    }    
    
    public List<OSCPConsumer> discoverOSCP(int timeout) {
        return discoverEPR(null, timeout, MIN_GAP_TIME);
    }
    
    /**
     * Find OSCP devices.
     * 
     * @param epr Set of endpoint references (null for all devices)
     * @param timeout Global upper search time limit
     * @param gaptime End search if no new device found in this time
     * 
     * @return List of found consumers
     */
    public List<OSCPConsumer> discoverEPR(final Set<String> epr, int timeout, int gaptime) {
        SearchParameter search = new SearchParameter();
        search.setDeviceTypes(new QNameSet(new QName("MedicalDevice", "http://message-model-uri/15/04")), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
        final List<DeviceReference> devMap = Collections.synchronizedList(new ArrayList<DeviceReference>());
        SearchManager.searchDevice(search, new DefaultClient() {

            @Override
            public void deviceFound(DeviceReference devRef, SearchParameter search, String comManId) {
                devMap.add(devRef);
                synchronized(OSCPServiceManager.this) {
                   OSCPServiceManager.this.notifyAll();
                }                
            }
            
        }, null);
        int i = 0;
        int lastSize = 0;
        int gap = 0;
        while (i < timeout) {
            List<DeviceReference> toRemove = new ArrayList<>();
            synchronized(devMap) {
                if (epr != null)
                {
                    for (DeviceReference next : devMap) {
                        if (!epr.contains(next.getEndpointReference().getAddress().toString())) {
                            toRemove.add(next);
                        }
                    }
                }
            }                
            try {
                long startTime = System.currentTimeMillis();              
                synchronized(this) {
                   wait(100); 
                }
                long elapsed = System.currentTimeMillis() - startTime;                
                i += elapsed;
                gap += elapsed;
            } catch (InterruptedException ex) {
                Logger.getLogger(OSCPServiceManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (lastSize < devMap.size()) {
                lastSize = devMap.size();    
                gap = 0;
            }
            devMap.removeAll(toRemove);
            if (epr != null && allFound(devMap, epr))
                break;
            if (gap > gaptime)
                break;            
        }
        List<OSCPConsumer> consumers = new ArrayList<>();
        synchronized(devMap) {
            for (DeviceReference nextRef : devMap) {
                final OSCPConsumer oscpConsumer = new OSCPConsumer(nextRef);
                if (oscpConsumer.isConnected())
                    consumers.add(oscpConsumer);
            }
        }
        return consumers;
    }

    private boolean allFound(List<DeviceReference> devMap, Set<String> epr) {
        if (devMap.size() != epr.size())
            return false;
        Set<String> current = new HashSet<>();
        for (DeviceReference nextRef : devMap) {
            current.add(nextRef.getEndpointReference().getAddress().toString());
        }
        return current.equals(epr);
    }

    
}
