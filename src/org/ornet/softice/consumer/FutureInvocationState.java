/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.InvocationState;

public class FutureInvocationState {
    
    private final AtomicLong transactionId = new AtomicLong();
    private final AtomicBoolean match = new AtomicBoolean(false);
    @SuppressWarnings("SetReplaceableByEnumSet")
    private final Set<InvocationState> expectedSet = Collections.synchronizedSet(new HashSet<InvocationState>());
    @SuppressWarnings("SetReplaceableByEnumSet")
    private final Set<InvocationState> actualSet = Collections.synchronizedSet(new HashSet<InvocationState>());
    private OSCPConsumer consumer;
  
    public boolean waitReceived(InvocationState expected, int timeout) {
        return waitReceived(new InvocationState[] { expected }, timeout);
    }
    
    public boolean waitReceived(InvocationState [] expected, int timeout) {
        expectedSet.addAll(Arrays.asList(expected));
        checkMatch();
        long startTime = System.currentTimeMillis();
        long elapsed = 0;
        boolean matches = false;
        while (elapsed < timeout && !(matches = match.get())) {
            try {
                long remaining = timeout - elapsed;
                synchronized(this) {
                    wait(remaining);    
                }
                long endTime = System.currentTimeMillis();
                elapsed = endTime - startTime;
            } catch (InterruptedException ex) {
                Logger.getLogger(FutureInvocationState.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        consumer.unregisterFutureInvocationstate(this);
        actualSet.clear();
        expectedSet.clear();
        return matches;
    }

    protected void setTransactionId(long transactionId) {
        this.transactionId.set(transactionId);
    }

    protected long getTransactionId() {
        return transactionId.get();
    } 
    
    protected void setActual(InvocationState is) {
        actualSet.add(is);
        checkMatch();
    }

    private void checkMatch() {
        synchronized(this) {
            if (!expectedSet.isEmpty() && actualSet.containsAll(expectedSet)) {
                match.set(true);
                notifyAll();
            }
        }
    }

    protected void setConsumer(OSCPConsumer consumer) {
        this.consumer = consumer;
    }   
    
}
