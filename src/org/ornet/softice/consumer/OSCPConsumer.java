/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.consumer;

import com.rits.cloning.Cloner;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractSet;
import org.ornet.cdm.AbstractSetResponse;
import org.ornet.cdm.Activate;
import org.ornet.cdm.ActivateResponse;
import org.ornet.cdm.AlertReportPart;
import org.ornet.cdm.ContextChangedReportPart;
import org.ornet.cdm.EpisodicAlertReport;
import org.ornet.cdm.EpisodicContextChangedReport;
import org.ornet.cdm.EpisodicMetricReport;
import org.ornet.cdm.GetMDDescription;
import org.ornet.cdm.GetMDDescriptionResponse;
import org.ornet.cdm.GetMDIB;
import org.ornet.cdm.GetMDIBResponse;
import org.ornet.cdm.GetMDState;
import org.ornet.cdm.GetMDStateResponse;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MDDescription;
import org.ornet.cdm.MDIB;
import org.ornet.cdm.MDState;
import org.ornet.cdm.MetricReportPart;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericValue;
import org.ornet.cdm.OperationInvokedReport;
import org.ornet.cdm.OperationInvokedReportPart;
import org.ornet.cdm.PeriodicAlertReport;
import org.ornet.cdm.PeriodicContextChangedReport;
import org.ornet.cdm.PeriodicMetricReport;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.SetAlertState;
import org.ornet.cdm.SetContextState;
import org.ornet.cdm.SetString;
import org.ornet.cdm.SetValue;
import org.ornet.cdm.State;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.StringMetricValue;
import org.ornet.cdm.WaveformStream;
import org.ornet.mdpws.MDPWSStreamingManager;
import org.ornet.softice.SoftICE;
import org.ornet.softice.provider.OSCPEndpoint;
import org.ornet.softice.provider.OSCPToolbox;
import org.ornet.softice.provider.OperationInvocationContext;
import org.yads.java.client.DefaultClient;
import org.yads.java.communication.CommunicationException;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.eventing.ClientSubscription;
import org.yads.java.eventing.EventSource;
import org.yads.java.message.eventing.SubscriptionEndMessage;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.security.SecurityKey;
import org.yads.java.service.Operation;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.service.reference.ServiceReference;
import org.yads.java.types.URI;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.util.Log;

public final class OSCPConsumer extends DefaultClient implements OSCPEndpoint {
    
    protected static final int DURATION_SUBSCRIPTION = 30;
    protected static final int CYLCLE_RENEW = 15;
    
    private static final String PERIODIC_CONTEXT_CHANGED_REPORT = "PeriodicContextChangedReport";
    private static final String PERIODIC_ALERT_REPORT = "PeriodicAlertReport";
    private static final String PERIODIC_METRIC_REPORT = "PeriodicMetricReport";
    private static final String OPERATION_INVOKED_REPORT = "OperationInvokedReport";
    private static final String EPISODIC_ALERT_REPORT = "EpisodicAlertReport";
    private static final String EPISODIC_CONTEXT_CHANGED_REPORT = "EpisodicContextChangedReport";
    private static final String EPISODIC_METRIC_REPORT = "EpisodicMetricReport";    
    
    private final ScheduledExecutorService renewScheduler = Executors.newScheduledThreadPool(1);
    private OSCPConnectionLostHandler connectionLostHandler;
    
    interface SubscriptionVisitor {
        boolean visit(ClientSubscription subscription);
    }
    
    class TransactionState {
        
        public TransactionState(long transactionId, InvocationState is) {
            this.transactionId = transactionId;
            this.invocationState = is;
        }

        public synchronized long getTransactionId() {
            return transactionId;
        }

        public synchronized void setTransactionId(long transactionId) {
            this.transactionId = transactionId;
        }

        public synchronized InvocationState getInvocationState() {
            return invocationState;
        }

        public synchronized void setInvocationState(InvocationState invocationState) {
            this.invocationState = invocationState;
        }
        
        private long transactionId;
        private InvocationState invocationState;
        
    }
    
    private final DeviceReference deviceRef;
    private final MDIB mdib = new MDIB();
    private final AtomicBoolean connected = new AtomicBoolean(true);
    private final List<String> streamAddrs = new ArrayList<>();

    private final Map<ClientSubscription, String> subscriptions = new ConcurrentHashMap<>();
    
    private final ConcurrentMap<String, OSCPConsumerHandler> eventHandler = new ConcurrentHashMap<>();
    private final ConcurrentLinkedQueue<TransactionState> transactionQueue = new ConcurrentLinkedQueue<>();
    private final ConcurrentMap<Long, FutureInvocationState> fisMap = new ConcurrentHashMap<>();

    public OSCPConsumer(DeviceReference deviceRef) {
        this.deviceRef = deviceRef;
        init(deviceRef);
    }

    private void init(DeviceReference deviceRef) {
        if (getMDIB() != null) {
            connected.set(initEventing());
            initMDPWSStreaming();
            if (connected.get()) {
                renewScheduler.scheduleAtFixedRate(createRenewManager(), OSCPConsumer.CYLCLE_RENEW / 2, OSCPConsumer.CYLCLE_RENEW, TimeUnit.SECONDS);
            }
            else {
                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, "Fatal error: could not initialize eventing! Device reference: {0}", deviceRef);
            }
        }
        else
            connected.set(false);
    }
    
    private boolean addSubscription(String name, ClientSubscription subscription) {
        if (subscription != null) {
            subscriptions.put(subscription, name);
            return true;
        }
        return false;
    }
    
    private void applyToSubscriptions(SubscriptionVisitor visitor) {
        synchronized(subscriptions) {
            for (Map.Entry<ClientSubscription, String> next : subscriptions.entrySet()) {
                if (!visitor.visit(next.getKey()))
                    break;
            }
        }
    }

    private boolean initEventing() {
        subscriptions.clear();
        if (!addSubscription(EPISODIC_METRIC_REPORT, initEventSink("EventReport", EPISODIC_METRIC_REPORT)))
            return false;
        if (!addSubscription(EPISODIC_CONTEXT_CHANGED_REPORT, initEventSink("PHI", EPISODIC_CONTEXT_CHANGED_REPORT)))
            return false;
        if (!addSubscription(EPISODIC_ALERT_REPORT, initEventSink("EventReport", EPISODIC_ALERT_REPORT)))
            return false;
        if (!addSubscription(OPERATION_INVOKED_REPORT, initEventSink("EventReport", OPERATION_INVOKED_REPORT)))
            return false;
        if (!addSubscription(PERIODIC_METRIC_REPORT, initEventSink("EventReport", PERIODIC_METRIC_REPORT)))
            return false;
        if (!addSubscription(PERIODIC_ALERT_REPORT, initEventSink("EventReport", PERIODIC_ALERT_REPORT)))
            return false;        
        return addSubscription(PERIODIC_CONTEXT_CHANGED_REPORT, initEventSink("PHI", PERIODIC_CONTEXT_CHANGED_REPORT));
    }

    private void initMDPWSStreaming() {
        try {
            // Check for waveform streaming service
            final ServiceReference serviceReference = deviceRef.getDevice().getServiceReference(new URI("WaveformEventReport"), SecurityKey.EMPTY_KEY);
            if (serviceReference == null)
                return;
            // Trigger custom metadata fetch from waveform streaming service
            UnknownDataContainer [] udc = serviceReference.getService().getCustomMData(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
            if (udc == null)
                // No custom metadata found, return
                return;
            for (UnknownDataContainer next : udc) {
                List streamingAddresses = next.getUnknownElements(MDPWSStreamingManager.STREAM_DESCRIPTIONS);
                // No streaming addresses found, return
                if (streamingAddresses == null)
                    return;
                for (Object nextAdrList : streamingAddresses) {
                    for (String nextAdr : (List<String>)nextAdrList) {
                        streamAddrs.add(nextAdr);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void disconnect() {
        connected.set(false);
        try {
            renewScheduler.shutdown();
            renewScheduler.awaitTermination(5, TimeUnit.SECONDS);
            applyToSubscriptions((ClientSubscription subscription) -> {
                try {
                    subscription.unsubscribe();
                } catch (Exception ex) {
                    Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
                }
                return true;
            });
            subscriptions.clear();
            MDPWSStreamingManager.getInstance().removeStreamListeners(this);
        } catch (Exception ex) {
            if (Log.isDebug()) {
                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);            
            }
        }
    }

    private Runnable createRenewManager() {
        return () -> {
            try {
                if (!connected.get()) {
                    // Try to subscribe again
                    connected.set(initEventing());
                    if (connected.get()) {
                        Logger.getLogger(OSCPConsumer.class.getName()).log(Level.INFO, "Connection re-established.");
                    }
                }
                applyToSubscriptions((ClientSubscription subscription) -> {
                    try {
                        subscription.renew(OSCPConsumer.DURATION_SUBSCRIPTION * 1000);
                        return true;
                    } catch (Exception ex) {
                        if (Log.isDebug()) {
                            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Logger.getLogger(OSCPConsumer.class.getName()).log(Level.INFO, "Connection lost. Trying to re-establish in {0} seconds...", CYLCLE_RENEW);
                        connected.set(false);
                        if (connectionLostHandler != null) {
                            try {
                                connectionLostHandler.onConnectionLost();
                            }
                            catch (Exception e) {
                                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, e);
                            }
                        }
                        return false;
                    }
                });
            } catch (Exception ex) {
                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
    }

    public boolean isConnected() {
        return connected.get();
    }
    
    private synchronized Operation getOperation(String serviceId, String operationName) {
        try {
            ServiceReference getServiceRef = deviceRef.getDevice().getServiceReference(new URI(serviceId), SecurityKey.EMPTY_KEY);
            if (getServiceRef == null)
                return null;
            Operation op = getServiceRef.getService().getOperation(null, operationName, null, null);
            return op;
        } catch (CommunicationException ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void subscriptionEndReceived(ClientSubscription subscription, int subscriptionEndType) {  
        if (!connected.get())
            return;
        connected.set(false);
        try {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.WARNING, "Subscription ended! Consumer is now disconnected.");
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void subscriptionTimeoutReceived(ClientSubscription subscription) {
        subscriptionEndReceived(subscription, SubscriptionEndMessage.WSE_STATUS_UNKNOWN);
    }
    
    private synchronized EventSource getEventSource(String serviceId, String sourceName) {
        try {
            ServiceReference getServiceRef = deviceRef.getDevice().getServiceReference(new URI(serviceId), SecurityKey.EMPTY_KEY);
            if (getServiceRef == null)
                return null;
            EventSource es = getServiceRef.getService().getEventSource(null, sourceName, null, null);
            return es;
        } catch (CommunicationException ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }    
      
    @Override
    public MDIB getMDIB() {    
        try {
            Operation op = getOperation("GetService", "GetMDIB");
            if (op == null)
                return null;
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(new GetMDIB());
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            GetMDIBResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetMDIBResponse.class);
            synchronized(mdib) {
                MDIB temp = response.getMDIB();
                mdib.setDescription(temp.getDescription());
                mdib.setStates(temp.getStates());
                mdib.setSequenceNumber(temp.getSequenceNumber());
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib);
            }
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }    

    @Override
    public MDDescription getMDDescription() {
        synchronized(mdib) {
            if (mdib.getDescription() != null) {
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib.getDescription());
            }
        }
        try {
            Operation op = getOperation("GetService", "GetMDDescription");
            if (op == null)
                return null;
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(new GetMDDescription());
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            GetMDDescriptionResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetMDDescriptionResponse.class);
            synchronized(mdib) {
                mdib.setDescription(response.getStaticDescription());
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib.getDescription());
            }
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

    @Override
    public MDState getMDState() {
        return getMDState(null);
    }

    @Override
    public MDState getMDState(List<String> handles) {
        try {
            Operation op = getOperation("GetService", "GetMDState");
            if (op == null)
                return null;
            GetMDState getState = new GetMDState();
            if (handles != null)
                getState.getHandles().addAll(handles);
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(getState);
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            GetMDStateResponse response = JAXBUtil.getInstance().createInputParameterValue(result, GetMDStateResponse.class);
            synchronized(mdib) {
                mdib.setStates(response.getMDState());
                Cloner cloner = new Cloner();
                return cloner.deepClone(mdib.getStates());
            }
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public State requestState(String handle) {
        List<String> handles = new ArrayList<>();
        handles.add(handle);
        MDState mdState = getMDState(handles);
        if (mdState == null)
            return null;
        if (mdState.getStates().isEmpty())
            return null;
        return mdState.getStates().get(0);
    }
    
    public InvocationState activate(String handle, FutureInvocationState fis) {
        try {
            Activate activate = new Activate();
            activate.setOperationHandle(handle);
            Operation op = getOperation("SetService", "Activate");
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(activate);
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            ActivateResponse response = JAXBUtil.getInstance().createInputParameterValue(result, ActivateResponse.class);
            handleFutureInvocationState(response.getTransactionId(), fis);
            return response.getInvocationState();
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return InvocationState.FAILED;        
    }
    
    public InvocationState commitValue(String descriptorHandle, NumericValue value, FutureInvocationState fis) {
        return commitValue(descriptorHandle, null, value, fis);
    }
    
    public InvocationState commitValue(String descriptorHandle, String handle, NumericValue value, FutureInvocationState fis) {
        NumericMetricState state = new NumericMetricState();
        state.setReferencedDescriptor(descriptorHandle);
        state.setHandle(handle);
        state.setObservedValue(value);
        return commitState(state, fis);
    }
    
    public InvocationState commitString(String descriptorHandle, NumericValue value, FutureInvocationState fis) {
        return commitValue(descriptorHandle, null, value, fis);
    }
    
    public InvocationState commitString(String descriptorHandle, StringMetricValue value, FutureInvocationState fis) {
        return commitString(descriptorHandle, null, value, fis);
    }
    
    public InvocationState commitString(String descriptorHandle, String handle, StringMetricValue value, FutureInvocationState fis) {
        StringMetricState state = new StringMetricState();
        state.setReferencedDescriptor(descriptorHandle);
        state.setHandle(handle);
        state.setObservedValue(value);
        return commitState(state, fis);
    }    
    
    public InvocationState commitState(State state, FutureInvocationState fis) {
        String opName = null;
        opName = getSetOperationName(state, opName);
        try {
            if (opName == null)
                return InvocationState.FAILED;
            String serviceId = (state instanceof AbstractContextState? "PHI" : "SetService");
            Operation op = getOperation(serviceId, opName);
            if (op == null)
                return InvocationState.FAILED;
            AbstractSet reqObj = createSetRequestType(opName, state);
            if (reqObj == null)
                return InvocationState.FAILED;
            reqObj.setOperationHandle(OSCPToolbox.getFirstOperationHandleForOperationTarget(this, state.getReferencedDescriptor()));
            if (reqObj.getOperationHandle() == null && state.getHandle() != null)
                reqObj.setOperationHandle(OSCPToolbox.getFirstOperationHandleForOperationTarget(this, state.getHandle()));
            if (reqObj.getOperationHandle() == null)
                return InvocationState.FAILED;
            ParameterValue request = JAXBUtil.getInstance().createOutputParameterValue(reqObj);
            ParameterValue result = op.invoke(request, CredentialInfo.EMPTY_CREDENTIAL_INFO);
            AbstractSetResponse response = JAXBUtil.getInstance().createInputParameterValue(result, AbstractSetResponse.class);
            handleFutureInvocationState(response.getTransactionId(), fis);
            return response.getInvocationState();
        } catch (Exception ex) {
            Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return InvocationState.FAILED;
    }

    private AbstractSet createSetRequestType(String opName, State state) {
        AbstractSet reqObj = null;
        switch(opName) {
            case "SetValue": 
                reqObj = new SetValue(); 
                ((SetValue)reqObj).setValue(((NumericMetricState)state).getObservedValue().getValue());
                break;
            case "SetString": 
                reqObj = new SetString(); 
                ((SetString)reqObj).setString(((StringMetricState)state).getObservedValue().getValue());
                break;
            case "SetContextState": 
                reqObj = new SetContextState(); 
                ((SetContextState)reqObj).getProposedContextStates().add((AbstractContextState) state);
                break;
            case "SetAlertState": 
                reqObj = new SetAlertState(); 
                ((SetAlertState)reqObj).setState((AbstractAlertState) state);
                break;
        }
        return reqObj;
    }

    private String getSetOperationName(State state, String opName) {
        if (state instanceof NumericMetricState) {
            opName = "SetValue";
        }
        else if (state instanceof StringMetricState) {
            opName = "SetString";
        }
        else if (state instanceof AbstractContextState) {
            opName = "SetContextState";
        }
        else if (state instanceof AbstractAlertState) {
            opName = "SetAlertState";
        }
        return opName;
    }

    private ClientSubscription initEventSink(String serviceId, String eventName) {
        EventSource reportSource = getEventSource(serviceId, eventName);
        if (reportSource == null) {
            return null;
        }
        List<HTTPBinding> bindings = new ArrayList<>();
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        int port = SoftICE.getInstance().extractNextPort();
        while (adrIter.hasNext()) {
            IPAddress next = adrIter.next();
            HTTPBinding binding = new HTTPBinding(next, port, "/" + eventName + "Sink", DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
            bindings.add(binding);            
        }
        try {
            return reportSource.subscribe(this, 0, bindings, CredentialInfo.EMPTY_CREDENTIAL_INFO);
        } catch (Exception ex) {
            if (Log.isDebug()) {
                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.SEVERE, null, ex);            
            }
            return null;
        }
    }
    
    public void registerEventHandler(OSCPConsumerHandler handler) {
        eventHandler.put(handler.getDescriptorHandle(), handler);
        if (handler instanceof IOSCPConsumerStateChangedHandler) {
            // Reflection magic, get the actual template type in case of a state handler
            try {
                Type superClass = handler.getClass().getGenericSuperclass();
                Type actualType = ((ParameterizedType)superClass).getActualTypeArguments()[0];
                if (actualType.getTypeName().equals(RealTimeSampleArrayMetricState.class.getTypeName())) {
                    // Enable stream reception in case of a realtime stream handler
                    MDPWSStreamingManager.getInstance().addStreamListeners(this, streamAddrs);                            
                }
            }
            catch(Exception e) {
                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.WARNING, "Handler is missing type parameters, streaming may not be supported!");
            }
        }
    }

    public void unregisterEventHandler(OSCPConsumerHandler handler) {
        eventHandler.remove(handler.getDescriptorHandle());
    }
    
    public void unregisterEventHandler(String desriptorHandle) {
        eventHandler.remove(desriptorHandle);
    }

    public void setConnectionLostHandler(OSCPConnectionLostHandler connectionLostHandler) {
        this.connectionLostHandler = connectionLostHandler;
    }

    public OSCPConnectionLostHandler getConnectionLostHandler() {
        return connectionLostHandler;
    }

    @Override
    public ParameterValue eventReceived(ClientSubscription subscription, URI actionURI, ParameterValue parameterValue) {
        synchronized(subscriptions) {        
            if (subscription == null && actionURI.equals(new URI(MDPWSStreamingManager.ACTION_URI))) {
                streamReceived(parameterValue);
                return null;
            }
            else if (subscription == null || !subscriptions.containsKey(subscription)) {
                throw new RuntimeException("Fatal error: subscription type can't be resolved!");
            }
            if (subscriptions.get(subscription).equals(EPISODIC_METRIC_REPORT)) {
                emrEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(EPISODIC_ALERT_REPORT)) {
                earEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(EPISODIC_CONTEXT_CHANGED_REPORT)) {
                ecrEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(OPERATION_INVOKED_REPORT)) {
                oirEventReceived(parameterValue);
            }
            else if (subscriptions.get(subscription).equals(PERIODIC_METRIC_REPORT)) {
                pmrEventReceived(parameterValue);
            }              
            else if (subscriptions.get(subscription).equals(PERIODIC_CONTEXT_CHANGED_REPORT)) {
                pcrEventReceived(parameterValue);
            }    
            else if (subscriptions.get(subscription).equals(PERIODIC_ALERT_REPORT)) {
                parEventReceived(parameterValue);
            }            
        }
        return null;
    }

    private void streamReceived(ParameterValue parameterValue) {
        WaveformStream wfs = JAXBUtil.getInstance().createInputParameterValue(parameterValue, WaveformStream.class);
        if (wfs != null) {
            for (RealTimeSampleArrayMetricState rtsams : wfs.getRealTimeSampleArrays()) {
                final OSCPConsumerHandler handler = eventHandler.get(rtsams.getReferencedDescriptor());
                if (handler != null && handler instanceof IOSCPConsumerStateChangedHandler)
                    ((IOSCPConsumerStateChangedHandler)handler).onStateChanged(rtsams);
            }
        }
    }
    
    protected void unregisterFutureInvocationstate(FutureInvocationState fis) {
        fisMap.remove(fis.getTransactionId());
    }
    
    private void handleFutureInvocationState(long transactionId, FutureInvocationState fis) {
        if (fisMap.containsValue(fis)) {
            throw new RuntimeException("FutureInvocationState instance already in use!");
        }
        fis.setTransactionId(transactionId);
        fis.setConsumer(this);
        fisMap.put(transactionId, fis);
        // Dequeue possible intermediate events
        while (transactionQueue.size() > 0) {
            TransactionState ts = transactionQueue.poll();
            if (fisMap.containsKey(ts.getTransactionId())) {
                fisMap.get(ts.getTransactionId()).setActual(ts.getInvocationState());
            }
        }
    }
    
    private void pmrEventReceived(ParameterValue pv) {
        PeriodicMetricReport pmr = JAXBUtil.getInstance().createInputParameterValue(pv, PeriodicMetricReport.class);
        for (MetricReportPart part : pmr.getReportParts()) {
            handleMetricReportPart(part);
        }
    }
    
    private void pcrEventReceived(ParameterValue pv) {
        PeriodicContextChangedReport ecr = JAXBUtil.getInstance().createInputParameterValue(pv, PeriodicContextChangedReport.class);
        for (ContextChangedReportPart part : ecr.getReportParts()) {      
            handleContextReportPart(part);
        } 
    }

    private void emrEventReceived(ParameterValue pv) {
        EpisodicMetricReport emr = JAXBUtil.getInstance().createInputParameterValue(pv, EpisodicMetricReport.class);
        for (MetricReportPart part : emr.getReportParts()) {
            handleMetricReportPart(part);
        }
    }

    private void earEventReceived(ParameterValue pv) {
        EpisodicAlertReport ear = JAXBUtil.getInstance().createInputParameterValue(pv, EpisodicAlertReport.class);
        for (AlertReportPart part : ear.getReportParts()) {
            handleAlertReportPart(part);
        }
    }
    
    private void parEventReceived(ParameterValue pv) {
        PeriodicAlertReport par = JAXBUtil.getInstance().createInputParameterValue(pv, PeriodicAlertReport.class);
        for (AlertReportPart part : par.getReportParts()) {
            handleAlertReportPart(part);
        }
    }    
    
    private void handleAlertReportPart(AlertReportPart part) {
        for (AbstractAlertState next : part.getAlertStates()) {
            final OSCPConsumerHandler handler = eventHandler.get(next.getReferencedDescriptor());
            if (handler != null && handler instanceof IOSCPConsumerStateChangedHandler)
                ((IOSCPConsumerStateChangedHandler)handler).onStateChanged(next);
        }
    }    

    private void handleMetricReportPart(MetricReportPart part) {
        for (AbstractMetricState next : part.getMetrics()) {
            final OSCPConsumerHandler handler = eventHandler.get(next.getReferencedDescriptor());
            if (handler != null && handler instanceof IOSCPConsumerStateChangedHandler)
                ((IOSCPConsumerStateChangedHandler)handler).onStateChanged(next);
        }
    }
    
    private void ecrEventReceived(ParameterValue pv) {
        EpisodicContextChangedReport ecr = JAXBUtil.getInstance().createInputParameterValue(pv, EpisodicContextChangedReport.class);
        for (ContextChangedReportPart part : ecr.getReportParts()) {
            handleContextReportPart(part);
        }
    }

    private void handleContextReportPart(ContextChangedReportPart part) {
        for (String next : part.getChangedContextStates()) {
            final OSCPConsumerHandler handler = eventHandler.get(next);
            if (handler != null && handler instanceof IOSCPConsumerStateChangedHandler) {
                final State requestedState = requestState(next);
                if (requestedState == null) {
                    Logger.getLogger(OSCPConsumer.class.getName()).log(Level.WARNING, "Error fetching context state. Can't forward to event handler: {0}", next);
                    continue;
                }
                ((IOSCPConsumerStateChangedHandler)handler).onStateChanged(requestedState);
            }
        }
    }

    private void oirEventReceived(ParameterValue pv) {
        OperationInvokedReport oir = JAXBUtil.getInstance().createInputParameterValue(pv, OperationInvokedReport.class);
        for (OperationInvokedReportPart part : oir.getReportDetails()) {
            String target = part.getOperationTarget();
            String handlerKey;
            if (target == null)
                target = OSCPToolbox.getOperationTargetForOperationHandle(this, part.getOperation());
            if (target == null) {
                Logger.getLogger(OSCPConsumer.class.getName()).log(Level.WARNING, "Error in operation invoked report, can't resolve target: {0}", part.getOperation());
                return;                    
            }
            if (eventHandler.containsKey(part.getOperation())) {
                // Activate operations use operation handle
                handlerKey = part.getOperation();
            } else {
                handlerKey = target;
            }
            // Queue to check intermediate events during commits
            transactionQueue.add(new TransactionState(part.getTransactionId(), part.getOperationState()));
            // Notify about future invocation state events
            if (fisMap.containsKey(part.getTransactionId())) {
                fisMap.get(part.getTransactionId()).setActual(part.getOperationState());
            }
            OperationInvocationContext oic = new OperationInvocationContext(target, part.getTransactionId());
            final OSCPConsumerHandler handler = eventHandler.get(handlerKey);
            if (handler != null && handler instanceof IOSCPConsumerOperationInvokedHandler) {
                ((IOSCPConsumerOperationInvokedHandler)handler).onOperationInvoked(oic, part.getOperationState());
            }
        }    
    }
    
    @Override
    public String getReferenceId() {
        return deviceRef.getEndpointReference().toString();
    }    
    
}
