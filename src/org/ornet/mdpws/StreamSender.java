/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.mdpws;

import java.io.ByteArrayOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.WaveformStream;
import org.ornet.softice.provider.OSCPProvider;
import org.w3c.dom.Document;
import org.yads.java.constants.DPWS2009.DPWSConstants2009;
import org.yads.java.constants.DPWS2009.WSDConstants2009;
import org.yads.java.constants.WSAConstants2009;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.types.AppSequence;

class StreamSender {
        
    private final List<DatagramSocket> sockets;
    private final OSCPProvider provider;
    private final InetAddress mCastDestination;
    private final int mCastPort;
    private MessageFactory messageFactory;
    private DocumentBuilderFactory docFactory;
	private DocumentBuilder docBuilder;
    
    StreamSender(List<DatagramSocket> sockets, InetAddress mCastDestination, int mCastPort, OSCPProvider provider) {
        this.sockets = sockets;
        this.mCastDestination = mCastDestination;
        this.mCastPort = mCastPort;
        this.provider = provider;
        try {
            messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();            
        } catch (Exception ex) {
            Logger.getLogger(StreamSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    OSCPProvider getProvider() {
        return provider;
    }

    synchronized void sendAll(RealTimeSampleArrayMetricState state) {
        try {
            SOAPMessage msg = createStreamSOAPMessage(state);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            msg.writeTo(bos);
            byte [] buf = bos.toByteArray();
            for (DatagramSocket socket : sockets) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length, mCastDestination, mCastPort);
                socket.send(packet);                
            }
        } catch (Exception ex) {
            Logger.getLogger(StreamSender.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private SOAPMessage createStreamSOAPMessage(RealTimeSampleArrayMetricState state) throws Exception {
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("wsa", WSAConstants2009.WSA_NAMESPACE_NAME);
        envelope.addNamespaceDeclaration("dpws", DPWSConstants2009.DPWS_NAMESPACE_NAME);
        envelope.addNamespaceDeclaration("wsd", WSDConstants2009.WSD_NAMESPACE_NAME);
        SOAPHeader header = envelope.getHeader();
        header.addChildElement("MessageID", "wsa").addTextNode(UUID.randomUUID().toString());
        header.addChildElement("Action", "wsa").addTextNode(MDPWSStreamingManager.ACTION_URI);
        header.addChildElement("To", "wsa").addTextNode(MDPWSStreamingManager.PROTOCOL_PREFIX + mCastDestination.getHostAddress() + ":" + mCastPort);
        AppSequence next = provider.getAppSequencer().getNext();
        SOAPElement appElem = header.addChildElement("AppSequence", "wsd").addAttribute(envelope.createName("InstanceId"), Long.toString(next.getInstanceId()));
        appElem.addAttribute(envelope.createName("MessageNumber"), Long.toString(next.getMessageNumber()));
        SOAPBody body = envelope.getBody();
        WaveformStream wfs = new WaveformStream();
        wfs.getRealTimeSampleArrays().add(state);
        Document streamDoc = docBuilder.newDocument();
        JAXBUtil.getInstance().marshallTo(wfs, streamDoc);
        body.addDocument(streamDoc);
        soapMessage.saveChanges();
        return soapMessage;
    }

    void close() {
        for (DatagramSocket next : sockets) {
            next.close();
        }
    }
        
}
