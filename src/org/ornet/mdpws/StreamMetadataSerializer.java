/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.mdpws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.ornet.mdpws.MDPWSStreamingManager.ACTION_URI;
import static org.ornet.mdpws.MDPWSStreamingManager.MESSAGE_MODEL_NS;
import static org.ornet.mdpws.MDPWSStreamingManager.STREAMING_NAMESPACE;
import static org.ornet.mdpws.MDPWSStreamingManager.STREAM_TYPE;
import static org.ornet.mdpws.MDPWSStreamingManager.TARGET_NAMESPACE;
import org.yads.java.constants.DPWS2009.WSMEXConstants2009;
import org.yads.java.io.xml.ElementHandler;
import org.yads.java.io.xml.ElementParser;
import org.yads.java.io.xml.Ws4dXmlSerializer;
import org.yads.java.types.QName;
import org.yads.java.util.WS4DIllegalStateException;
import org.yads.java.xmlpull.v1.XmlPullParser;
import org.yads.java.xmlpull.v1.XmlPullParserException;

public class StreamMetadataSerializer {

   protected static ElementHandler StreamMEXSerializer() {
        return new ElementHandler() {
            
            @Override
            public Object handleElement(QName elementName, ElementParser parser) throws XmlPullParserException, IOException {
                String entryName = parser.getName();
                int event = parser.nextTag();
                List<String> addresses = new ArrayList<>();
                while (event != XmlPullParser.END_TAG || !parser.getName().equals(entryName)) {
                    String name = parser.getName();
                    String nameSpace = parser.getNamespace();
                    if (name.equals("streamAddress") && nameSpace.equals(STREAMING_NAMESPACE) && event != XmlPullParser.END_TAG) {
                        parser.next();
                        addresses.add(parser.getText());
                    }
                    event = parser.nextTag();
                }
                return addresses;
            }

            @Override
            public void serializeElement(Ws4dXmlSerializer serializer, QName qname, Object value) throws IllegalArgumentException, WS4DIllegalStateException, IOException {
                serializer.startTag(WSMEXConstants2009.WSX_NAMESPACE_NAME, "MetadataSection");
                serializer.attribute(null, "Dialect", STREAMING_NAMESPACE + "/StreamDescriptions");
                serializer.attribute(null, "Identifier", TARGET_NAMESPACE);
                serializer.setPrefix("wsstm", STREAMING_NAMESPACE);
                serializer.startTag(STREAMING_NAMESPACE, "StreamDescriptions");
                serializer.attribute(null, "targetNamespace", TARGET_NAMESPACE);
                serializer.startTag(STREAMING_NAMESPACE, "streamType");
                serializer.attribute(null, "id", "WaveformStream");
                serializer.attribute(null, "actionURI", ACTION_URI);
                serializer.attribute(null, "streamType", STREAM_TYPE);
                serializer.attribute(MESSAGE_MODEL_NS, "element", "WaveformStream");
                if (qname.equals(new QName("StreamDescriptions", STREAMING_NAMESPACE))) {
                    List<String> addresses = (List<String>)value;
                    for (String nextAdr : addresses) {
                        serializer.startTag(STREAMING_NAMESPACE, "StreamTransmission");
                        serializer.startTag(STREAMING_NAMESPACE, "streamAddress");
                        serializer.text(nextAdr);
                        serializer.endTag(STREAMING_NAMESPACE, "streamAddress");
                        serializer.endTag(STREAMING_NAMESPACE, "StreamTransmission"); 
                    }
                }
                serializer.endTag(STREAMING_NAMESPACE, "streamType");            
                serializer.endTag(STREAMING_NAMESPACE, "StreamDescriptions");
                serializer.endTag(WSMEXConstants2009.WSX_NAMESPACE_NAME, "MetadataSection");
            }
            
        };        
    }    
    
}
