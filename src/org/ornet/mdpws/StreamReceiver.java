/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.mdpws;

import java.io.ByteArrayInputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import static org.ornet.mdpws.MDPWSStreamingManager.ACTION_URI;
import org.ornet.softice.consumer.OSCPConsumer;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.yads.java.constants.WSAConstants2009;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.structures.MaxHashMap;
import org.yads.java.types.URI;

class StreamReceiver extends Thread {

    private static final int BUFFER_SIZE = 65536;

    private MulticastSocket socket;
    private InetAddress group;
    private OSCPConsumer consumer;
    private DocumentBuilder builder;
    private static final Set<String> msgIdMap = Collections.synchronizedSet(Collections.newSetFromMap(new MaxHashMap<String, Boolean>(128)));

    StreamReceiver(OSCPConsumer consumer, MulticastSocket socket) {
        this.consumer = consumer;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        this.socket = socket;
        try {
            this.builder = factory.newDocumentBuilder();            
        }
        catch(Exception e) {
            Logger.getLogger(StreamReceiver.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void run() {
        try {
            if (socket == null) {
                Logger.getLogger(StreamReceiver.class.getName()).log(Level.SEVERE, "Multicast socket not properly initialized, aborting stream receiver!");
                return;
            }
            while (!isInterrupted()) {
                byte[] buffer = new byte[BUFFER_SIZE];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                try {
                    socket.receive(packet);
                    Document soapDoc = builder.parse(new ByteArrayInputStream(packet.getData(), 0, packet.getLength()));
                    NodeList soapBodySub = soapDoc.getElementsByTagNameNS("*", "Body");
                    NodeList messageID = soapDoc.getElementsByTagNameNS(WSAConstants2009.WSA_NAMESPACE_NAME, "MessageID");
                    if (messageID != null && messageID.getLength() == 1) {
                        String id = messageID.item(0).getTextContent();
                        if (msgIdMap.contains(id))
                            continue;
                        msgIdMap.add(id);
                    }
                    Node bodyNode = soapBodySub.item(0);                        
                    Node firstChild = bodyNode.getFirstChild();
                    ParameterValue pv = new ParameterValue();
                    pv.setParameterXmlData(firstChild instanceof org.w3c.dom.Element? firstChild : firstChild.getNextSibling());
                    consumer.eventReceived(null, new URI(ACTION_URI), pv);
                }                    
                catch (SAXException e) {
                    Logger.getLogger(MDPWSStreamingManager.class.getName()).log(Level.SEVERE, null, e);
                }
                catch (Exception ex) {
                    break;
                }
            }
            socket.leaveGroup(group);                
            socket.close();
        } catch (Exception ex) {
            Logger.getLogger(MDPWSStreamingManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    OSCPConsumer getConsumer() {
        return consumer;
    }

}
