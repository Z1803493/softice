package org.ornet.softice.test;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.FutureInvocationState;
import org.ornet.softice.consumer.OSCPConsumer;
import org.ornet.softice.consumer.OSCPConsumerEventHandler;
import org.ornet.softice.consumer.OSCPServiceManager;
import org.ornet.softice.provider.OperationInvocationContext;

/**
 *
 * @author besting
 */
public class ExampleConsumer {
    
    static OSCPConsumer consumer = null;
    
    public ExampleConsumer() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();    
        SoftICE.getInstance().setPortStart(30000);
        
        consumer = OSCPServiceManager.getInstance().discoverEPR("UDI-1234567890");
        consumer.registerEventHandler(new OSCPConsumerEventHandler<NumericMetricState>("handle_metric") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received operation invoked (handle CUR): " + is.name());
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                System.out.println("Received state changed, value (handle CUR): " + ((NumericMetricState)state).getObservedValue().getValue().doubleValue());
            }
        });        
    }
    
    @AfterClass
    public static void tearDownClass() {
        if (consumer != null)
            consumer.disconnect();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ExampleConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }        
        SoftICE.getInstance().shutdown();
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
        try {
            Thread.sleep(2000);
        } catch (Exception ex) {
            Logger.getLogger(ExampleProvider.class.getName()).log(Level.SEVERE, null, ex);
        }          
    }

    @Test
    public void testGetSet() {
        assertNotNull(consumer);
        if (consumer == null)
            return;                
        // Get
        NumericMetricState nms = (NumericMetricState) consumer.requestState("handle_metric");
        assertNotNull(nms);
        assertNotNull(nms.getObservedValue());
        assertNotNull(nms.getObservedValue().getValue());
        // Set
        nms.getObservedValue().setValue(BigDecimal.TEN);
        for (int i=0; i < 100; i++) {
            FutureInvocationState fis = new FutureInvocationState();
            long startTime = System.currentTimeMillis();
            assertEquals(InvocationState.WAITING, consumer.commitState(nms, fis));
            long stopTime = System.currentTimeMillis();
            assertEquals(true, fis.waitReceived(InvocationState.FINISHED, 5000));
            long elapsedTime = stopTime - startTime;
            System.out.println("Time Set Value: " + elapsedTime);
        }        
        // Get
        long startTime = System.currentTimeMillis();
        nms = (NumericMetricState) consumer.requestState("handle_metric");
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Time Get Value: " + elapsedTime);
        assertNotNull(nms.getObservedValue());
        assertEquals(BigDecimal.TEN, nms.getObservedValue().getValue());        
    }
    
}
