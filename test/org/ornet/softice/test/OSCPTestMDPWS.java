package org.ornet.softice.test;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.OSCPConsumer;
import org.ornet.softice.consumer.OSCPConsumerStateChangedHandler;
import org.ornet.softice.provider.OSCPProvider;
import org.ornet.softice.consumer.OSCPServiceManager;
import org.ornet.softice.test.classes.DemoProviderFactory;
import org.ornet.softice.test.classes.DemoProviderFactory.DemoStreamStateHandler;

/**
 *
 * @author besting
 */
public class OSCPTestMDPWS {
    
    static OSCPProvider provider = null;
    static OSCPConsumer consumer = null;
    
    public OSCPTestMDPWS() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();
        SoftICE.getInstance().setSchemaValidationEnabled(true);
        provider = DemoProviderFactory.getDemoProvider("UDI-1234567890");
        provider.startup();              
        consumer = OSCPServiceManager.getInstance().discoverEPR("UDI-1234567890");        
    }
    
    @AfterClass
    public static void tearDownClass() {         
        if (consumer != null)
            consumer.disconnect();
        provider.shutdown();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(OSCPTest.class.getName()).log(Level.SEVERE, null, ex);
        }        
        SoftICE.getInstance().shutdown();
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    @Test
    public void testStreaming() {
        assertNotNull(consumer);        
        if (consumer == null)
            return;         
        final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        // Receive stream
        consumer.registerEventHandler(new OSCPConsumerStateChangedHandler<RealTimeSampleArrayMetricState>("handle_stream") {
            
            @Override
            public void onStateChanged(RealTimeSampleArrayMetricState state) {
                assertNotNull(state.getObservedValue());
                assertNotNull(state.getObservedValue().getValues());                
                List<BigDecimal> values = state.getObservedValue().getValues();
                StringBuilder sb = new StringBuilder();
                sb.append("Received: ");
                for (BigDecimal next : values) {
                    sb.append(next.doubleValue()).append(" ");
                }
                System.out.println(sb.toString()); 
                changeEventReceived.set(true);
            }
        });
        // Produce stream
        DemoStreamStateHandler dsh = (DemoStreamStateHandler) provider.getHandler("handle_stream");
        for (int i = 0; i < 5; i++) {
            System.out.println("Sending...");
            dsh.setValueInternal(new double [] {1, 2, 3, 4, 5});
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(OSCPTestMDPWS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        assertEquals(true, changeEventReceived.get());
        consumer.unregisterEventHandler("handle_stream");
    }

    
}
