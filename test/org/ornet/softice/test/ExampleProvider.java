package org.ornet.softice.test;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ornet.softice.SoftICE;
import org.ornet.softice.provider.OSCPProvider;
import org.ornet.softice.test.classes.DemoProviderFactory;

/**
 *
 * @author besting
 */
public class ExampleProvider {
    
    static OSCPProvider provider = null;
    
    public ExampleProvider() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();
        provider = DemoProviderFactory.getDemoProvider("UDI-1234567890");
        provider.startup();      
    }
       
    @AfterClass
    public static void tearDownClass() {
        try {
            System.out.println("Provider running...");
            Thread.sleep(Integer.MAX_VALUE);
        } catch (Exception ex) {
            Logger.getLogger(ExampleProvider.class.getName()).log(Level.SEVERE, null, ex);
        }        
        SoftICE.getInstance().shutdown();
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMDIB() {
        assertEquals(provider.isRunning(), true);
    }
    
}
