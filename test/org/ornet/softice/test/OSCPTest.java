package org.ornet.softice.test;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ornet.cdm.CurrentAlertSignal;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.LocationContextState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.SignalPresence;
import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.FutureInvocationState;
import org.ornet.softice.consumer.OSCPConsumer;
import org.ornet.softice.consumer.OSCPConsumerEventHandler;
import org.ornet.softice.consumer.OSCPConsumerOperationInvokedHandler;
import org.ornet.softice.consumer.OSCPConsumerStateChangedHandler;
import org.ornet.softice.provider.OSCPProvider;
import org.ornet.softice.consumer.OSCPServiceManager;
import org.ornet.softice.provider.OperationInvocationContext;
import org.ornet.softice.test.classes.DemoProviderFactory;

/**
 *
 * @author besting
 */
public class OSCPTest {
    
    static OSCPProvider provider = null;
    static OSCPConsumer consumer = null;
    
    public OSCPTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();
        SoftICE.getInstance().setSchemaValidationEnabled(true);
        provider = DemoProviderFactory.getDemoProvider("UDI-1234567890");
        provider.startup();      
        consumer = OSCPServiceManager.getInstance().discoverEPR("UDI-1234567890");     
    }
    
    @AfterClass
    public static void tearDownClass() {         
        if (consumer != null)
            consumer.disconnect();
        provider.shutdown();       
        SoftICE.getInstance().shutdown();
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMDIB() {
        assertEquals(provider.isRunning(), true);
        try {
            Thread.sleep(0);
        } catch (InterruptedException ex) {
            Logger.getLogger(OSCPTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertNotNull(consumer);
        if (consumer == null)
            return;       
        long startTime = System.currentTimeMillis();
        assertEquals(true, consumer.getMDIB() != null);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Time Get MDIB: " + elapsedTime);
    }
    
    @Test
    public void testGetSetMetric() {
        assertNotNull(consumer);
        if (consumer == null)
            return;         
        final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new OSCPConsumerEventHandler<NumericMetricState>("handle_metric") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());                
            }
            
            @Override
            public void onStateChanged(NumericMetricState state) {
                changeEventReceived.set(true);
            }
        });
        // Get metric
        NumericMetricState nms = (NumericMetricState) consumer.requestState("handle_metric");
        assertNotNull(nms);
        assertNotNull(nms.getObservedValue());
        assertNotNull(nms.getObservedValue().getValue());
        // Set metric
        FutureInvocationState fis = new FutureInvocationState();
        nms.getObservedValue().setValue(BigDecimal.valueOf(2));
        long startTime = System.currentTimeMillis();
        assertEquals(InvocationState.WAITING, consumer.commitState(nms, fis));
        long stopTime = System.currentTimeMillis();
        assertEquals(true, fis.waitReceived(InvocationState.FINISHED, 2000));
        long elapsedTime = stopTime - startTime;
        System.out.println("Time Set Value: " + elapsedTime);
        // Get again and compare
        startTime = System.currentTimeMillis();
        nms = (NumericMetricState) consumer.requestState("handle_metric");
        stopTime = System.currentTimeMillis();
        elapsedTime = stopTime - startTime;
        System.out.println("Time Get Value: " + elapsedTime);
        assertNotNull(nms);
        assertNotNull(nms.getObservedValue());
        assertNotNull(nms.getObservedValue().getValue());
        assertEquals(BigDecimal.valueOf(2), nms.getObservedValue().getValue());
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(OSCPTest.class.getName()).log(Level.SEVERE, null, ex);
        }        
        assertEquals(true, changeEventReceived.get());
        consumer.unregisterEventHandler("handle_metric");
    }
    
    @Test
    public void testGetSetContext() {
        assertNotNull(consumer);
        if (consumer == null)
            return;         
        final AtomicBoolean changeEventReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new OSCPConsumerEventHandler<LocationContextState>("handle_context") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name());
            }
            
            @Override
            public void onStateChanged(LocationContextState state) {
                changeEventReceived.set(true);
            }
            
        });
        FutureInvocationState fis = new FutureInvocationState();
        // Get context
        LocationContextState lcs = (LocationContextState) consumer.requestState("handle_context");
        assertNotNull(lcs);
        assertEquals(true, lcs.getIdentification().size() == 1);
        // Set context
        InstanceIdentifier instId = new InstanceIdentifier();
        instId.setRoot("root");
        instId.setExt("TestSet");
        lcs.getIdentification().set(0, instId);
        assertEquals(InvocationState.WAITING, consumer.commitState(lcs, fis));
        assertEquals(true, fis.waitReceived(new InvocationState[] { InvocationState.STARTED, InvocationState.FINISHED }, 2000));
        // Get again and compare
        lcs = (LocationContextState) consumer.requestState("handle_context");
        assertNotNull(lcs);
        assertEquals(true, lcs.getIdentification().get(0).getExt().equals("TestSet"));
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(OSCPTest.class.getName()).log(Level.SEVERE, null, ex);
        }                
        assertEquals(true, changeEventReceived.get());
        consumer.unregisterEventHandler("handle_context");
    }    
    
    @Test
    public void testActivate() {
        assertNotNull(consumer);
        if (consumer == null)
            return;
        final AtomicBoolean invokeEventReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new OSCPConsumerOperationInvokedHandler("handle_cmd") {
            
            @Override
            public void onOperationInvoked(OperationInvocationContext oic, InvocationState is) {
                System.out.println("Received OIR, handle = " + getDescriptorHandle() + ", IS = " + is.name()); 
                invokeEventReceived.set(true);
            }

        });
        FutureInvocationState fis = new FutureInvocationState();
        assertEquals(InvocationState.WAITING, consumer.activate("handle_cmd", fis));
        assertEquals(true, fis.waitReceived(new InvocationState[] { InvocationState.STARTED, InvocationState.FINISHED }, 2000));
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(OSCPTest.class.getName()).log(Level.SEVERE, null, ex);
        }                
        assertEquals(true, invokeEventReceived.get());        
        consumer.unregisterEventHandler("handle_cmd");
    }
    
    @Test
    public void testAlerts() {
        // register for alarm signal events
        final AtomicBoolean onReceived = new AtomicBoolean(false);
        final AtomicBoolean latchReceived = new AtomicBoolean(false);
        consumer.registerEventHandler(new OSCPConsumerStateChangedHandler<CurrentAlertSignal>("handle_alert_signal_latching") {

            @Override
            public void onStateChanged(CurrentAlertSignal state) {
                if (state.getPresence() == SignalPresence.ON)
                    onReceived.set(true);
                if (state.getPresence() == SignalPresence.LATCHED)
                    latchReceived.set(true);
            }
            
        });        
        // Get metric
        NumericMetricState nms = (NumericMetricState) consumer.requestState("handle_metric");
        assertNotNull(nms);
        assertNotNull(nms.getObservedValue());
        // Set metric to value above upper limit (0, 2), alert condition triggers alert signal ON
        FutureInvocationState fis = new FutureInvocationState();
        nms.getObservedValue().setValue(BigDecimal.valueOf(3));
        assertEquals(InvocationState.WAITING, consumer.commitState(nms, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FINISHED, 2000));        
        // Set metric to value within bounds (0, 2), alert condition triggers alert signal LATCHED
        fis = new FutureInvocationState();
        nms.getObservedValue().setValue(BigDecimal.valueOf(1));
        assertEquals(InvocationState.WAITING, consumer.commitState(nms, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FINISHED, 2000));
        // Check if events have been received
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(OSCPTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(true, onReceived.get());
        assertEquals(true, latchReceived.get());
        // Test get/set alert signal
        CurrentAlertSignal cas = (CurrentAlertSignal) consumer.requestState("handle_alert_signal_latching");
        assertNotNull(cas);
        assertNotSame(cas.getPresence(), SignalPresence.OFF);
        cas.setPresence(SignalPresence.OFF);
        fis = new FutureInvocationState();
        assertEquals(InvocationState.WAITING, consumer.commitState(cas, fis));
        assertEquals(true, fis.waitReceived(InvocationState.FINISHED, 2000));
        // Compare
        cas = (CurrentAlertSignal) consumer.requestState("handle_alert_signal_latching");
        assertEquals(cas.getPresence(), SignalPresence.OFF);
    }
    
    
}
