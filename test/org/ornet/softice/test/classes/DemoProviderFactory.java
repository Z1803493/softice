/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.test.classes;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import org.ornet.cdm.AbstractIdentifiableContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.ActivateOperationDescriptor;
import org.ornet.cdm.AlertConditionPriority;
import org.ornet.cdm.AlertConditionType;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.AlertSignalManifestation;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.CauseInfo;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.CodedValue;
import org.ornet.cdm.ComponentActivation;
import org.ornet.cdm.ComponentState;
import org.ornet.cdm.ContextAssociationStateValue;
import org.ornet.cdm.CurrentAlertCondition;
import org.ornet.cdm.CurrentAlertSignal;
import org.ornet.cdm.CurrentAlertSystem;
import org.ornet.cdm.CurrentLimitAlertCondition;
import org.ornet.cdm.HydraMDSDescriptor;
import org.ornet.cdm.HydraMDSState;
import org.ornet.cdm.InstanceIdentifier;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.LimitAlertConditionDescriptor;
import org.ornet.cdm.LimitAlertObservationState;
import org.ornet.cdm.LocationContextDescriptor;
import org.ornet.cdm.LocationContextState;
import org.ornet.cdm.MDIB;
import org.ornet.cdm.MeasurementState;
import org.ornet.cdm.MetricAvailability;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.MetricMeasurementState;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericValue;
import org.ornet.cdm.PausableActivation;
import org.ornet.cdm.Range;
import org.ornet.cdm.RealTimeSampleArrayMetricDescriptor;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.RealTimeSampleArrayValue;
import org.ornet.cdm.RemedyInfo;
import org.ornet.cdm.SignalPresence;
import org.ornet.cdm.State;
import org.ornet.cdm.SystemContext;
import org.ornet.cdm.VMDDescriptor;
import org.ornet.softice.provider.OSCPProvider;
import org.ornet.softice.provider.OSCPProviderActivateOperationHandler;
import org.ornet.softice.provider.OSCPProviderAlertConditionStateHandler;
import org.ornet.softice.provider.OSCPProviderMDStateHandler;
import org.ornet.softice.provider.OperationInvocationContext;

public class DemoProviderFactory {
    
    static class DemoNumericStateHandler extends OSCPProviderMDStateHandler<NumericMetricState> {
      
        public static final String HANDLE_METRIC = "handle_metric";
        
        public DemoNumericStateHandler() {
            super(HANDLE_METRIC);
        }
        
        // Helper method
        private synchronized NumericMetricState createState(double value) {
            NumericMetricState nms = new NumericMetricState();
            nms.setState(ComponentActivation.ON);
            nms.setReferencedDescriptor(HANDLE_METRIC);
            NumericValue nv = new NumericValue();
            nv.setValue(BigDecimal.valueOf(value));
            MeasurementState msmntState = new MeasurementState();
            msmntState.setState(MetricMeasurementState.VALID);
            nv.setMeasurementState(msmntState);
            nms.setObservedValue(nv);
            return nms;
        }

        @Override
        public InvocationState onStateChangeRequest(NumericMetricState state, OperationInvocationContext oic) {
            System.out.println("Received numeric value change request: " + ((NumericMetricState)state).getObservedValue().getValue());
            return InvocationState.FINISHED;  // Request O.K., let SoftICE update internal MDIB
            //return InvocationState.CANCELLED;  // State will not be updated in internal MDIB
        }
        
        @Override
        protected NumericMetricState getInitialState() {
            return createState(1);
        }
               
        public final void setValue(double value) {
            NumericMetricState state = createState(value);
            // Update state in internal MDIB and notify consumers (MDIB version will be increased)
            updateState(state);
        }
        
    }
    
    static class AlwaysOnComponentStateHandler extends OSCPProviderMDStateHandler {

        private final boolean mds;
        
        public AlwaysOnComponentStateHandler(String descriptorHandle, boolean mds) {
            super(descriptorHandle);
            this.mds = mds;
        }

        @Override
        protected State getInitialState() {
            ComponentState state = mds? new HydraMDSState() : new ComponentState();
            state.setReferencedDescriptor(super.getDescriptorHandle());
            state.setState(ComponentActivation.ON);
            return state;
        }
        
    }
    
    // Create an arbitrary handler for any type of context state
    static class DemoContextStateHandler<T extends AbstractIdentifiableContextState> extends OSCPProviderMDStateHandler<AbstractIdentifiableContextState> {
      
        private final String handle;
        private final Class<T> clazz;
        
        public DemoContextStateHandler(Class<T> clazz, String handle) {
            super(handle);
            this.clazz = clazz;
            this.handle = handle;
        }
        
        // Helper method
        private synchronized T createState(String ext) {
            try {
                T t = clazz.newInstance();
                t.setReferencedDescriptor(handle);
                t.setContextAssociation(ContextAssociationStateValue.ASSOCIATED);
                if (ext == null)
                    return t;
                InstanceIdentifier instId = new InstanceIdentifier();
                instId.setRoot("root");
                instId.setExt(ext);
                t.getIdentification().add(instId);
                return t;
            } catch (InstantiationException | IllegalAccessException ex) {
                Logger.getLogger(DemoProviderFactory.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }

        @Override
        public InvocationState onStateChangeRequest(AbstractIdentifiableContextState state, OperationInvocationContext oic) {
            System.out.println("Received context value change request...");   
            return InvocationState.FINISHED;  // Request O.K., let SoftICE update internal MDIB and notify consumers
            //return InvocationState.FAILED;  // State will not be updated in internal MDIB
        }
        
        @Override
        protected AbstractIdentifiableContextState getInitialState() {
            return createState("Test");
        }
               
        public final void setValueInternal(String ext) {
            T state = createState(ext);
            // Update state in internal MDIB and notify consumers (MDIB version will be increased)
            updateState(state);
        }
        
    }
    
    static class DemoActivateOperationHandler extends OSCPProviderActivateOperationHandler {

        public static final String HANDLE_CMD = "handle_cmd";
        
        public DemoActivateOperationHandler() {
            super(HANDLE_CMD);
        }

        @Override
        public InvocationState onActivateRequest(MDIB mdib, OperationInvocationContext oic) {
            System.out.println("Received activate request...");   
            return InvocationState.FINISHED;
        }
        
    }
    
    public static class DemoStreamStateHandler extends OSCPProviderMDStateHandler<RealTimeSampleArrayMetricState> {
        
        public DemoStreamStateHandler(String descriptorHandle) {
            super(descriptorHandle);
        }
        
        // Helper method
        private synchronized RealTimeSampleArrayMetricState createState(double [] values) {
            RealTimeSampleArrayMetricState rtsams = new RealTimeSampleArrayMetricState();
            rtsams.setState(ComponentActivation.ON);
            rtsams.setReferencedDescriptor(getDescriptorHandle());
            if (values == null)
                return rtsams;
            RealTimeSampleArrayValue rtsav = new RealTimeSampleArrayValue();
            MeasurementState msmntState = new MeasurementState();
            msmntState.setState(MetricMeasurementState.VALID);
            rtsav.setMeasurementState(msmntState);
            List<BigDecimal> list = rtsav.getValues();
            for (double next : values) {
                list.add(BigDecimal.valueOf(next));
            }
            rtsams.setObservedValue(rtsav);
            return rtsams;
        }        

        @Override
        protected RealTimeSampleArrayMetricState getInitialState() {
            return createState(null);
        }
        
        public final void setValueInternal(double [] values) {
            // Update state in internal MDIB and notify consumers (MDIB version will be increased)
            updateState(createState(values));
        }        
        
    }
    
    public static class AlertSystemStateHandler extends OSCPProviderMDStateHandler<CurrentAlertSystem> {

        public AlertSystemStateHandler(String descriptorHandle) {
            super(descriptorHandle);
        }

        @Override
        protected CurrentAlertSystem getInitialState() {
            CurrentAlertSystem alertSystemState = new CurrentAlertSystem();
            alertSystemState.setState(PausableActivation.ON);
            alertSystemState.setReferencedDescriptor(getDescriptorHandle());
            return alertSystemState;
        }
        
    }
    
    public static class DemoLimitAlertConditionStateHandler extends OSCPProviderAlertConditionStateHandler<CurrentLimitAlertCondition> {

        public DemoLimitAlertConditionStateHandler(String descriptorHandle) {
            super(descriptorHandle);
        }

        // Helper method
        private CurrentLimitAlertCondition createState(double lower, double upper) {
            CurrentLimitAlertCondition alertCondition = new CurrentLimitAlertCondition();
            alertCondition.setReferencedDescriptor(getDescriptorHandle());
            alertCondition.setState(PausableActivation.ON);
            alertCondition.setPresence(false);
            alertCondition.setLimitObservationState(LimitAlertObservationState.ALL_ON);
            Range range = new Range();
            range.setLower(BigDecimal.valueOf(lower));
            range.setUpper(BigDecimal.valueOf(upper));
            alertCondition.setLimits(range);
            return alertCondition;
        }

        @Override
        protected CurrentLimitAlertCondition getInitialState() {
            return createState(0, 2);
        }

        @Override
        public InvocationState onStateChangeRequest(CurrentLimitAlertCondition state, OperationInvocationContext oic) {
            return InvocationState.FINISHED;
        }       

        @Override
        public void sourceHasChanged(AbstractMetricState source, CurrentAlertCondition currentState) {
            // This function will be called whenenver the source state has changed
            if (!(source instanceof NumericMetricState))
                return;
            NumericMetricState nms = (NumericMetricState)source;
            if (nms.getObservedValue() == null)
                return;
            BigDecimal nv;
            if ((nv = nms.getObservedValue().getValue()) == null)
                return;
            CurrentLimitAlertCondition lac = (CurrentLimitAlertCondition)currentState;
            Range limits;
            if ((limits = lac.getLimits()) == null)
                return;
            if (limits.getUpper() == null || limits.getLower() == null)
                return;
            double val = nv.doubleValue();
            boolean trigger = (val > limits.getUpper().doubleValue() || val < limits.getLower().doubleValue());
            // The condition will eventually trigger all signals
            setAlertConditionPresence(currentState, trigger);
        }
  
    }    
    
    public static class DemoAlertSignalStateHandler extends OSCPProviderMDStateHandler<CurrentAlertSignal> {

        public DemoAlertSignalStateHandler(String descriptorHandle) {
            super(descriptorHandle);
        }
        
        // Helper method
        private CurrentAlertSignal createState() {
            CurrentAlertSignal alertSignal = new CurrentAlertSignal();
            alertSignal.setReferencedDescriptor(getDescriptorHandle());
            alertSignal.setState(PausableActivation.ON);
            alertSignal.setPresence(SignalPresence.OFF);
            return alertSignal;
        }
        
        @Override
        public InvocationState onStateChangeRequest(CurrentAlertSignal state, OperationInvocationContext oic) {
            return InvocationState.FINISHED;
        }               

        @Override
        protected CurrentAlertSignal getInitialState() {
            return createState();
        }

    }     
    
    public static synchronized OSCPProvider getDemoProvider(String epr) {
        OSCPProvider provider = new OSCPProvider();
        provider.setEndpointReference(epr);
        // Set some DPWS properties
        provider.setFriendlyName("SoftICE Demo Device");
        provider.setModelName("OR.NET Demo");

        HydraMDSDescriptor mds = new HydraMDSDescriptor();
        mds.setHandle("handle_mds");
        VMDDescriptor vmd = new VMDDescriptor();
        vmd.setHandle("handle_vmd");
        mds.getVMDs().add(vmd);     
        
        ChannelDescriptor chn = new ChannelDescriptor();
        chn.setHandle("handle_chn");
        vmd.getChannels().add(chn);
        
        // Alert condition
        LimitAlertConditionDescriptor limitAlertCondition = new LimitAlertConditionDescriptor();
        limitAlertCondition.getSources().add(DemoNumericStateHandler.HANDLE_METRIC);
        CodedValue limitAlertConditionType = new CodedValue();
        limitAlertConditionType.setCode("MDCX_CODE_ID_ALERT_NUMERIC_CONDITION");
        limitAlertCondition.setType(limitAlertConditionType);
        limitAlertCondition.setConditionType(AlertConditionType.TECHNICAL);
        limitAlertCondition.setPriority(AlertConditionPriority.MEDIUM);
        CauseInfo causeInfo = new CauseInfo();
        final RemedyInfo remedyInfo = new RemedyInfo();
        causeInfo.setRemedyInfo(remedyInfo);
        limitAlertCondition.getCauseInfo().add(causeInfo);
        limitAlertCondition.setHandle("handle_limit_alert_condition");
        final Range maxLimitRange = new Range();
        maxLimitRange.setLower(BigDecimal.ZERO);
        maxLimitRange.setUpper(BigDecimal.TEN);
        limitAlertCondition.setMaxLimitRange(maxLimitRange);
        // Alert signal (latching)
        AlertSignalDescriptor latchingAlertSignal = new AlertSignalDescriptor();
        CodedValue latchingAlertSignalType = new CodedValue();
        latchingAlertSignalType.setCode("MDCX_CODE_ID_ALERT_NUMERIC_SIGNAL");
        latchingAlertSignal.setType(latchingAlertSignalType);
        latchingAlertSignal.setConditionSignaled("handle_limit_alert_condition");
        latchingAlertSignal.setSignalManifestation(AlertSignalManifestation.VISIBLE);
        latchingAlertSignal.setIsLatching(true);
        latchingAlertSignal.setHandle("handle_alert_signal_latching");
        // Alert system
        AlertSystemDescriptor alertSystem = new AlertSystemDescriptor();
        CodedValue alertSystemType = new CodedValue();
        alertSystemType.setCode("MDCX_CODE_ID_ALERT_SYSTEM");
        alertSystem.setType(alertSystemType);
        alertSystem.getAlertConditions().add(limitAlertCondition);
        alertSystem.getAlertSignals().add(latchingAlertSignal);
        alertSystem.setHandle("handler_alert_system");
        // Add alert system to channel
        chn.setAlertSystem(alertSystem);
        
        NumericMetricDescriptor nmd = new NumericMetricDescriptor();
        nmd.setCategory(MetricCategory.SETTING);
        nmd.setAvailability(MetricAvailability.CONTINUOUS);
        CodedValue unit = new CodedValue();
        unit.setCodingSystem("11073");
        unit.setCode("MDCX_MY_UNIT");
        nmd.setUnit(unit);
        CodedValue type = new CodedValue();
        type.setCodingSystem("11073");
        type.setCode("MDCX_MY_TYPE");
        nmd.setType(type);
        nmd.setHandle(DemoNumericStateHandler.HANDLE_METRIC);
        nmd.setResolution(BigDecimal.ONE);
        chn.getMetrics().add(nmd);   
        
        LocationContextDescriptor lcd = new LocationContextDescriptor();
        lcd.setHandle("handle_context");
        SystemContext sc = new SystemContext();
        sc.setLocationContext(lcd);
        sc.setHandle("handle_sc");
        mds.setContext(sc);
        
        RealTimeSampleArrayMetricDescriptor rtsamd = new RealTimeSampleArrayMetricDescriptor();
        rtsamd.setHandle("handle_stream");
        rtsamd.setUnit(unit);
        rtsamd.setType(type);
        rtsamd.setCategory(MetricCategory.MEASUREMENT);
        rtsamd.setAvailability(MetricAvailability.CONTINUOUS);
        try {
            rtsamd.setSampleRate(DatatypeFactory.newInstance().newDuration(10));
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(DemoProviderFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        rtsamd.setResolution(BigDecimal.ONE);
        chn.getMetrics().add(rtsamd);   
        
        provider.addHandler(new DemoNumericStateHandler());              
        provider.addHandler(new DemoContextStateHandler(LocationContextState.class, "handle_context"));   
        provider.addHandler(new DemoActivateOperationHandler());   
        provider.addHandler(new DemoStreamStateHandler("handle_stream"));   
        // States for MDS, VMD and channel
        provider.addHandler(new AlwaysOnComponentStateHandler("handle_mds", true));   
        provider.addHandler(new AlwaysOnComponentStateHandler("handle_vmd", false));   
        provider.addHandler(new AlwaysOnComponentStateHandler("handle_chn", false));   
        provider.addHandler(new AlertSystemStateHandler("handle_alert_system"));
        provider.addHandler(new DemoLimitAlertConditionStateHandler("handle_limit_alert_condition"));
        provider.addHandler(new DemoAlertSignalStateHandler("handle_alert_signal_latching"));
        
        // Make states writable
        provider.createSetOperationForDescriptor(nmd, mds);
        provider.createSetOperationForDescriptor(lcd, mds);
        provider.createSetOperationForDescriptor(latchingAlertSignal, mds);
        // Make activation operation activatable
        ActivateOperationDescriptor aod = new ActivateOperationDescriptor();
        aod.setHandle(DemoActivateOperationHandler.HANDLE_CMD);
        aod.setOperationTarget("none");
        provider.addActivateOperationForDescriptor(aod, mds);
        
        provider.addHydraMDS(mds);

        return provider;
    }
    
}
